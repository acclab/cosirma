\chapter{Technical documentation}
\label{ch:technical-documentation}

\section{First time running \programname{}}

When \programname{} is run for the first time, it will generate the files and folders described in this chapter. However, all can be changed later when the application is up and running.

\programname{} generates an application wide \texttt{\$HOME/.config/acclab/cosirma.conf} configuration file on Linux and a registry entry on Windows. This is where the user-set viewport settings and a few other window related parameters are saved. If you for some reason need to revert \programname{} to its original form, this file can safely be removed.


\section{Project management}
\label{sec:project-management}

\programname{} uses a comprehensive project system to handle simulation data in an efficient way. The default location of the projects is \texttt{\$HOME/cosirma/projects/}. All projects store snapshots of the simulation state into the \emph{history} (\cref{fig:project-management}). Every time \programname{} saves changes of the system, a new history entry will be generated. You can go back and continue from earlier snapshots using the history tree (\cref{sec:history-tree}).


\begin{figure}[!ht]
\begin{forest}
	pic dir tree,
	where level=0{}{% folder icons by default
	directory,
	},
	for tree={%
	s sep-=10pt,
	l sep+=10pt,
	},
	[\$HOME
		[cosirma
			[runtimes]
			[projects
				[<projectname>
					[.cosirmaproject, file]
					[history
						[entry\_1
							[info.txt, file]
							[types.bin, file]
							[elstop
								[elstop.A.AB.in, file]
								[elstop.B.AB.in, file]
							]
							[potentials
								[reppot.A.A.in, file]
								[reppot.A.B.in, file]
								[reppot.B.B.in, file]
							]
							[frames
								[frame\_0
									[mdlat.xyz (mdlat.bin), file]
								]
							]
						]
					]
				]
			]
		]
	]
\end{forest}
\caption{The hierarchy of the \emph{projects directory}. The default location is in \texttt{\$HOME/cosirma/}. See the text for more information about all of the files.}
\label{fig:project-management}
\end{figure}



\subsection{Project}
\label{sec:project-project}

All projects (\texttt{<projectname>} in \cref{fig:project-management}) contain the following files and folders:

\begin{tabularx}{\textwidth}{p{3cm} X}
\texttt{.cosirmaproject} & This file helps \programname{} identify a project folder when opening a project, it also provieds the instance with information about how to populate the fields to recreate the state of the project. \\
\texttt{history/}       & Contains all modifications stored as separate \emph{entries} (\cref{sec:project-history,sec:project-entry}).
\end{tabularx}


\subsection{History}
\label{sec:project-history}

The history has a separate subfolder for each \emph{entry} (\cref{sec:project-entry}), and gives you a simple view of everything that has changed since you started editing your simulation setup.



\subsection{Entry}
\label{sec:project-entry}

Each \emph{entry} corresponds to a specific state in the history of the project (\cref{sec:project-project}), and contains information to recreate that state.

\textbf{Types:}

\begin{tabularx}{\textwidth}{p{3cm} X}
\texttt{modification} & When the structure is modified in \programname{}'s \emph{editor tab} (\cref{sec:editor-tab}) a new modification entry is generated. A modification entry contains one single frame. \\
\texttt{simulation}   & A \programname{} simulation might run PARCAS multiple times, and these runs are saved as separate frames in the \emph{entry}.
\end{tabularx}


\textbf{Content:}

\begin{tabularx}{\textwidth}{p{3cm} X}
\texttt{info.txt}    & Human-readable file with a short description of the \emph{entry}. \\
\texttt{types.bin}   & Binary file storing the atom type data: type number, element symbols, masses, and how they interact with each other (\cref{sec:atom-interactions}) for this \emph{entry}. \\
\texttt{elstop/}     & Directory with files describing the interactions related to electronic stopping power. Mostly related to ion--materia interactions for ions with high kinetic energy. When simulating a multicomponent material, PARCAS uses a weighted average of the different elemental contributions. \\
\texttt{potentials/} & Directory with files describing the short-range interactions. \\
\texttt{frames/}     & Directory containing the simulation structure data. The \emph{entry} can have either one or more frames, numbered from 0, depending on the entry type. The data can be saved in either human-readable or binary format (\texttt{*.dat} or \texttt{*.bin}). Each frame corresponds to one single simulation with PARCAS, while each \emph{simulation entry} corresponds to one \programname{} simulation. \emph{Modification entries} only contain one single frame.
\end{tabularx}



\newpage

\section{Runtime management}
\label{sec:runtime-management}

\programname{} has a dedicated location for running the simulations, namely the \texttt{runtime directory} (\cref{fig:runtime-management}). This directory can either be a default, a temporary, or a user defined location. If the temporary location is used, the finished simulation will be copied back to the default \texttt{\$HOME/cosirma/runtime/} directory.


\begin{figure}[!ht]
\begin{forest}
	pic dir tree,
	where level=0{}{% folder icons by default
	directory,
	},
	for tree={%
	s sep-=10pt,
	l sep+=10pt,
	},
	[\$HOME
		[cosirma
			[projects]
			[runtimes
				[runtime
					[control\_variables.txt, file]
					[start\_parcas.sh, file]
					[in
						[elstop.A.AB.in, file]
						[elstop.B.AB.in, file]
						[repoot.A.A.in, file]
						[reppot.A.B.in, file]
						[reppot.B.B.in, file]
						[md.in, file]
						[mdlat.in.xyz, file]
					]
					[parcas.log, file]
					[out]
					[frames
						[frame\_0
							[mdlat.xyz, file]
						]
					]
				]
			]
		]
	]
\end{forest}
\caption{The hierarchy of the \emph{runtime directory}. The default location is in \texttt{\$HOME/cosirma/}. See the text for more information about all of the files.}
\label{fig:runtime-management}
\end{figure}


\programname{} writes the input data to the \emph{runtime directory}. As the simulation potentially is run on a completely other machine than where \programname{} is running, the execution of the simulation has to be handled by another instance than \programname{} itself. The \texttt{start\_parcas.sh} script runs PARCAS and collects necessary output to a safe location.

PARCAS has a naive way of reading inputs from a folder called \texttt{in/} and writing the output to a folder named \texttt{out/}. This does indeed have the benefit of fixed locations and is easier to use because of that. However, when running multiple simulations, this inferes a need to handle the input and output data dynamically.


\vspace{0.5cm}
\textbf{Input data:}

\begin{tabularx}{\textwidth}{p{4.5cm} X}
\texttt{control\_variables.txt} & \texttt{start\_parcas.sh} uses the content of this file to tailor the \programname{} simulation.\\
\texttt{start\_parcas.sh}       & A batch script for running a \programname{} simulation. \\
\texttt{in/}                    & The input data used by PARCAS.
\end{tabularx}


\vspace{0.5cm}
\textbf{Output data:}

\begin{tabularx}{\textwidth}{p{4.5cm} X}
\texttt{parcas.log} & The standard output from every single PARCAS instance is printed to this file. \texttt{start\_parcas.sh} makes sure that it is saved if it is needed. \\
\texttt{out/}       & The various output created by PARCAS will be saved in this directory. All the files \programname{} needs will be copied to another location before the next PARCAS simulation starts using the \texttt{out/} folder again. \\
\texttt{frames/}    & The intermediate frames are saved in a similar manner as in the \emph{frames} directory of the project (\cref{sec:project-entry}). These are then copied back to the project for analysis.
\end{tabularx}


