#
# Copyright (C) 2020, University of Helsinki
#     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
#
# Authors: Jarno Laakso, Christoffer Fridlund
#
#
# This file is part of COSIRMA.
#
# COSIRMA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COSIRMA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
#
#


import numpy as np

import sys
#import argparse
#
#parser = argparse.ArgumentParser(description="Process xyz data between simulations.")
#parser.add_argument("--infile", "-f", help="input filename")
#parser.add_argument("--outfile", "-o", help="output filename")
#parser.add_argument("--config", help="config filename")
#
#args = parser.parse_args()


class Cluster():

  def __init__(self):
    self.ids = []


class ClusterAnalyzer():
      
  def __init__(self, xyz, pbcX, pbcY, pbcZ):
    self.xyz = xyz
    self.pbcX = pbcX
    self.pbcY = pbcY
    self.pbcZ = pbcZ

    self.clusters = []

  def analyze(self, radius):
    self.cluster = []

    # Constructing the neighbour list. Candidates for neighbours are first reduced by dividing
    # the atoms into a 3D grid.
    
    resX = int(np.minimum(1000.0, np.maximum(1.0, (self.xyz.xAtomLimits[1] - self.xyz.xAtomLimits[0]) / radius)))
    resY = int(np.minimum(1000.0, np.maximum(1.0, (self.xyz.yAtomLimits[1] - self.xyz.yAtomLimits[0]) / radius)))
    resZ = int(np.minimum(1000.0, np.maximum(1.0, (self.xyz.zAtomLimits[1] - self.xyz.zAtomLimits[0]) / radius)))

    n = len(self.xyz.elements)
    radiusSquared = radius * radius

    gridMap = {}

    for i in range(n):
      x = self.xyz.xs[i]
      y = self.xyz.ys[i]
      z = self.xyz.zs[i]

      xi = np.maximum(0, np.minimum(int((x - self.xyz.xAtomLimits[0]) / (self.xyz.xAtomLimits[1] - self.xyz.xAtomLimits[0]) * resX), resX - 1))
      yi = np.maximum(0, np.minimum(int((y - self.xyz.yAtomLimits[0]) / (self.xyz.yAtomLimits[1] - self.xyz.yAtomLimits[0]) * resY), resY - 1))
      zi = np.maximum(0, np.minimum(int((z - self.xyz.zAtomLimits[0]) / (self.xyz.zAtomLimits[1] - self.xyz.zAtomLimits[0]) * resZ), resZ - 1))
      
      key = (xi, yi, zi)
      if key not in gridMap:
        gridMap.update({key: []})
      gridMap[key].append(i)


    # Internal helper function for finding the correct cell based on the index
    def mod(i, n):
      return (i % n + n) % n


    nList = {}

    for i in range(resX):
      for j in range(resY):
        for k in range(resZ):

          cells = []
          for a in [i-1, i, i+1]:
            if (self.pbcX and resX > 1):
              a = mod(a, resX)
            if a < 0 or a >= resX:
              continue
            for b in [j-1, j, j+1]:
              if self.pbcY and resY > 1:
                b = mod(b, resY)
              if b < 0 or b >= resY:
                continue
              for c in [k-1, k, k+1]:
                if self.pbcZ and resZ > 1:
                  c = mod(c, resZ)
                if c < 0 or c >= resZ:
                  continue
                if (a, b, c) in gridMap:
                  cells.append(list(gridMap[(a, b, c)]))

          # Finding the neighbours based on the squared distance
          if (i, j, k) in gridMap:
            for i1 in gridMap[(i, j, k)]:
              for cell in cells:
                for i2 in cell:
                  if (i1 >= i2): continue

                  dx = np.abs(self.xyz.xs[i1] - self.xyz.xs[i2])
                  dy = np.abs(self.xyz.ys[i1] - self.xyz.ys[i2])
                  dz = np.abs(self.xyz.zs[i1] - self.xyz.zs[i2])

                  if self.pbcX:
                    dx = np.minimum(dx, self.xyz.xLimits[1] - self.xyz.xLimits[0] - dx)
                  if self.pbcY:
                    dy = np.minimum(dy, self.xyz.yLimits[1] - self.xyz.yLimits[0] - dy)
                  if self.pbcZ:
                    dz = np.minimum(dz, self.xyz.zLimits[1] - self.xyz.zLimits[0] - dz)

                  dSquared = dx*dx + dy*dy + dz*dz

                  if dSquared <= radiusSquared:
                    if i1 not in nList:
                      nList.update({i1 : []})
                    if i2 not in nList:
                      nList.update({i2 : []})
                    nList[i1].append(i2)
                    nList[i2].append(i1)

    added = np.full(n, dtype=bool, fill_value=False)
    for i in range(n):
      if added[i]:
        continue
      c = Cluster()
      toBeHandled = []
      toBeHandled.append(i)
      added[i] = True
      while len(toBeHandled) != 0:
        handling = toBeHandled.pop()
        c.ids.append(handling)
        if handling in nList:
          for iatom in nList[handling]:
            if not added[iatom]:
              toBeHandled.append(iatom)
              added[iatom] = True
      self.clusters.append(c)

  def largestCluster(self):
    xyz = XyzData()
    xyz.xLimits = self.xyz.xLimits
    xyz.yLimits = self.xyz.yLimits
    xyz.zLimits = self.xyz.zLimits

    if len(self.clusters) == 0:
      return xyz

    clusterIndex = 0
    for i in range(1, len(self.clusters)):
      if len(self.clusters[i].ids) > len(self.clusters[clusterIndex].ids):
        clusterIndex = i

    xmin = float("inf")
    xmax = -float("inf")
    ymin = float("inf")
    ymax = -float("inf")
    zmin = float("inf")
    zmax = -float("inf")

    for i in self.clusters[clusterIndex].ids:
      x = self.xyz.xs[i]
      y = self.xyz.ys[i]
      z = self.xyz.zs[i]

      xyz.elements.append(self.xyz.elements[i])
      xyz.xs.append(x)
      xyz.ys.append(y)
      xyz.zs.append(z)
      xyz.ids.append(self.xyz.ids[i])
      xyz.types.append(self.xyz.types[i])
      xyz.vxs.append(self.xyz.vxs[i])
      xyz.vys.append(self.xyz.vys[i])
      xyz.vzs.append(self.xyz.vzs[i])

      xmin = np.minimum(xmin, x)
      xmax = np.maximum(xmax, x)
      ymin = np.minimum(ymin, y)
      ymax = np.maximum(ymax, y)
      zmin = np.minimum(zmin, z)
      zmax = np.maximum(zmax, z)

    xyz.comment = "boxsize {} {} {} pbc {} {} {}\n".format(
      xmax-xmin, ymax-ymin, zmax-zmin, int(self.pbcX), int(self.pbcY), int(self.pbcZ)
    )
    xyz.xAtomLimits = [xmin, xmax]
    xyz.yAtomLimits = [ymin, ymax]
    xyz.zAtomLimits = [zmin, zmax]

    xyz.boxsize = [xmax-xmin, ymax-ymin, zmax-zmin]
    xyz.pbc = [self.pbcX, self.pbcY, self.pbcZ]

    return xyz


class Intersection():
  
  def __init__(self, distance, periodic, shiftX, shiftY, shiftZ):
    self.distance = distance
    self.periodic = periodic
    self.shiftX = shiftX
    self.shiftY = shiftY
    self.shiftZ = shiftZ
    

class CascadeLocationPredictor():
      
  def __init__(self, xyz, pbcX, pbcY, pbcZ):
    self.xyz = xyz
    self.pbcX = pbcX
    self.pbcY = pbcY
    self.pbcZ = pbcZ

  def predict(self, x, y, z, theta, phi, cutoff=3.0, recursions=5):

    thetaRad = np.deg2rad(theta)
    phiRad = np.deg2rad(phi)

    # Compute direction from angles
    dx = np.sin(thetaRad) * np.cos(phiRad)
    dy = np.sin(thetaRad) * np.sin(phiRad)
    dz = np.cos(thetaRad)

    position = np.array([x, y, z], dtype=np.float)
    direction = np.array([dx, dy, dz], dtype=np.float)

    # Normalize the direction
    directionLength = np.sqrt(np.sum(direction**2))
    direction /= directionLength

    success, px, py, pz = self.trace(position, direction, cutoff**2, recursions)
    if success:
      return (px, py, pz)

    return (x, y, z)


  def trace(self, position, direction, cutoffSquared, recursions):
    """
    Recursive function for finding the cascade location. First the distances from all the atoms to the ion trace are
    checked. If no atoms meet the cutoff criterium, the ion is traced on the cell boundary. If the ion hits a periodic
    boundary, the tracing continues with the shifted ion coordinate. Otherwise the tracing is stopped.
    """
    closest = np.array(3, dtype=np.float)
    closestDistanceSquared = np.float("inf")
    closestFound = False

    for i in range(len(self.xyz.elements)):
      atomPosition = np.array([self.xyz.xs[i], self.xyz.ys[i], self.xyz.zs[i]])
      atomOffset = atomPosition - position

      # Projection vector of the atom position on the ion trace
      projection = np.dot(atomOffset, direction) * direction

      # Shortest vector from the ion trace to the atom
      diff = atomOffset - projection

      # Ignore the atom if the distance from the ion trace is larger than the cutoff radius or
      # if the ion is moving away from the atom
      if np.sum(diff**2) > cutoffSquared or np.dot(direction, atomOffset) < 0:
        continue

      distanceSquared = np.sum(projection**2)
      if distanceSquared < closestDistanceSquared:
        closest = position + projection
        closestDistanceSquared = distanceSquared
        closestFound = True

    if closestFound:
      return True, closest[0], closest[1], closest[2]

    # If this is the last level of recursion, stop!
    if recursions == 1:
      return False, x, y, z

    # Find the face of the cell that the ion track intersects first.
    # First we find all the potential intersections. The sign of hte distance tells if the
    # intersection point is in front on the track.
    intersections = []

    # X-direction
    if direction[0] != 0:
      dx1 = (self.xyz.xCellLimits[0] - position[0]) / direction[0]
      if dx1 >= 0 and direction[0] < 0:
        intersections.append(Intersection(dx1, self.pbcX, self.xyz.boxsize[0], 0, 0))
      dx2 = (self.xyz.xCellLimits[1] - position[0]) / direction[0]
      if dx2 >= 0 and direction[0] > 0:
        intersections.append(Intersection(dx2, self.pbcX, -self.xyz.boxsize[0], 0, 0))

    # Y-direction
    if direction[1] != 0:
      dy1 = (self.xyz.yCellLimits[0] - position[1]) / direction[1]
      if dy1 >= 0 and direction[1] < 0:
        intersections.append(Intersection(dy1, self.pbcY, 0, self.xyz.boxsize[1], 0))
      dy2 = (self.xyz.yCellLimits[1] - position[1]) / direction[1]
      if dy2 >= 0 and direction[1] > 0:
        intersections.append(Intersection(dy2, self.pbcY, 0, -self.xyz.boxsize[1], 0))

    # Z-direction
    if direction[2] != 0:
      dz1 = (self.xyz.zCellLimits[0] - position[2]) / direction[2]
      if dz1 >= 0 and direction[2] < 0:
        intersections.append(Intersection(dz1, self.pbcZ, 0, 0, self.xyz.boxsize[2]))
      dz2 = (self.xyz.zCellLimits[1] - position[2]) / direction[2]
      if dz2 >= 0 and direction[2] > 0:
        intersections.append(Intersection(dz2, self.pbcZ, 0, 0, -self.xyz.boxsize[2]))

    if len(intersections) == 0:
      return False, x, y, z

    
    # Sorting the intersection points to find the first intersection
    intersections = sorted(intersections, key=lambda x: x.distance, reverse=False)

    # If the first intersection is in non-periodic direction, stop the recursion
    if not intesections[0].periodic:
      return False, x, y, z

    # If the intersection is in periodic direction, start a new tracing on the opposite side of the cell
    b = positions + intersections[0].distance * direction # First intersection point
    offset = np.array([intersections[0].shiftX, intersections[0].shiftY, intersections[0].shiftZ])
    return trace(b + offset, direction, cutoffSquared, recursions - 1)



class XyzData():

  def __init__(self):
    self.comment = ""
    self.elements = []
    self.xs = []
    self.ys = []
    self.zs = []
    self.ids = []
    self.types = []
    self.vxs = []
    self.vys = []
    self.vzs = []


  def read(self, filename, stream=None):
    # Parse the first two lines of the data file
    nAtoms = 0
    comment = ""
    with open(filename) as f:
      for i, line in enumerate(f):
        if (i == 0):
          nAtoms = int(line)
        elif (i == 1):
          comment = line
          break

    self.comment = comment

    # Read the data
    elements, xs, ys, zs, types, ids, vxs, vys, vzs = np.loadtxt(filename, skiprows=2, unpack=True, dtype={
      'names': ('element', 'x', 'y', 'z', 'type', 'id', 'vx', 'vy', 'vz'), 
      'formats': ('|U2', np.float, np.float, np.float, np.int, np.int, np.float, np.float, np.float)
      })

    # Assert the file content
    if (len(elements) != nAtoms):
      print("Not enough data points in {}. {} != {}".format(filename, len(elements), nAtoms), file=sys.stderr)
      exit(1)

    self.elements = elements
    self.positions = np.stack((xs, ys, zs), axis=1)
    self.xs = xs
    self.ys = ys
    self.zs = zs
    self.velocities = np.stack((vxs, vys, vzs), axis=1)
    self.vxs = vxs
    self.vys = vys
    self.vzs = vzs
    self.ids = ids
    self.types = types

    # Extract information from the comment line
    commentParts = [x.lower() for x in comment.split()]

    index = commentParts.index("boxsize")
    if index == -1:
      index = commentParts.index("size")
    if index == -1:
      print("Couldn't find `boxsize' or `size' in the comment!", file=sys.stderr)
      exit(2)
    self.boxsize = np.array(commentParts[index+1 : index+4], dtype=np.float)

    index = commentParts.index("pbc")
    if index == -1:
      print("Couldn't find `pbc' in the comment!", file=sys.stderr)
      exit(2)
    self.pbc = np.array(commentParts[index+1 : index+4], dtype=np.int) == 1

    # Assert the inputs
    if len(self.boxsize) != 3 or len(self.pbc) != 3:
      print("Too few values in `boxsize' or `pbc'", file=sys.stderr)
      exit(3)

    extent = np.array([-1, 1])
    self.xLimits = self.boxsize[0]/2 * extent
    self.yLimits = self.boxsize[1]/2 * extent
    self.zLimits = self.boxsize[2]/2 * extent

    self.xAtomLimits = np.array([np.min(xs), np.max(xs)])
    self.yAtomLimits = np.array([np.min(ys), np.max(ys)])
    self.zAtomLimits = np.array([np.min(zs), np.max(zs)])

    if stream != None:
        print("Frame read from file {} : {} atoms".format(filename, nAtoms), file=stream)


  def write(self, filename, stream=None):
    data = np.stack((
        self.elements, self.xs, self.ys, self.zs, 
        self.types, self.ids, 
        self.vxs, self.vys, self.vzs
      ),
      axis=1
    )
    
    outFile = open(filename, "w")
    outFile.write("{}\n".format(len(self.elements)))
    outFile.write("{}".format(self.comment))
    np.savetxt(outFile, data, fmt="%s")
    outFile.close()

    if stream != None:
        print("Frame written to file {} : {} atoms".format(filename, len(self.elements)), file=stream)


class Ion():

  def __init__(self):
    self.activated = False
    self.x = 0.0
    self.y = 0.0
    self.z = 0.0
    self.theta = 0.0
    self.phi = 0.0
    self.energy = 0.0


class Data():

  def __init__(self):
    self.xyz = XyzData()
    self.ion = Ion()
    self.pbcX = False
    self.pbcY = False
    self.pbcZ = False


class ActionHandler():

  def __init__(self, data):
    self.data = data

  def handleActions(self, config, stream=None):
    with open(config) as f:
      for line in f:
        a = self.parseAction(line)
        if (not a.valid):
          return False
        if (not a.performAction(self.data, stream)):
          return False
    return True

  def parseAction(self, line):
    # No command found
    if len(line) == 0:
      return NoneAction(True)

    # Comment
    if line.startswith("#"):
      return NoneAction(True)

    lineParts = line.split()
    command = lineParts[0]

    action = None

    if command == "shift":
      action = ShiftAction()
    elif command == "periodic":
      action = PeriodicAction()
    elif command == "remove_sputtered":
      action = RemoveSputteredAction()
    elif command == "center_cascade":
      action = CenterCascadeAction()
    elif command == "ion":
      action = IonAction()
    elif command == "merge_type":
      action = MergeTypeAction()
    else:
      return NoneAction(False)

    action.valid = action.setArguments(lineParts[1:])

    return action
    

class Action():

  def __init__(self):
    self.valid = False
  
  def setArguments(self, args):
    return False

  def performAction(self, data, stream=None):
    return False


class NoneAction(Action):

  def __init__(self, valid):
    self.valid = valid

  def setArguments(self, args):
    return True

  def performAction(self, data, stream=None):
    return True


class ShiftAction(Action):

  def __init__(self):
    self.shiftX = 0.0
    self.shiftY = 0.0
    self.shiftZ = 0.0

  def setArguments(self, args):
    if len(args) < 3:
      return False

    self.shiftX = float(args[0])
    self.shiftY = float(args[1])
    self.shiftZ = float(args[2])

    return True

  def performAction(self, data, stream=None):
    data.xyz.xs += self.shiftX
    data.xyz.ys += self.shiftY
    data.xyz.zs += self.shiftZ

    def shiftHelper(x, limits, size):
      # Shift left
      while True:
        indices = x > limits[1]
        if np.count_nonzero(indices) == 0:
          break
        x[indices] -= size

      # Shift right
      while True:
        indices = x <= limits[0]
        if np.count_nonzero(indices) == 0:
          break
        x[indices] += size

      return x
    
    # If periodic boundary conditions are enabled, shift the atoms by boxsize[x/y/z] if the
    # coordinate is outside the limits
    if data.pbcX:
      data.xyz.xs = shiftHelper(data.xyz.xs, data.xyz.xLimits, data.xyz.boxsize[0])
    
    if data.pbcY:
      data.xyz.ys = shiftHelper(data.xyz.ys, data.xyz.yLimits, data.xyz.boxsize[1])

    if data.pbcZ:
      data.xyz.zs = shiftHelper(data.xyz.zs, data.xyz.zLimits, data.xyz.boxsize[2])
      

    # Shift the ion in the same way.
    if data.ion.activated:
      data.ion.x += self.shiftX
      data.ion.y += self.shiftY
      data.ion.z += self.shiftZ

      if data.pbcX:
        data.ion.x = shiftHelper(data.ion.x, data.xyz.xLimits, data.xyz.boxsize[0])

      if data.pbcY:
        data.ion.y = shiftHelper(data.ion.y, data.xyz.yLimits, data.xyz.boxsize[1])

      if data.pbcZ:
        data.ion.z = shiftHelper(data.ion.z, data.xyz.zLimits, data.xyz.boxsize[2])

    if stream != None:
      print("Cell shifted: {} {} {}".format(self.shiftX, self.shiftY, self.shiftZ), file=stream)

    return True
    

class PeriodicAction(Action):

  def setArguments(self, args):
    if len(args) < 3:
      print("Too few arguments")
      return False

    self.pbcX = int(args[0]) != 0
    self.pbcY = int(args[1]) != 0
    self.pbcZ = int(args[2]) != 0

    return True

  def performAction(self, data, stream=None):
    data.pbcX = self.pbcX
    data.pbcY = self.pbcY
    data.pbcZ = self.pbcZ

    if stream != None:
      print("Periodic boundaries set: {} {} {}".format(1 if self.pbcX else 0, 1 if self.pbcY else 0, 1 if self.pbcZ else 0), file=stream)

    return True


class RemoveSputteredAction(Action):

  def __init__(self):
    self.radius = 3.0
  
  def setArguments(self, args):
    if len(args) > 0:
      self.radius = float(args[0])
    return True

  def performAction(self, data, stream=None):
    analyzer = ClusterAnalyzer(data.xyz, data.pbcX, data.pbcY, data.pbcZ)
    analyzer.analyze(self.radius)

    nBefore = len(data.xyz.elements)
    data.xyz = analyzer.largestCluster()
    nAfter = len(data.xyz.elements)

    if stream != None:
      print("Sputtered atoms removed with cutoff radius[A] {} : {} atoms removed, {} atoms remaining".format(self.radius, (nBefore - nAfter), nAfter), file=stream)

    return True


class CenterCascadeAction(Action):
  
  def setArguments(self, args):
    self.radius = 2.0
    self.recursions = 5

    if len(args) > 0:
      self.radius = float(args[0])
    if len(args) > 1:
      self.recursions = int(args[1])
    return True

  def performAction(self, data, stream=None):
    predictor = CascadeLocationPredictor(data.xyz, data.pbcX, data.pbcY, data.pbcZ)
    x, y, z = predictor.predict(data.ion.x, data.ion.y, data.ion.z, data.ion.theta, data.ion.phi,
      self.radius, self.recursions)
    
    if stream != None:
      print("Cascade location predicted: ({}, {}, {})".format(x, y, z), file=stream)

    shift = ShiftAction()
    if (data.pbcX):
      shift.shiftX = -x
      shift.shiftY = -y
      shift.shiftZ = -z

    shift.performAction(data, stream)

    return True


class IonAction(Action):

  def __init__(self):
    super().__init__()
    self.ion = Ion()
  
  def setArguments(self, args):
    if len(args) < 5:
      return False

    self.ion.x = float(args[0])
    self.ion.y = float(args[1])
    self.ion.z = float(args[2])
    self.ion.theta = float(args[3])
    self.ion.phi = float(args[4])
    if len(args) > 5:
      ion.energy = float(args[5])

    return True

  def performAction(self, action, stream=None):
    data.ion = self.ion

    def shiftHelper(x, limits, size):
      # Shift left
      while True:
        indices = x > limits[1]
        if np.count_nonzero(indices) == 0:
          break
        x[indices] -= size

      # Shift right
      while True:
        indices = x <= limits[0]
        if np.count_nonzero(indices) == 0:
          break
        x[indices] += size

      return x

    if data.pbcX:
      data.ion.x = shiftHelper(data.ion.x, data.xyz.xLimits, data.xyz.boxsize[0])

    if data.pbcY:
      data.ion.y = shiftHelper(data.ion.y, data.xyz.yLimits, data.xyz.boxsize[1])

    if data.pbcZ:
      data.ion.z = shiftHelper(data.ion.z, data.xyz.zLimits, data.xyz.boxsize[2])

    data.ion.activated = True

    if stream != None:
      print("Ion set: position[A] = ({}, {}, {}), theta[deg] = {}, phi[deg] = {}, energy[eV] = {}".format(self.ion.x, self.ion.y, self.ion.z, self.ion.theta, self.ion.phi, self.ion.energy), file=stream)
      dist = np.sqrt((self.ion.x - data.ion.x)**2 + (self.ion.y - data.ion.y)**2 + (self.ion.z - data.ion.z)**2)
      if dist > 1e-6:
        print("Ion moved over periodic boundaries. New location: ({}, {}, {})".format(data.ion.x, data.ion.y, data.ion.z), file=stream)
      
    return True
    

class MergeTypeAction(Action):
  
  def setArguments(self, args):
    if len(args) < 2:
      return False

    self.fromType = int(args[0])
    self.toType = int(args[1])

    return True

  def performAction(self, data, stream=None):
    elementList = np.unique(data.xyz.elements[np.where(data.xyz.types == self.toType)])
    if len(elementList) == 0:
      return False

    index = data.xyz.types == self.fromType
    data.xyz.elements[index] = elementList[0]

    if stream != None:
        print("{} atoms of type {} merged to type {}".format(np.count_nonzero(index), self.fromType, self.toType), file=stream)
    
    return True



if len(sys.argv) < 3:
    print("Too few arguments:", sys.argv, file=sys.stderr)
    exit(1)

config = sys.argv[1]
infile = sys.argv[2]
outfile = sys.argv[3]


data = Data()

#data.xyz.read(args.infile, sys.stdout)
data.xyz.read(infile, sys.stdout)

actionHandler = ActionHandler(data)
#if not actionHandler.handleActions(args.config, sys.stdout):
if not actionHandler.handleActions(config, sys.stdout):
  print("Failed!")

#data.xyz.write(args.outfile, sys.stdout)
data.xyz.write(outfile, sys.stdout)

if data.ion.activated:
  print("Ion position: {} {} {}".format(data.ion.x, data.ion.y, data.ion.z), file=sys.stdout)

