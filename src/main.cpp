/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "api/api.h"
#include "gui/mainwindow.h"

#include "api/potentiallist.h"
#include "api/programsettings.h"

#include <QApplication>
#include <QDebug>
#include <QStringList>
#include <QSurfaceFormat>


void customMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg) {
    QByteArray localMsg = msg.toLocal8Bit();
    const char* file = context.file ? context.file : "";
    const char* function = context.function ? context.function : "";
    switch (type) {
        case QtDebugMsg:
            fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
            break;
        case QtInfoMsg:
            fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
            break;
        case QtWarningMsg:
            fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
            break;
        case QtCriticalMsg:
            fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
            break;
        case QtFatalMsg:
            fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
            break;
    }
}


/*!
 * Initialize application-wide settings. E.g. Application Name, Organization Name, etc.
 */
void initApplication() {
    QCoreApplication::setOrganizationName("acclab");
    QCoreApplication::setApplicationName("cosirma");
    QCoreApplication::setApplicationVersion("v1.0.2");

    qInfo() << "Executable path: " << QCoreApplication::applicationDirPath();
    qInfo() << "Organization:    " << QCoreApplication::organizationName();
    qInfo() << "Application:     " << QCoreApplication::applicationName();
    qInfo() << "Version:         " << QCoreApplication::applicationVersion();
}


int main(int argc, char* argv[]) {
    //! This is only needed if the resources are loaded dynamically. In the current application we include everything
    //! into the executable.
    // Q_INIT_RESOURCE(application);

    qInstallMessageHandler(customMessageHandler);

    QApplication a(argc, argv);

    QSurfaceFormat format;
    format.setMajorVersion(3);
    format.setMinorVersion(3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setOption(QSurfaceFormat::DebugContext);
    QSurfaceFormat::setDefaultFormat(format);

    initApplication();

    /*
     * Use the argument parser if arguments are needed.
     *     http://doc.qt.io/qt-5/qcommandlineparser.html
     */

    Api api;
    MainWindow w(&api);
    w.show();
    return a.exec();
}
