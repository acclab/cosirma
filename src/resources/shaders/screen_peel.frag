#version 330

out vec4 fragColor;

in vec2 texCoords;

uniform sampler2D colorTexture;

void main()
{
    fragColor = texture(colorTexture, texCoords);
}
