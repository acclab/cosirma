#version 330

in vec4 color;
uniform sampler2D textureSampler;
uniform sampler2D depthSampler;

out vec4 colorOut;

void main(void)
{
    float clipDepth = texelFetch(depthSampler, ivec2(gl_FragCoord.xy), 0).r;
    if (gl_FragCoord.z <= clipDepth) {
        discard;
    }

    vec4 col = texture(textureSampler, gl_PointCoord) * color;
    if (col.a > 0) {
        vec2 shifted_coords = gl_PointCoord - vec2(0.5, 0.5);
        if(dot(shifted_coords, shifted_coords) >= 0.22) discard;
        colorOut = vec4(col.rgb * col.a, col.a);
    } else {
        discard;
    }
}
