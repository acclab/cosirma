#version 330

in vec4 color;

out vec4 colorOut;

void main(void)
{
    colorOut = vec4(color.rgb, 1.0);
//    colorOut = vec4(1.0, 0.0, 0.0, 1.0);
}

