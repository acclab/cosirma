/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TESTENTRYCHANGES_H
#define TESTENTRYCHANGES_H

#include <QDialog>

#include "api/entry.h"


namespace Ui {
class TestEntryChanges;
}

class TestEntryChanges : public QDialog {
    Q_OBJECT

  public:
    explicit TestEntryChanges(QWidget* parent = nullptr);
    ~TestEntryChanges();

  private slots:
    void on_pushButton_createEntry_clicked();

    void on_pushButton_addFrame_clicked();

  private:
    Ui::TestEntryChanges* ui;

    Entry* m_entry = nullptr;
};

#endif  // TESTENTRYCHANGES_H
