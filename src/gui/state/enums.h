/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef GUI_ENUMS_H
#define GUI_ENUMS_H

namespace gui {
namespace enums {

namespace state {
/**
 * @brief The Status enum is used to indicate issues with the current state in the GUI.
 */
enum Status {
    None = 0,       //!< \brief No errors without icon
    Ok = 1,         //!< \brief No errors with icon
    Modified = 2,   //!< \brief Unsaved modifications
    Attention = 3,  //!< \brief Attention with icon
    Warning = 4,    //!< \brief Minor errors with icon
    Error = 5       //!< \brief Fatal errors with icon
};
}  // namespace state

}  // namespace enums
}  // namespace gui

#endif  // GUI_ENUMS_H
