/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "particlemodel.h"

#include "api/util.h"

#include "gui/rendering/particlemesh.h"


ParticleModel::ParticleModel(const XyzData& frame, const QHash<int, QColor>& colors, const QHash<int, float>& radii,
                             QObject* parent)
        : AbstractModel(parent) {
    m_frame = &frame;

    m_typeColors = colors;
    m_typeRadii = radii;

    m_particleMesh = new ParticleMesh();

    setupVertexData();
}


void ParticleModel::setupVertexData() {
    std::size_t size = static_cast<std::size_t>(m_frame->atoms.count());

    std::vector<glm::vec3> positions(size);
    std::vector<glm::vec4> colors(size);
    std::vector<float> radii(size);

    for (std::size_t i = 0; i < size; ++i) {
        int ii = static_cast<int>(i);
        const Atom& a = m_frame->atoms[ii];
        positions[i] = glm::vec3(a.position);
        radii[i] = m_typeRadii[a.type];
        colors[i] = util::colorToVector(m_typeColors[a.type]);
    }

    m_particleMesh->setPositions(positions);
    m_particleMesh->setColors(colors);
    m_particleMesh->setRadii(radii);

    updateIndices();
}

void ParticleModel::updateIndices() {
    std::size_t size = static_cast<std::size_t>(m_frame->atoms.size());
    std::vector<unsigned int> indices(size);
    std::size_t count = 0;
    for (unsigned int i = 0; i < size; ++i) {
        int ii = static_cast<int>(i);
        if (m_typeHidden[m_frame->atoms[ii].type]) continue;
        indices[count++] = i;
    }
    indices.resize(count);
    m_particleMesh->setIndices(indices);
}


void ParticleModel::draw(const glm::mat4& model, QOpenGLShaderProgram& shader) {
    shader.setUniformValue("m", QMatrix4x4(glm::value_ptr(model * modelMatrix())).transposed());

    if (!m_particleMesh->isInitialized()) m_particleMesh->initialize();
    m_particleMesh->draw(shader);
}


void ParticleModel::on_atomsUpdated(const QHash<int, QColor>& colors, const QHash<int, float>& radii) {
    m_typeColors = colors;
    m_typeRadii = radii;

    setupVertexData();
}


void ParticleModel::on_hideType(int type, bool hide) {
    m_typeHidden[type] = hide;
    updateIndices();
}


void ParticleModel::on_setColors(const QHash<int, QColor>& typeColors) {
    m_typeColors = typeColors;
    setupVertexData();
}


void ParticleModel::on_setTypeRadius(int type, float radius) {
    m_typeRadii[type] = radius;
    setupVertexData();
}


void ParticleModel::on_setRadii(const QHash<int, float>& typeRadii) {
    m_typeRadii = typeRadii;
    setupVertexData();
}
