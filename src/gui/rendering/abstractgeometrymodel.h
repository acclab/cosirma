/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ABSTRACTGEOMETRYMODEL_H
#define ABSTRACTGEOMETRYMODEL_H

#include "gui/rendering/abstractmodel.h"

#include <QObject>


class LineMesh;
class TriangleMesh;

class AbstractGeometryModel : public AbstractModel {
    Q_OBJECT

  public:
    explicit AbstractGeometryModel(QObject* parent = nullptr);
    virtual ~AbstractGeometryModel() override;

    virtual void draw(const glm::mat4& model, QOpenGLShaderProgram& shader) override;

    void setLineVertexData(const std::vector<glm::vec3>& positions, const std::vector<glm::vec4>& colors,
                           const std::vector<unsigned int>& indices);

    void setFaceVertexData(const std::vector<glm::vec3>& positions, const std::vector<glm::vec4>& colors,
                           const std::vector<unsigned int>& indices);

    bool showFaces();
    bool showLines();

  private:
    LineMesh* m_lineMesh = nullptr;
    TriangleMesh* m_faceMesh = nullptr;

    bool m_showLines;
    bool m_showFaces;

  public slots:
    void setShowFaces(bool show);
    void setShowLines(bool show);
};

#endif  // ABSTRACTGEOMETRYMODEL_H
