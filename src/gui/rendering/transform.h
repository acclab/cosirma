/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <glm.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/transform.hpp>


class Transform {
  public:
    Transform() : m_position(0), m_rotation(0), m_scale(1) {}


    glm::mat4 modelMatrix() const {
        glm::mat4 model(1.0);
        model = glm::translate(model, m_position);
        model = glm::rotate(model, m_rotation.z, glm::vec3(0, 0, 1));
        model = glm::rotate(model, m_rotation.y, glm::vec3(0, 1, 0));
        model = glm::rotate(model, m_rotation.x, glm::vec3(1, 0, 0));
        model = glm::scale(model, m_scale);
        return model;
    }


    template <typename T> void setPosition(T x, T y, T z) { setPosition(glm::vec3(x, y, z)); }
    template <typename T> void setPosition(const T& position) { m_position = glm::vec3(position); }


    template <typename T> void translate(T x, T y, T z) { translate(glm::vec3(x, y, z)); }
    template <typename T> void translate(const T& translation) { m_position += glm::vec3(translation); }


    template <typename T> void rotate(T x, T y, T z) { rotate(glm::vec3(x, y, z)); }
    template <typename T> void rotate(const T& rotation) { m_rotation = glm::vec3(rotation); }


    template <typename T> void setScale(T x, T y, T z) { setScale(glm::vec3(x, y, z)); }
    template <typename T> void setScale(const T& scale) { m_scale = glm::vec3(scale); }

  private:
    glm::vec3 m_position;
    glm::vec3 m_rotation;
    glm::vec3 m_scale;
};

#endif  // TRANSFORM_H
