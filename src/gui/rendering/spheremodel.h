/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SPHEREMODEL_H
#define SPHEREMODEL_H

#include "api/geometries.h"

#include "gui/rendering/abstractgeometrymodel.h"

#include <QColor>
#include <QObject>


class TriangleMesh;
class LineMesh;

class SphereModel : public AbstractGeometryModel {
    Q_OBJECT

  public:
    explicit SphereModel(const Sphere& sphere, QColor faceColor, QColor lineColor, unsigned int resolution,
                         QObject* parent = nullptr);

  private:
    void setupVertexData();
    void divideIcosphere();
    glm::vec3 computeMiddlePoints(const glm::vec3 v1, const glm::vec3 v2);

    unsigned int m_resolution;

    QColor m_faceColor;
    QColor m_lineColor;

    std::vector<glm::vec3> m_positions;
    std::vector<unsigned int> m_faceIndices;
    std::vector<unsigned int> m_lineIndices;

  public slots:
    void setSphere(const Sphere& sphere);

    void setFaceColor(const QColor& color);
    void setLineColor(const QColor& color);
};

#endif  // SPHEREMODEL_H
