/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef CYLINDERMODEL_H
#define CYLINDERMODEL_H

#include "api/geometries.h"
#include "gui/rendering/abstractgeometrymodel.h"
#include "gui/rendering/linemesh.h"
#include "gui/rendering/trianglemesh.h"

#include <QObject>


class CylinderModel : public AbstractGeometryModel {
    Q_OBJECT

  public:
    explicit CylinderModel(const Cylinder& cylinder, const QColor& faceColor, const QColor& lineColor,
                           unsigned int resolution, bool wireframe, QObject* parent = nullptr);

  private:
    void setupVertexData();

    Cylinder m_cylinder;
    QColor m_faceColor;
    QColor m_lineColor;
    unsigned int m_resolution;
    bool m_wireframe;

  public slots:
    void setCylinder(const Cylinder& cylinder);

    void setFaceColor(const QColor& color);
    void setLineColor(const QColor& color);
};

#endif  // CYLINDERMODEL_H
