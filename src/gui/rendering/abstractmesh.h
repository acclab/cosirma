/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ABSTRACTMESH_H
#define ABSTRACTMESH_H

#include <QColor>
#include <QObject>
#include <QOpenGLBuffer>
#include <QOpenGLWidget>

#include <glm.hpp>


class QOpenGLShaderProgram;
class QOpenGLVertexArrayObject;

class AbstractMesh : public QObject {
    Q_OBJECT

  public:
    explicit AbstractMesh(QObject* parent = nullptr);
    explicit AbstractMesh(const std::vector<glm::vec3>& positions, const std::vector<glm::vec4>& colors,
                          const std::vector<unsigned int>& indices, QObject* parent = nullptr);
    virtual ~AbstractMesh();

    virtual void initialize();
    virtual void setupVertexBufferObjects();
    virtual void setupVertexArrayObject();

    virtual void draw(QOpenGLShaderProgram& shader) = 0;

    void setPositions(const std::vector<glm::vec3>& positions);
    void setColors(const std::vector<glm::vec4>& colors);
    void setIndices(const std::vector<unsigned int>& indices);

    const std::vector<unsigned int>& indices();
    int indexCount();

    bool isInitialized() { return m_vao != nullptr; }

  protected:
    template <typename T> void updateVertexBufferObject(QOpenGLBuffer* vbo, const std::vector<T>& data) {
        if (!vbo) return;
        vbo->bind();
        vbo->allocate(data.data(), data.size() * sizeof(T));
        vbo->release();
    }

    QOpenGLVertexArrayObject* m_vao = nullptr;
    QOpenGLBuffer* m_positionVbo = nullptr;
    QOpenGLBuffer* m_colorVbo = nullptr;
    QOpenGLBuffer* m_indexVbo = nullptr;

  private:
    std::vector<glm::vec3> m_positions;
    std::vector<glm::vec4> m_colors;
    std::vector<unsigned int> m_indices;
};

#endif  // ABSTRACTMESH_H
