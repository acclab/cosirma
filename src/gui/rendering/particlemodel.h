/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PARTICLEMODEL_H
#define PARTICLEMODEL_H

#include "api/xyzdata.h"
#include "gui/rendering/abstractmodel.h"

#include <QColor>
#include <QObject>


class ParticleMesh;

class ParticleModel : public AbstractModel {
    Q_OBJECT
  public:
    explicit ParticleModel(const XyzData& frame, const QHash<int, QColor>& colors, const QHash<int, float>& radii,
                           QObject* parent = nullptr);

    void draw(const glm::mat4& model, QOpenGLShaderProgram& shader) override;

  private:
    void setupVertexData();
    void updateIndices();

    const XyzData* m_frame = nullptr;

    //! \todo Maybe these are redundant here?
    QHash<int, QColor> m_typeColors;
    QHash<int, float> m_typeRadii;
    QHash<int, bool> m_typeHidden;

    ParticleMesh* m_particleMesh = nullptr;

  public slots:
    void on_atomsUpdated(const QHash<int, QColor>& colors, const QHash<int, float>& radii);
    void on_hideType(int type, bool hide);
    void on_setColors(const QHash<int, QColor>& typeColors);
    void on_setTypeRadius(int type, float radius);
    void on_setRadii(const QHash<int, float>& typeRadii);
};

#endif  // PARTICLEMODEL_H
