/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "cylindermodel.h"

#include "api/util.h"


CylinderModel::CylinderModel(const Cylinder& cylinder, const QColor& faceColor, const QColor& lineColor,
                             unsigned int resolution, bool wireframe, QObject* parent)
        : AbstractGeometryModel(parent) {
    m_cylinder = cylinder;
    m_faceColor = faceColor;
    m_lineColor = lineColor;
    m_resolution = resolution;
    m_wireframe = wireframe;

    setupVertexData();

    setCylinder(cylinder);
}


void CylinderModel::setupVertexData() {

    std::vector<glm::vec3> positions;
    std::vector<unsigned int> faceIndices;
    std::vector<unsigned int> lineIndices;

    // End points
    positions.push_back(glm::vec3(0, 0, 0.5));
    positions.push_back(glm::vec3(0, 0, -0.5));
    // Two circles
    for (unsigned int i = 0; i < m_resolution; ++i) {
        float theta = (2 * glm::pi<float>() * i) / m_resolution;
        positions.push_back(glm::vec3(std::cos(theta), std::sin(theta), 0.5));
        positions.push_back(glm::vec3(std::cos(theta), std::sin(theta), -0.5));
    }

    unsigned int i1, i2, i3;

    // Loop over all points in one of the circles
    for (unsigned int i = 0; i < m_resolution; ++i) {
        i1 = 0;          // Center is fixed
        i2 = i * 2 + 2;  // Move around the circle
        i3 = i * 2 + 4;  // by adding every second
        if (i3 >= positions.size()) {
            i3 = 2;
        }
        // top circle (even indices)
        faceIndices.push_back(i1);
        faceIndices.push_back(i2);
        faceIndices.push_back(i3);
        // top circle lines
        lineIndices.push_back(i2);
        lineIndices.push_back(i3);

        // bottom circle (odd indices)
        faceIndices.push_back(i1 + 1);
        faceIndices.push_back(i3 + 1);
        faceIndices.push_back(i2 + 1);
        // bottom circle lines
        lineIndices.push_back(i2 + 1);
        lineIndices.push_back(i3 + 1);

        // side faces
        faceIndices.push_back(i2);
        faceIndices.push_back(i2 + 1);
        faceIndices.push_back(i3 + 1);
        faceIndices.push_back(i3 + 1);
        faceIndices.push_back(i3);
        faceIndices.push_back(i2);

        if (m_wireframe) {
            // side lines
            lineIndices.push_back(i2);
            lineIndices.push_back(i2 + 1);
        }
    }


    if (m_wireframe) {
        for (unsigned int i = 0; i < m_resolution / 4; ++i) {
            i1 = i * 2 + 2;
            i2 = m_resolution + 2 - (i * 2);

            lineIndices.push_back(i1);
            lineIndices.push_back(i2);
            lineIndices.push_back(i1 + m_resolution / 2);
            lineIndices.push_back(i2 + m_resolution / 2);
            lineIndices.push_back(i1 + m_resolution);
            lineIndices.push_back(i2 + m_resolution);
            lineIndices.push_back(2 * m_resolution - i1 + 4);
            lineIndices.push_back(i1);


            lineIndices.push_back(i1 + 1);
            lineIndices.push_back(i2 + 1);
            lineIndices.push_back(i1 + m_resolution / 2 + 1);
            lineIndices.push_back(i2 + m_resolution / 2 + 1);
            lineIndices.push_back(i1 + m_resolution + 1);
            lineIndices.push_back(i2 + m_resolution + 1);
            lineIndices.push_back(2 * m_resolution - i1 + 4 + 1);
            lineIndices.push_back(i1 + 1);
        }
    }


    std::vector<glm::vec4> faceColors(positions.size());
    std::fill(std::begin(faceColors), std::end(faceColors), util::colorToVector(m_faceColor));

    std::vector<glm::vec4> lineColors(positions.size());
    std::fill(std::begin(lineColors), std::end(lineColors), util::colorToVector(m_lineColor));


    setLineVertexData(positions, lineColors, lineIndices);
    setFaceVertexData(positions, faceColors, faceIndices);
}


void CylinderModel::setCylinder(const Cylinder& cylinder) {
    m_cylinder = cylinder;

    glm::vec3 direction = cylinder.axis;
    glm::vec3 zDirection = glm::vec3(0, 0, 1);
    glm::vec3 rotationAxis = (direction == zDirection) ? zDirection : glm::cross(zDirection, direction);
    float rotationAngle =
        std::acos(glm::dot(zDirection, direction) / (glm::length(zDirection) * glm::length(direction)));

    transform.setPosition(cylinder.center);
    transform.rotate(rotationAngle * rotationAxis);
    transform.setScale(glm::vec3(m_cylinder.radius, m_cylinder.radius, m_cylinder.length));
}


void CylinderModel::setFaceColor(const QColor& color) {
    m_faceColor = color;
    setupVertexData();
}


void CylinderModel::setLineColor(const QColor& color) {
    m_lineColor = color;
    setupVertexData();
}
