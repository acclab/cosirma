/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "dialogexportsimulation.h"
#include "ui_dialogexportsimulation.h"

#include <QDir>
#include <QFileDialog>
#include <QThread>


DialogExportSimulation::DialogExportSimulation(QWidget* parent) : QDialog(parent), ui(new Ui::DialogExportSimulation) {
    ui->setupUi(this);

    ui->pageComboBox->addItem(tr("Simple"));
    ui->pageComboBox->addItem(tr("Slurm"));
    connect(ui->pageComboBox, QOverload<int>::of(&QComboBox::activated), ui->stackedWidget,
            &QStackedWidget::setCurrentIndex);

    ui->lineEdit_saveLocation->setText(QDir::homePath() + "/cosirma/standalone/");
    ui->spinBox_simple_numberOfCores->setValue(QThread::idealThreadCount());
}


DialogExportSimulation::~DialogExportSimulation() {
    delete ui;
}


const ExportData& DialogExportSimulation::data() {
    int index = ui->pageComboBox->currentIndex();

    m_exportData.path = ui->lineEdit_saveLocation->text();
    m_exportData.parcasPath = ui->lineEdit_parcas->text();
    m_exportData.description = ui->plainTextEdit_description->document()->toPlainText();

    if (index == 0) {
        m_exportData.type = ExportData::Simple;
        if (ui->groupBox_simple_mpi->isChecked()) {
            m_exportData.parameters.insert("parallel_exe",
                                           QString("mpirun -np %1").arg(ui->spinBox_simple_numberOfCores->value()));
        } else {
            m_exportData.parameters.insert("parallel_exe", "");
        }
    } else if (index == 1) {
        m_exportData.type = ExportData::Slurm;
        m_exportData.parameters.insert("parallel_exe", "srun");

        QString slurmData = "#!/bin/bash -l\n\n";
        slurmData += ui->plainTextEdit_slurmCommands->document()->toPlainText() + "\n\n";
        slurmData += "source run-simulation.sh\n\n";
        m_exportData.files.insert("submit-slurm.sh", slurmData);
    }

    // Append the description as a file for writing
    if (!m_exportData.description.isEmpty()) {
        m_exportData.files.insert("description.dat", m_exportData.description);
    }

    return m_exportData;
}


void DialogExportSimulation::on_pushButton_clicked() {
    QString currentPath = ui->lineEdit_saveLocation->text();
    QString path = QDir::currentPath();
    if (QFile(currentPath).exists()) {
        path = currentPath;
    }
    QString filename = QFileDialog::getOpenFileName(this, "Save location of the simulation", path);
    if (!filename.isEmpty()) {
        ui->lineEdit_saveLocation->setText(filename);
    }
}
