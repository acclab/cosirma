/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "potentialfiledialog.h"
#include "ui_potentialfiledialog.h"

#include <limits>


/*!
 * \brief Constructs an object that ís connected to a state object.
 * \param s The state object
 * \param parent The parent widget
 */
PotentialFileDialog::PotentialFileDialog(QHash<ElementPair, DataFile<ReppotFileData>> potentialFiles, QWidget* parent)
        : QDialog(parent), ui(new Ui::PotentialFileDialog) {
    ui->setupUi(this);

    m_potentialFiles = potentialFiles;

    ui->table_pair->setTextElideMode(Qt::ElideLeft);
    ui->table_pair->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->table_pair->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->table_pair->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->table_pair->setWordWrap(false);

    ui->table_eam->setTextElideMode(Qt::ElideLeft);
    ui->table_eam->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->table_eam->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->table_eam->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->table_eam->setWordWrap(false);

    ui->plotWidget->addGraph();
    ui->plotWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
    ui->plotWidget->xAxis->setLabel("r [Å]");
    ui->plotWidget->yAxis->setLabel("V [eV]");

    ui->pushButton_replace->setEnabled(false);
    ui->pushButton_create->setEnabled(false);

    QStandardItemModel* pairModel = new QStandardItemModel(0, 3, this);
    pairModel->setHorizontalHeaderItem(0, new QStandardItem(QString("Element 1")));
    pairModel->setHorizontalHeaderItem(1, new QStandardItem(QString("Element 2")));
    pairModel->setHorizontalHeaderItem(2, new QStandardItem(QString("Path")));
    ui->table_pair->setModel(pairModel);

    connect(ui->table_pair->selectionModel(), &QItemSelectionModel::selectionChanged, [this] {
        QModelIndexList selectedRows = ui->table_pair->selectionModel()->selectedRows();
        if (selectedRows.count() != 0) {
            int row = selectedRows.front().row();
            m_selectedPair = &m_shownPairPairs[row];

            if (row < m_shownPairPairs.count()) {
                if (m_potentialFiles.contains(*m_selectedPair)) {
                    plotFile(m_potentialFiles[*m_selectedPair]);
                    if (m_potentialFiles[*m_selectedPair].hasData()) {
                        ui->pushButton_replace->setText("Replace");
                    } else {
                        ui->pushButton_replace->setText("Import");
                    }
                }
                ui->pushButton_replace->setEnabled(true);
                ui->pushButton_create->setEnabled(true);
            }
        } else {
            m_selectedPair = nullptr;

            ui->pushButton_replace->setText("Replace");
            ui->pushButton_replace->setEnabled(false);
            ui->pushButton_create->setEnabled(false);
        }
    });


    QStandardItemModel* eamModel = new QStandardItemModel(0, 3, this);
    eamModel->setHorizontalHeaderItem(0, new QStandardItem(QString("Element 1")));
    eamModel->setHorizontalHeaderItem(1, new QStandardItem(QString("Element 2")));
    eamModel->setHorizontalHeaderItem(2, new QStandardItem(QString("Path")));
    ui->table_eam->setModel(eamModel);

    //    connect(ui->eamTable->selectionModel(), &QItemSelectionModel::selectionChanged, [this] {
    //        //        std::unique_ptr<PotentialFile::PairAndType> selected = currentlySelectedPair();
    //        //        if (selected) {
    //        ////            plotFile(selected);
    //        //            ui->replaceSelectedPB->setEnabled(true);
    //        //        } else {
    //        //            ui->replaceSelectedPB->setEnabled(false);
    //        //        }
    //    });


    ui->tableTabWidget->setCurrentIndex(0);

    connect(ui->pushButton_replace, &QPushButton::clicked, this, &PotentialFileDialog::replaceSelected);
    connect(ui->pushButton_create, &QPushButton::clicked, this, &PotentialFileDialog::createSelected);

    connect(ui->pushButton_ok, &QPushButton::clicked, this, &PotentialFileDialog::accept);

    updateTables();
}


PotentialFileDialog::~PotentialFileDialog() {
    delete ui;
}


const QHash<ElementPair, DataFile<ReppotFileData>>& PotentialFileDialog::potentialFiles() {
    return m_potentialFiles;
}


/*!
 * \brief Updates the file path tables.
 * \param files Map from element pair and potential type to the file name. Includes the file names that will be shown.
 */
void PotentialFileDialog::updateTables() {

    QStandardItemModel* pairModel = static_cast<QStandardItemModel*>(ui->table_pair->model());
    QStandardItemModel* eamModel = static_cast<QStandardItemModel*>(ui->table_eam->model());

    QSignalBlocker b1(ui->table_pair);
    QSignalBlocker b2(ui->table_eam);

    pairModel->setRowCount(m_potentialFiles.count());
    eamModel->setRowCount(m_potentialFiles.count());

    m_shownPairPairs.clear();
    m_shownEamPairs.clear();

    int pairNonexisting = 0;
    int eamNonexisting = 0;


    QBrush backgroundBrush = QBrush(QColor(255, 190, 200));
    QBrush foregroundBrush = QBrush(Qt::red);

    //! \todo Here it would be nice with a sorted list, but currently the ElementPair::lessthan operator isn't working
    //! properly.
    for (const ElementPair& pair : m_potentialFiles.keys()) {
        const DataFile<ReppotFileData>& file = m_potentialFiles[pair];

        int rowIndex = m_shownPairPairs.count();

        QStandardItem* elementItem1 = new QStandardItem(pair.element1);
        QStandardItem* elementItem2 = new QStandardItem(pair.element2);
        QStandardItem* pathItem = new QStandardItem(file.filepath());

        if (!file.hasData()) {
            ++pairNonexisting;

            elementItem1->setBackground(backgroundBrush);
            elementItem1->setForeground(foregroundBrush);
            elementItem2->setBackground(backgroundBrush);
            elementItem2->setForeground(foregroundBrush);
            pathItem->setBackground(backgroundBrush);
            pathItem->setForeground(foregroundBrush);
        }

        pairModel->setItem(rowIndex, 0, elementItem1);
        pairModel->setItem(rowIndex, 1, elementItem2);
        pairModel->setItem(rowIndex, 2, pathItem);

        m_shownPairPairs.push_back(pair);

        if (m_selectedPair && *m_selectedPair == pair) {
            ui->table_pair->selectRow(rowIndex);
        }
    }

    // Updating the status icons
    auto updateTab = [this](QStandardItemModel* model, int count, int countNonexisting, int index) {
        model->setRowCount(count);
        QIcon icon;
        if (count > 0) {
            if (countNonexisting == 0)
                icon.addFile(":/icons/check-circle-green", QSize(10, 10));
            else
                icon.addFile(":/icons/times-circle-red", QSize(10, 10));
        }

        ui->tableTabWidget->setTabIcon(index, icon);
        ui->tableTabWidget->setTabEnabled(index, count != 0);
    };

    updateTab(pairModel, m_shownPairPairs.count(), pairNonexisting, 0);
    updateTab(eamModel, m_shownEamPairs.count(), eamNonexisting, 1);
}

/*!
 * \brief Plots the contents of a potential file
 * \param file The plotted file
 * \todo Move the reading of the contents to PotentialFile
 */
void PotentialFileDialog::plotFile(const DataFile<ReppotFileData>& file) {

    if (!file.hasData()) {
        ui->plotWidget->graph(0)->clearData();
        ui->plotWidget->replot();
        return;
    }

    double rMin = std::numeric_limits<double>::max();
    double rMax = std::numeric_limits<double>::min();
    double potMin = std::numeric_limits<double>::max();
    double potMax = std::numeric_limits<double>::min();

    QVector<double> r, pot;
    for (const ReppotFileData& value : file.data().values()) {
        if (value.r < rMin) rMin = value.r;
        if (value.r > rMax) rMax = value.r;
        if (value.V < potMin) potMin = value.V;
        if (value.V > potMax) potMax = value.V;
        r.push_back(value.r);
        pot.push_back(value.V);
    }

    ui->plotWidget->graph(0)->setData(r, pot);
    ui->plotWidget->xAxis->setRange(0, 10);
    ui->plotWidget->yAxis->setRange(potMin - 5, 50);

    ui->plotWidget->replot();
}


void PotentialFileDialog::replaceSelected() {
    if (!m_selectedPair) return;

    if (confirmReplace()) {

        QFileDialog dialog(this);
        dialog.setAcceptMode(QFileDialog::AcceptOpen);
        dialog.setOptions(QFileDialog::ReadOnly);
        dialog.setFileMode(QFileDialog::ExistingFile);
        dialog.setNameFilter("Only reppot.*.*.in files (reppot.*.*.in)");

        if (dialog.exec()) {
            DataFileContent<ReppotFileData> data;
            QStringList selectedFiles = dialog.selectedFiles();
            if (selectedFiles.size() > 0) {
                data.read(dialog.selectedFiles()[0]);
                m_potentialFiles[*m_selectedPair].setData(data);

                updateTables();
            }
        }
    }
}


void PotentialFileDialog::createSelected() {
    if (!m_selectedPair) return;

    if (confirmReplace()) {
        if (m_reppotGenerator.generate(m_selectedPair->element1, m_selectedPair->element2, 0.1, 10.0, 0.02)) {
            m_potentialFiles[*m_selectedPair].setData(m_reppotGenerator.data());

            updateTables();
        }
    }
}


bool PotentialFileDialog::confirmReplace() {
    if (m_potentialFiles[*m_selectedPair].hasData()) {
        if (QMessageBox::warning(
                this, "Overwrite existing potential data?",
                "The atom pair already has a potential data file, do you want to replace this with a new one?",
                QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes) {
            return false;
        }
    }
    return true;
}
