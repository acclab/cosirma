/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef INFORMATIONPROMPTDIALOG_H
#define INFORMATIONPROMPTDIALOG_H

#include "gui/layouts/borderlayout.h"

#include <QDialog>
#include <QLabel>
#include <QScrollArea>


namespace Ui {
class InformationPromptDialog;
}

/*!
 * \brief A dialog for showing information.
 *
 * Used for showing file contents and widgets in a dialog.
 *
 * Example:
 * \code{.cpp}
 * InformationPromptDialog *dialog = new InformationPromptDialog(this);
 * dialog->setTitle("Copyright COSIRMA");
 * dialog->addLogo(":/logos/placeholder-logo.png", BorderLayout::West);
 * dialog->setContent(":/files/copyright.html");
 * dialog->show();
 * \endcode
 */
class InformationPromptDialog : public QDialog {
    Q_OBJECT

  public:
    explicit InformationPromptDialog(QWidget* parent = nullptr);
    ~InformationPromptDialog();

    void setTitle(const QString& title);
    void addLogo(const QString& resourcePath, int width, int height, BorderLayout::Position position);
    void setContent(const QString& resourcePath);
    void setContent(QWidget* widget);

  private:
    Ui::InformationPromptDialog* ui;

    BorderLayout* m_layout;
    QScrollArea* m_scrollArea;

    QLabel* createLabel(const QString& resourcePath, int width, int height);
};

#endif  // INFORMATIONPROMPTDIALOG_H
