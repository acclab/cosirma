/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "chooseelementdialog.h"
#include "ui_chooseelementdialog.h"

#include "api/elementreader.h"

/*!
 * \brief Constructs a dialog that is ready to be shown.
 * \param parent Parent widget
 */
ChooseElementDialog::ChooseElementDialog(QWidget* parent) : QDialog(parent), ui(new Ui::ChooseElementDialog) {
    ui->setupUi(this);

    ui->groupBox_latticeData->setVisible(false);
    initTable();

    //    ui->dataGB->hide();
    connect(ui->cancelPB, &QPushButton::clicked, this, &ChooseElementDialog::close);
    connect(ui->okPB, &QPushButton::clicked, this, [this] {
        if (m_z != 0) {
            accept();
        }
        close();
    });
}

ChooseElementDialog::~ChooseElementDialog() {
    delete ui;
}

/*!
 * \brief The atomic number of the selected element.
 *
 * After the dialog has been successfully executed, the atomic number of the element that was chosen can be retrieved
 * using this function.
 *
 * \return The atomic number
 */
int ChooseElementDialog::Z() const {
    return m_z;
}

/*!
 * Periodic table is drawn by adding QPushButtons into a grid Layout.
 * Positions and colors of buttons are determined by the tables "layout" and "color".
 * Clicked event of each button is mapped so that the function loadInfo()
 * can be called with corresponding argument.
 */
void ChooseElementDialog::initTable() {

    // clang-format off
    QString layout[10][18] = {
        {"H" , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , "He"},
        {"Li", "Be", ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , "B" , "C" , "N" , "O" , "F" , "Ne"},
        {"Na", "Mg", ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , "Al", "Si", "P" , "S" , "Cl", "Ar"},
        {"K" , "Ca", "Sc", "Ti", "V" , "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr"},
        {"Rb", "Sr", "Y" , "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pb", "Ag", "Cd", "In", "Sn", "Sb", "Te", "I" , "Xe"},
        {"Cs", "Ba", ""  , "Hf", "Ta", "W" , "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn"},
        {"Fr", "Ra", ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  },
        {""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  , ""  },
        {""  , ""  , ""  , "La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu"},
        {""  , ""  , ""  , "Ac", "Th", "Pa", "U" , "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No", "Lr"}};
    // clang-format on

    QString green = "#CCFFCC";
    QString blue = "#CCFFFF";
    QString red = "#FF9ACC";
    QString orange = "#FFCCCC";
    QString lgrey = "#EBEBEB";
    QString dgrey = "#A0A0A0";
    QString pink = "#FFCCFF";
    QString purple = "#CCCCFF";
    QString yellow = "#FFFFCC";

    // clang-format off
    QString color[10][18] = {
        {green, ""    , ""   , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , purple},
        {red  , orange, ""   , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , blue  , green , green , green , pink  , purple},
        {red  , orange, ""   , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , dgrey , blue  , green , green , pink  , purple},
        {red  , orange, lgrey, lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , dgrey , blue  , blue  , green , pink  , purple},
        {red  , orange, lgrey, lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , dgrey , dgrey , blue  , blue  , pink  , purple},
        {red  , orange, ""   , lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , lgrey , dgrey , dgrey , dgrey , blue  , pink  , purple},
        {red  , orange, ""   , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    },
        {""   , ""    , ""   , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    , ""    },
        {""   , ""    , ""   , yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow},
        {""   , ""    , ""   , yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow, yellow}};
    // clang-format on


    QGridLayout* table = static_cast<QGridLayout*>(ui->groupBox_ptable->layout());

    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 18; j++) {
            if (layout[i][j].length() != 0) {
                QPushButton* button = new QPushButton(layout[i][j]);
                //                button->setMinimumHeight(ui->groupBox_ptable->height() / 10);
                //                button->setMinimumWidth(ui->groupBox_ptable->width() / 10);
                button->setMinimumHeight(30);
                button->setMinimumWidth(20);
                QString stylesheet = "background-color: " + color[i][j] + "; color: black";
                button->setStyleSheet(stylesheet);
                connect(button, &QPushButton::clicked, [this, button] { loadInfo(button->text()); });
                table->addWidget(button, i, j);
            }
        }
    }

    QLabel* text = new QLabel("Lanthanides");
    table->addWidget(text, 8, 0, 1, 3);
    text = new QLabel("Actinides");
    table->addWidget(text, 9, 0, 1, 3);
    text = new QLabel("");
    table->addWidget(text, 7, 0);
}


/*!
 * Uses ElementReader to read and show information about the element given as an argument.
 */
void ChooseElementDialog::loadInfo(const QString& symbol) {
    ui->groupBox_data->show();

    ElementReader reader;
    ui->groupBox_data->setTitle(symbol);
    m_z = reader.zFromSymbol(symbol);
    ui->zLabel->setText(QString::number(m_z));
    ui->nameLabel->setText(reader.getName(m_z));

    ui->label_latticeType->setText(reader.latticeType(m_z));
    ui->label_latticeConstant->setText(QString::number(reader.latticeConstant(m_z)));

    double mass = reader.getMass(m_z);
    if (mass < -0) {
        ui->massLabel->setText("(" + QString::number(-mass) + ")");
    } else {
        ui->massLabel->setText(QString::number(mass));
    }
}


void ChooseElementDialog::showLatticeData(bool show) {
    ui->groupBox_latticeData->setVisible(show);
}
