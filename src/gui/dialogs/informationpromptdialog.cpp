/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "informationpromptdialog.h"
#include "ui_informationpromptdialog.h"

#include <QFile>
#include <QGridLayout>
#include <QTextBrowser>
#include <QTextEdit>
#include <QVBoxLayout>

#include <QDebug>


/*!
 * \brief Constructs a dialog without any content.
 * \param parent The parent widget
 */
InformationPromptDialog::InformationPromptDialog(QWidget* parent)
        : QDialog(parent), ui(new Ui::InformationPromptDialog) {
    ui->setupUi(this);

    // The default behaviour is to delete the window when it is closed. This prevents memory leaks when the window has
    // been created with the 'new' keyword.
    setAttribute(Qt::WA_DeleteOnClose);

    // The window is in modal mode by default.
    setModal(true);

    m_layout = new BorderLayout;
    m_layout->setContentsMargins(50, 50, 50, 50);
    m_layout->setSpacing(5);

    // Create the scroll area.
    m_scrollArea = new QScrollArea(this);
    m_scrollArea->setObjectName("scrollArea");
    m_scrollArea->setWidgetResizable(true);
    m_scrollArea->setFrameShape(QFrame::NoFrame);

    // Add the scroll area to the center position of the information prompt.
    m_layout->addWidget(m_scrollArea, BorderLayout::Center);

    ui->widget->setLayout(m_layout);
}

InformationPromptDialog::~InformationPromptDialog() {
    delete m_layout;
    delete m_scrollArea;
    delete ui;
}

/*!
 * \brief Changes the title of the dialog.
 * \param title The new title
 */
void InformationPromptDialog::setTitle(const QString& title) {
    setWindowTitle(title);
}


/**
 * @brief InformationPromptDialog::addLogo
 * @param resourcePath
 * @param position
 *
 * Places logos on the information prompt window at the desired location. It is possible to stack many logos on all
 * sides of the content.
 *
 * \note The main content is placed at the BorderLayout::Center location, and will be overwritten if this locaiont is
 * used.
 */
void InformationPromptDialog::addLogo(const QString& resourcePath, int width, int height, BorderLayout::Position position) {
    m_layout->addWidget(createLabel(resourcePath, width, height), position);
}


/**
 * @brief InformationPromptDialog::createLabel
 * @param resourcePath
 *
 * Helper function for generating labels with the given graphic as content.
 *
 * @return
 */
QLabel* InformationPromptDialog::createLabel(const QString& resourcePath, int width, int height) {
    QLabel* label = new QLabel(ui->widget);

    QPixmap pixmap(resourcePath);
    label->setPixmap(pixmap.scaled(width, height, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    label->setFrameStyle(QFrame::NoFrame);
    label->setAlignment(Qt::AlignCenter);

    return label;
}


/*!
 * \brief InformationPromptDialog::setContent
 *
 * Creates a QTextBrowser displaying the content of the given resource. If the text is formatted as HTML, it will get
 * rendered as such.
 *
 * \param resourcePath
 */
void InformationPromptDialog::setContent(const QString& resourcePath) {
    qDebug() << "InformationPromptDialog::setContent(const QString &resourcePath)";

    QFile file(resourcePath);
    file.open(QFile::ReadOnly | QFile::Text);
    QTextStream stream(&file);

    QTextBrowser* content = new QTextBrowser(ui->widget);
    content->setOpenExternalLinks(true);
    content->setHtml(stream.readAll());
    content->setFrameShape(QFrame::NoFrame);

    setContent(content);
}

/*!
 * \brief InformationPromptDialog::setContent
 *
 * General function for setting the content of the information prompt.
 *
 * \param widget
 */
void InformationPromptDialog::setContent(QWidget* widget) {
    qDebug() << "InformationPromptDialog::setContent(QWidget *widget)";

    // Create a simple QVBoxLayout for the scroll area.
    QVBoxLayout* scrollLayout = new QVBoxLayout(m_scrollArea);
    scrollLayout->addWidget(widget);
}
