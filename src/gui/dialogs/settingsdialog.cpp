/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include "api/programsettings.h"

#include "gui/widgets/typetablesettings.h"

#include <QColorDialog>
#include <QFileDialog>


SettingsDialog::SettingsDialog(QWidget* parent) : QDialog(parent), ui(new Ui::SettingsDialog) {
    ui->setupUi(this);

    m_typeTableSettingsWidget = new TypeTableSettings(this);
    ui->groupBox_atoms->layout()->addWidget(m_typeTableSettingsWidget);

    ui->clickableLabel_backgroundColor->setText("");
    ui->clickableLabel_backgroundColor->setFixedSize(20, 20);
    connect(ui->clickableLabel_backgroundColor, &ClickableLabel::clicked, [this] {
        QColorDialog dialog(this);
        dialog.setOption(QColorDialog::DontUseNativeDialog, true);
        dialog.setWindowTitle("Color Picker");
        dialog.setCurrentColor(ui->clickableLabel_backgroundColor->color());

        if (dialog.exec() == QDialog::Accepted) {
            if (ui->clickableLabel_backgroundColor->color() != dialog.selectedColor()) {
                ui->clickableLabel_backgroundColor->setColor(dialog.selectedColor());
            }
        }
    });

    ui->clickableLabel_simulationBoxColor->setText("");
    ui->clickableLabel_simulationBoxColor->setFixedSize(20, 20);
    connect(ui->clickableLabel_simulationBoxColor, &ClickableLabel::clicked, [this] {
        QColorDialog dialog(this);
        dialog.setOption(QColorDialog::DontUseNativeDialog, true);
        dialog.setWindowTitle("Color Picker");
        dialog.setCurrentColor(ui->clickableLabel_simulationBoxColor->color());

        if (dialog.exec() == QDialog::Accepted) {
            if (ui->clickableLabel_simulationBoxColor->color() != dialog.selectedColor()) {
                ui->clickableLabel_simulationBoxColor->setColor(dialog.selectedColor());
            }
        }
    });

    connect(ui->clickableLabel_simulationBox, &ClickableLabel::clicked,
            [this] { ui->checkBox_showSimulationBox->toggle(); });

    load();
}


SettingsDialog::~SettingsDialog() {
    delete ui;
}


/*!
 * \brief Get the current state of the underlying settings object.
 */
void SettingsDialog::load() {
    // Backend Settings
    ui->lineEdit_parcasPath->setText(ProgramSettings::getInstance().backendSettings().parcasPath);
    ui->lineEdit_frameProcessorPath->setText(ProgramSettings::getInstance().backendSettings().processorPath);
    ui->checkBox_enableSpeedup->setChecked(ProgramSettings::getInstance().backendSettings().speedupEnabled);
    ui->groupBox_mpi->setChecked(ProgramSettings::getInstance().backendSettings().useMpi);
    ui->spinBox_mpiCores->setValue(ProgramSettings::getInstance().backendSettings().mpiCores);
    ui->checkBox_showStartDialog->setChecked(
        ProgramSettings::getInstance().backendSettings().showSimulationStartDialog);

    // Location Settings
    ui->checkBox_useTempRuntimeDir->setChecked(ProgramSettings::getInstance().locationSettings().useTempRuntimeDir);
    ui->checkBox_binaryOutput->setChecked(ProgramSettings::getInstance().locationSettings().saveBinaryFrames);
    ui->lineEdit_runtimePath->setText(ProgramSettings::getInstance().locationSettings().runtimeDirPath);
    ui->lineEdit_projectPath->setText(ProgramSettings::getInstance().locationSettings().projectDirPath);

    // Visual Settings
    m_typeTableSettingsWidget->setSettings(ProgramSettings::getInstance().visualSettings());
    ui->checkBox_showSimulationBox->setChecked(ProgramSettings::getInstance().visualSettings().cellBoundaryEnabled());
    ui->clickableLabel_simulationBoxColor->setColor(
        ProgramSettings::getInstance().visualSettings().cellBoundaryColor());
    ui->clickableLabel_backgroundColor->setColor(ProgramSettings::getInstance().visualSettings().backgroundColor());
    ui->spinBox_depthPeelCount->setValue(ProgramSettings::getInstance().visualSettings().depthPeelCount());
}


/*!
 * \brief Apply the changes to the underlying settings object. This function should not be called unless the user
 * actually wants to the save the changes (i.e. pressing OK button).
 */
void SettingsDialog::save() {
    // Backend Settings
    ProgramSettings::getInstance().backendSettings().parcasPath = ui->lineEdit_parcasPath->text();
    ProgramSettings::getInstance().backendSettings().processorPath = ui->lineEdit_frameProcessorPath->text();
    ProgramSettings::getInstance().backendSettings().speedupEnabled = ui->checkBox_enableSpeedup->isChecked();
    ProgramSettings::getInstance().backendSettings().useMpi = ui->groupBox_mpi->isChecked();
    ProgramSettings::getInstance().backendSettings().mpiCores = ui->spinBox_mpiCores->value();
    ProgramSettings::getInstance().backendSettings().showSimulationStartDialog =
        ui->checkBox_showStartDialog->isChecked();

    // Location Settings
    ProgramSettings::getInstance().locationSettings().useTempRuntimeDir = ui->checkBox_useTempRuntimeDir->isChecked();
    ProgramSettings::getInstance().locationSettings().runtimeDirPath = ui->lineEdit_runtimePath->text();
    ProgramSettings::getInstance().locationSettings().projectDirPath = ui->lineEdit_projectPath->text();
    ProgramSettings::getInstance().locationSettings().saveBinaryFrames = ui->checkBox_binaryOutput->isChecked();

    // Visual Settings
    ProgramSettings::getInstance().visualSettings().changeTypeColors(m_typeTableSettingsWidget->colors());
    ProgramSettings::getInstance().visualSettings().changeTypeSizes(m_typeTableSettingsWidget->radii());
    ProgramSettings::getInstance().visualSettings().changeCellBoundaryEnabled(
        ui->checkBox_showSimulationBox->isChecked());
    ProgramSettings::getInstance().visualSettings().changeCellBoundaryColor(
        ui->clickableLabel_simulationBoxColor->color());
    ProgramSettings::getInstance().visualSettings().changeBackgroundColor(ui->clickableLabel_backgroundColor->color());
    ProgramSettings::getInstance().visualSettings().changeDepthPeelCount(ui->spinBox_depthPeelCount->value());
}


/*!
 * \brief Helper function for locating files
 * \param message
 * \param currentFilePath
 * \return The path to the file
 */
QString SettingsDialog::locateFilePath(const QString& message, const QString& currentFilePath) {
    QString path = QDir::currentPath();
    if (QFile(currentFilePath).exists()) {
        path = currentFilePath;
    }
    QString filename = QFileDialog::getOpenFileName(this, message, path);
    return filename;
}


/*!
 * \brief Helper function for locating existing folders
 * \param message
 * \param currentPath
 * \return The path the folder
 */
QString SettingsDialog::locateDirectoryPath(const QString& message, const QString& currentPath) {
    QString path = QDir::currentPath();
    if (QDir(currentPath).exists()) {
        path = currentPath;
    }
    return QFileDialog::getExistingDirectory(this, message, path);
}


//====================
// Callback functions
//====================


void SettingsDialog::on_pushButton_openParcasPath_clicked() {
    QString path = locateFilePath("Locate PARCAS", ui->lineEdit_parcasPath->text());
    if (!path.isEmpty()) ui->lineEdit_parcasPath->setText(path);
}


void SettingsDialog::on_pushButton_openFrameProcessorPath_clicked() {
    QString path = locateFilePath("Locate the Frame Processor", ui->lineEdit_frameProcessorPath->text());
    if (!path.isEmpty()) ui->lineEdit_frameProcessorPath->setText(path);
}


void SettingsDialog::on_pushButton_openRuntimePath_clicked() {
    QString path = locateDirectoryPath("Locate Runtime Path", ui->lineEdit_runtimePath->text());
    if (!path.isEmpty()) ui->lineEdit_runtimePath->setText(path);
}


void SettingsDialog::on_pushButton_openProjectPath_clicked() {
    QString path = locateDirectoryPath("Locate Project Path", ui->lineEdit_projectPath->text());
    if (!path.isEmpty()) ui->lineEdit_projectPath->setText(path);
}


void SettingsDialog::on_checkBox_enableSpeedup_clicked(bool checked) {
    emit enableSpeedupModule(checked);
}
