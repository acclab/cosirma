/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PLOTCELLGLWIDGET_H
#define PLOTCELLGLWIDGET_H

#include "api/data/beamdata.h"
#include "api/data/iontransformdata.h"
#include "api/geometries.h"
#include "api/visualsettings.h"

#include "gui/rendering/abstractmesh.h"
#include "gui/rendering/abstractmodel.h"
#include "gui/widgets/utils/particlegrid.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <vector>

#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLWidget>
#include <QQuaternion>
#include <QVector2D>


class QOpenGLBuffer;
class QOpenGLVertexArrayObject;
class QOpenGLTexture;

class ParticleModel;
class CubeModel;
class ArrowModel;

/*! \brief QOpenGLWidget with 3D plot of atoms.
 *
 * Can be used to plot atoms as point sprites.
 *
 * \sa atom
 * \todo Plot needs some kind of an orientatio reference.
 * \todo For optimization purposes PlotMode could be used more.
 */

class PlotCellGLWidget : public QOpenGLWidget, protected QOpenGLFunctions {
    Q_OBJECT

  public:
    /*!
     * Plotting mode. "Cooling" supports transparent boxes and "Beam" draws an arrow showing the direction of the beam.
     */
    enum PlotMode { Plain, Cooling, Beam };

    explicit PlotCellGLWidget(QWidget* parent = nullptr);
    ~PlotCellGLWidget();
    void setVisualSettings(const VisualSettings& vSettings);
    void setFrame(const XyzData& frame);
    void zero();

    void addModel(AbstractModel* model);
    void removeModel(AbstractModel* model);

    void setModelOrientation(float, float, float);
    void rotate(const glm::vec3& axis, float angle);
    void centerCell();

    void initView(const Box& limits);

    void usePerspective(bool);
    bool isPerspective();

    void setBackgroundColor(QColor);
    QColor getBackgroundColor();

  protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

    QSize minimumSizeHint() const;
    QSize sizeHint() const;
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void wheelEvent(QWheelEvent* event);
    void leaveEvent(QEvent* event);

  private:
    struct BeamParameters {
        int type;
        float width;
        glm::vec3 position;
        glm::vec3 direction;
        glm::vec3 rotation;
    };

    void bindTexture(QOpenGLShaderProgram& shader, int textureUnit, GLuint texture, const char* uniformName);

    void createShaderProgram(QOpenGLShaderProgram& program, const QString& vert, const QString& frag);

    void setupShaders();
    void setupRendering();
    void cleanupRendering();

    void setBeamArrowTransformFromBeam(const BeamParameters& m_beam);

    void renderParticles(QOpenGLShaderProgram& program, const float& spaceW);
    void renderModels(QOpenGLShaderProgram& program);
    void renderTripod(QOpenGLShaderProgram& program);
    void renderText(int x, int y, const QString& text, const QColor& color);

    const VisualSettings* m_visualSettings = nullptr;

    PlotMode m_mode;

    GLuint m_frontFboId[2];
    GLuint m_frontDepthTexId[2];
    GLuint m_frontColorTexId[2];
    GLuint m_frontColorBlenderTexId;
    GLuint m_frontColorBlenderFboId;

    // Rendering to screen
    QOpenGLVertexArrayObject* m_screenVAO;
    QOpenGLShaderProgram m_screenQuadShader;
    QOpenGLShaderProgram m_screenQuadPeelBlendShader;

    // Rendering the particles
    ParticleModel* m_particles = nullptr;
    QOpenGLShaderProgram m_particlesInitShader;
    QOpenGLShaderProgram m_particlesPeelShader;

    // Shaders for rendering models
    CubeModel* m_particleFrame = nullptr;
    ArrowModel* m_beamArrow = nullptr;
    std::vector<AbstractModel*> m_models;
    QOpenGLShaderProgram m_shaderInitShader;
    QOpenGLShaderProgram m_shaderPeelShader;

    // Tripod models
    ArrowModel* m_axisTripodX = nullptr;
    ArrowModel* m_axisTripodY = nullptr;
    ArrowModel* m_axisTripodZ = nullptr;
    QOpenGLShaderProgram m_simpleShader;


    QVector2D m_previousCursorPosition;
    bool m_mouseMovedAfterPress;

    glm::quat m_rotation;
    glm::quat m_orientationRotation;

    float m_zoomScalingFactor;
    int m_zoomCount = 0;

    glm::vec3 m_focalPoint;
    glm::vec3 m_cameraLoc;

    glm::mat4 m_modelView;
    glm::mat4 m_model;
    glm::mat4 m_view;
    glm::mat4 m_projection;
    glm::mat4 m_projection_screen;

    Box m_spaceLimits;

    bool m_perspective;

    BeamParameters m_beam;

    QColor m_bgColor;

  public slots:
    void setBeam(BeamData beamData, Box cellLims);
    void setBeam(IonTransformData data);
    void setPlotMode(PlotMode);

    void on_atomsUpdated();

    void on_hideType(int type, bool hide);
    void on_setTypeRadius(int type, float radius);
    void on_setTypeColors(const QHash<int, QColor>& colors);

    void on_setOutlineBox(const Box& box);

  signals:
    void mouseMoved(glm::vec3);
    void orientationChanged();
    void widgetLeft();
    void clicked(glm::vec3);
    void mouseDragged();
};

#endif  // PLOTCELLGLWIDGET_H
