/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PLOTCELL_H
#define PLOTCELL_H

#include "api/atom.h"
#include "api/atomtype.h"
#include "api/entry.h"
#include "api/visualsettings.h"

#include "gui/rendering/particlemesh.h"
#include "gui/widgets/plotcellglwidget.h"
#include "gui/widgets/utils/clickablelabel.h"
#include "gui/widgets/utils/particlegrid.h"
#include "gui/widgets/utils/wheelguard.h"

#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QWidget>

#include <glm/vec3.hpp>

#include <vector>


namespace Ui {
class PlotCell;
}

/*!
 * \brief QWidget that includes a 3D plot of the simulation cell and some user interactions.
 *
 * Plot is done with PlotCellGLWidget. For example the size and color of the
 * plotted atoms can be changed.
 *
 * \sa PlotCellGLWidget VisualSettings
 */
class PlotCell : public QWidget {
    Q_OBJECT

  public:
    enum ViewOption { Top, Bottom, Left, Right, Front, Back, Perspective, Orthogonal };

    explicit PlotCell(QWidget* parent = nullptr);
    ~PlotCell();
    void setEntry(const ExtendedEntryData& entry);
    void setVisuals(VisualSettings* settings);
    void entryUpdated();

    PlotCellGLWidget* glWidget();
    QWidget* lastWidget();

  public slots:
    void changeViewOption(ViewOption option);
    void bbUpdated(const Box& box);

  private:
    Ui::PlotCell* ui;

    ParticleGrid* m_grid = nullptr;

    const ExtendedEntryData* m_entry = nullptr;
    QList<QWidget*> m_gridWidgets;

    VisualSettings* m_visuals = nullptr;

  private slots:
    void updateList();
    void updateParticles();
    void updateBackground(const QColor&);
    void updateOutline(const QColor&);

    void freeOrientation();

    void updateHoveredParticle(const glm::dvec3& mousePos);
    void updateClickedParticle(const glm::dvec3& mousePos);

    void particleColorChangeEvent(int);
    void outlineColorChangeEvent();
    void backgroundColorChangeEvent();

    void showMenus(bool);


    void on_spinBox_depthPeels_valueChanged(int arg1);

  signals:
    void tabOrderUpdated(QWidget* last);
};

#endif  // PLOTCELL_H
