/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "trackoutputwidget.h"
#include "ui_trackoutputwidget.h"

#include "api/history.h"
#include "api/programsettings.h"
#include "api/project.h"
#include "ratio.h"

#include "api/util.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>


TrackOutputWidget::TrackOutputWidget(QWidget* parent) : QWidget(parent), ui(new Ui::TrackOutputWidget) {
    ui->setupUi(this);
    // ui->outputHL->setStretch(1, 0);

    beingLoaded = QPair<int, int>(-1, -1);
    loaded = QPair<int, int>(-1, -1);

    connect(&frameReader, &FrameReaderThread::frameReadFinished, this, [this](bool success) {
        if (success)
            loadingFinished();
        else
            loadingFailed();
    });

    connect(ui->frameSelectorWidget, &FrameSelector::selectionChanged, [this](int frame) {
        if (beingLoaded.first == -1)
            startLoading(loaded.first, frame);
        else
            startLoading(beingLoaded.first, frame);
    });

    //    connect(&Project::getInstance().history, &History::historyChanged, [this] {
    //        ui->historyTree->changeData(Project::getInstance().history);
    //        ui->frameSelectorWidget->setNumberOfFrames(Project::getInstance().history.latestEntry().frameCount());
    //        startLoading(Project::getInstance().history.entryCount() - 1,
    //                     Project::getInstance().history.latestEntry().frameCount() - 1);
    //    });

    ui->historyTree->setHistory(Project::getInstance().history);

    connect(ui->historyTree, &HistoryTreeView::selectionModified, this, &TrackOutputWidget::startLoading);

    connect(&Project::getInstance().history, &History::entryAdded, this,
            [this] { ui->historyTree->addEntry(Project::getInstance().history.newestEntry()); });

    const Entry* e = Project::getInstance().history.newestEntry();
    loadedEntry.data.types = e->types();
    loadedEntry.frame = frameReader.frame();
    ui->plotWidget->setEntry(loadedEntry);

    ui->splitter->setCollapsible(0, true);
    ui->splitter->setCollapsible(1, false);
    ui->splitter->setCollapsible(2, true);
    util::decorateSplitterHandle(ui->splitter, 1);
    util::decorateSplitterHandle(ui->splitter, 2);

    ui->splitter->setSizes(QList<int>() << 300 << 0 << 0);
}


TrackOutputWidget::~TrackOutputWidget() {
    disconnect(&Project::getInstance().history);
    delete ui;
}


void TrackOutputWidget::startLoading(int entry, int frame) {
    QPair<int, int> newLoad = QPair<int, int>(entry, frame);
    if (loaded == newLoad || beingLoaded == newLoad) return;
    if (entry < 0 || entry >= Project::getInstance().history.entryCount()) return;

    const Entry* e = Project::getInstance().history.entry(entry);
    if (frame < 0 || frame >= e->frameCount()) return;
    loadedEntry.data.info = e->info();

    beingLoaded = newLoad;

    ui->frameSelectorWidget->setNumberOfFrames(e->frameCount());
    ui->frameSelectorWidget->setValue(frame);
    ui->historyTree->setSelectedFrame(entry, frame);

    frameReader.read(e->framePath(frame));
}


void TrackOutputWidget::loadingFailed() {
    beingLoaded = QPair<int, int>(-1, -1);

    loadedEntry = ExtendedEntryData();

    ui->plotWidget->setEntry(loadedEntry);
    if (selectedModule) {
        selectedModule->draw(&loadedEntry.frame);
    }

    QMessageBox::critical(this, "Error!", "Reading failed!", QMessageBox::Yes);
}


void TrackOutputWidget::loadingFinished() {
    loaded = beingLoaded;
    beingLoaded = QPair<int, int>(-1, -1);

    const Entry* e = Project::getInstance().history.entry(loaded.first);
    loadedEntry.data.types = e->types();
    loadedEntry.frame = frameReader.frame();

    ui->plotWidget->setEntry(loadedEntry);
    if (selectedModule) {
        selectedModule->draw(&loadedEntry.frame);
    }
}


/*!
 * Closes the selected OutputModule.
 */
void TrackOutputWidget::closeOutputModule() {
    if (selectedModule) {
        ui->ouputModuleLayout->removeWidget(selectedModule);
        // ui->outputHL->setStretch(1, 0);
        delete selectedModule;
        selectedModule = nullptr;
    }
}


void TrackOutputWidget::on_comboBox_currentIndexChanged(const QString& arg1) {
    QList<int> sizes = ui->splitter->sizes();
    int mid = sizes[1];
    closeOutputModule();
    if (arg1.compare("Ratio") == 0) {
        selectedModule = new Ratio(this);
        ui->ouputModuleLayout->addWidget(selectedModule);
        // ui->outputHL->setStretch(1, 1);
        selectedModule->draw(&Project::getInstance().history.newestEntry()->extendedData().frame);
        sizes[1] = mid / 2;
        sizes[2] = mid / 2;
    } else {
        sizes[1] = mid;
        sizes[2] = 0;
    }
    ui->splitter->setSizes(sizes);

    ui->comboBox->setItemText(0, "None");
}
