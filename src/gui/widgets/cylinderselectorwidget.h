/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef CYLINDERSELECTORWIDGET_H
#define CYLINDERSELECTORWIDGET_H

#include "api/atomselections.h"
#include "gui/rendering/cylindermodel.h"
#include "gui/widgets/abstractatomselectorwidget.h"


namespace Ui {
class CylinderSelectorWidget;
}

class CylinderSelectorWidget : public AbstractAtomSelectorWidget {
    Q_OBJECT

  public:
    explicit CylinderSelectorWidget(PlotCellGLWidget* context, QWidget* parent = nullptr);
    ~CylinderSelectorWidget() override;

    const AbstractAtomSelection& selector() override;

    void init() override;
    void connectToPlot() override;
    void hidePlottables() override;

  private:
    Ui::CylinderSelectorWidget* ui;
    CylinderAtomSelection m_selector;

    CylinderModel* m_cylinder = nullptr;
    CylinderModel* m_wireframeCylinder = nullptr;

    QColor m_color;

  private slots:
    void blockSignalsFromComponents(bool);
    void setColor(const QColor& color);

    void updateDimensions();
};

#endif  // CYLINDERSELECTORWIDGET_H
