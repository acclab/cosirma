/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SIMULATIONLISTWIDGET_H
#define SIMULATIONLISTWIDGET_H

#include "api/simulationlist.h"
#include "gui/widgets/plotcellglwidget.h"
#include "gui/widgets/simulations/abstractsimulationwidget.h"

#include <QString>
#include <QWidget>


namespace Ui {
class SimulationListWidget;
}

class SimulationListWidget : public QWidget {
    Q_OBJECT

  public:
    explicit SimulationListWidget(SimulationList* state, QWidget* parent = nullptr);
    ~SimulationListWidget();

    const AbstractSimulationWidget* selectedWidget() const;

    void setEntry(ExtendedEntryData* entry);
    ExtendedEntryData entry();

    bool isModified();

  private:
    Ui::SimulationListWidget* ui;

    SimulationList* m_simulationList;
    QHash<QString, AbstractSimulationWidget*> m_widgets;
    AbstractSimulationWidget* m_selectedSimulationWidget = nullptr;

    ExtendedEntryData* m_entry = nullptr;

  public slots:
    void onEnableSpeedupModule(bool enable);

    void discardChanges();
    void saveChanges();

  private slots:
    void onSimulationChanged();

  signals:
    void enableSpeedupModule(bool enable);
    void enableSimulationButtons(bool enable);
};

#endif  // SIMULATIONLISTWIDGET_H
