/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef DIMENSIONSELECTOR_H
#define DIMENSIONSELECTOR_H

#include "api/geometries.h"

#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QWidget>


/*!
 * \brief QWidget that can be used to select dimensions for a box.
 *
 * Unlike BoxSelector, does not use RangeSliderBoxes and thus does not have
 * limits (other than those set by the limits of the QDoubleSpinBoxes).
 *
 * \sa box BoxSelector
 */
class DimensionSelector : public QWidget {
    Q_OBJECT

  public:
    explicit DimensionSelector(QWidget* parent = nullptr);
    ~DimensionSelector() {}

    const Box& getDimensions();
    void setDimensions(const Box& box);

    const glm::bvec3& getPeriodicBoundaries();
    void setPeriodicBoundaries(const glm::bvec3& pbc);

  private:
    Box m_dimensions;
    glm::bvec3 m_pbc;

    QDoubleSpinBox* m_xLower;
    QDoubleSpinBox* m_xUpper;
    QDoubleSpinBox* m_yLower;
    QDoubleSpinBox* m_yUpper;
    QDoubleSpinBox* m_zLower;
    QDoubleSpinBox* m_zUpper;

    QCheckBox* m_pbcX;
    QCheckBox* m_pbcY;
    QCheckBox* m_pbcZ;

  private slots:
    void updateWidgets();

  signals:
    void dimensionsEdited();
    void pbcEdited();
};

#endif  // DIMENSIONSELECTOR_H
