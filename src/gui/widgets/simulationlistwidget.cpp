/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "simulationlistwidget.h"
#include "ui_simulationlistwidget.h"

#include "api/programsettings.h"
#include "api/simulation/parcas/parcasirradiationcontroller.h"
#include "api/util.h"

#include "gui/widgets/simulations/abstractsimulationwidget.h"
#include "gui/widgets/simulations/irradiationsimulationwidget.h"
#include "gui/widgets/simulations/relaxationsimulationwidget.h"
#include "gui/widgets/simulations/singlecascadesimulationwidget.h"
#include "gui/widgets/utils/wheelguard.h"

#include <QDebug>
#include <QHeaderView>
#include <QMessageBox>


SimulationListWidget::SimulationListWidget(SimulationList* simulationList, QWidget* parent)
        : QWidget(parent), ui(new Ui::SimulationListWidget), m_simulationList(simulationList) {
    ui->setupUi(this);

    QStringList names = m_simulationList->names();

    ui->comboBox_simulationType->addItem("None");
    for (const QString& n : names) {
        ui->comboBox_simulationType->addItem(n);
    }

    m_widgets["Relaxation"] = new RelaxationSimulationWidget(this);
    m_widgets["Cascade"] = new SingleCascadeSimulationWidget(this);
    m_widgets["Irradiation"] = new IrradiationSimulationWidget(this);

    // Adding the widgets to a stacked widget
    for (AbstractSimulationWidget* w : m_widgets.values()) {
        ui->stackedWidget->addWidget(w);
        w->layout()->setMargin(0);
        connect(this, &SimulationListWidget::enableSpeedupModule, w, &AbstractSimulationWidget::enableSpeedupModule);
    }

    ui->comboBox_simulationType->installEventFilter(new WheelGuard(this));
    ui->comboBox_simulationType->setFocusPolicy(Qt::StrongFocus);

    connect(ui->comboBox_simulationType, &QComboBox::currentTextChanged, this, [=](const QString& name) {
        qDebug() << "Changing simulation!";
        if (m_selectedSimulationWidget && m_selectedSimulationWidget->isModified()) {
            if (QMessageBox::Yes == QMessageBox::warning(this, "Unsaved changes",
                                                         "Do you want to continue and discard the unsaved changes?",
                                                         QMessageBox::Yes | QMessageBox::No)) {
                m_simulationList->changeSimulation(name);
            } else {
                ui->comboBox_simulationType->blockSignals(true);
                ui->comboBox_simulationType->setCurrentText(m_simulationList->currentController()->name());
                ui->comboBox_simulationType->blockSignals(false);
            }
        } else {
            m_simulationList->changeSimulation(name);
        }
    });
    connect(m_simulationList, &SimulationList::simulationChanged, this, &SimulationListWidget::onSimulationChanged);

    ui->splitter->setChildrenCollapsible(false);
    util::decorateSplitterHandle(ui->splitter, 1);

    // To sync the widths of the Editor Inspector and the Simulations Inspector we add +3 to take into account the empty
    // splitter handle found in the Editor.
    ui->splitter->setSizes(QList<int>() << 400 + 3 << 300);
}


SimulationListWidget::~SimulationListWidget() {
    m_simulationList->changeSimulation("");

    for (QWidget* w : m_widgets.values()) {
        delete w;
    }
    delete ui;
}


const AbstractSimulationWidget* SimulationListWidget::selectedWidget() const {
    return m_selectedSimulationWidget;
}


void SimulationListWidget::setEntry(ExtendedEntryData* entry) {
    m_entry = entry;

    ui->plotWidget->setEntry(*m_entry);

    if (m_selectedSimulationWidget) {
        // Re-populate the widgets with the current state.
        m_selectedSimulationWidget->setEntry(m_entry);
    } else {
        qDebug() << "Trying to set the entry without simulation selected...";
    }
}


ExtendedEntryData SimulationListWidget::entry() {
    if (!m_selectedSimulationWidget) {
        return ExtendedEntryData();
    }
    return ExtendedEntryData(m_selectedSimulationWidget->entry());
}


bool SimulationListWidget::isModified() {
    if (!m_selectedSimulationWidget) return false;

    return m_selectedSimulationWidget->isModified();
}


void SimulationListWidget::onEnableSpeedupModule(bool enable) {
    emit enableSpeedupModule(enable);
}


void SimulationListWidget::discardChanges() {
    m_widgets["Relaxation"]->setControllerParametersToWidget(m_simulationList->controller("Relaxation"));
    m_widgets["Cascade"]->setControllerParametersToWidget(m_simulationList->controller("Cascade"));
    m_widgets["Irradiation"]->setControllerParametersToWidget(m_simulationList->controller("Irradiation"));
}


void SimulationListWidget::saveChanges() {
    m_widgets["Relaxation"]->setWidgetParametersToController(m_simulationList->controller("Relaxation"));
    m_widgets["Cascade"]->setWidgetParametersToController(m_simulationList->controller("Cascade"));
    m_widgets["Irradiation"]->setWidgetParametersToController(m_simulationList->controller("Irradiation"));
    // Re-apply the saved values!
    discardChanges();
}


void SimulationListWidget::onSimulationChanged() {
    if (m_selectedSimulationWidget) {
        m_selectedSimulationWidget->disconnectFromPlot(ui->plotWidget->glWidget());
    }

    if (!m_simulationList->currentController()) return;

    const QString& simulationName = m_simulationList->currentController()->name();
    if (m_widgets.contains(simulationName)) {
        // Fetch the current simulation widget from the list
        m_selectedSimulationWidget = m_widgets[simulationName];

        // Connect the current simulation widget to the OpenGL widget for showing the atom data.
        m_selectedSimulationWidget->connectToPlot(ui->plotWidget->glWidget());

        // Re-populate the widgets with the current state. If it is unsaved, it will be reverted to the latest saved.
        m_selectedSimulationWidget->setEntry(m_entry);
        discardChanges();

        // Do the actual widget switch and update the GUI.
        ui->stackedWidget->setCurrentWidget(m_selectedSimulationWidget);
        //        ui->comboBox_simulationType->setCurrentText(simulationName);

        emit enableSimulationButtons(true);
    } else {
        ui->stackedWidget->setCurrentWidget(ui->page_empty);
        ui->comboBox_simulationType->setCurrentIndex(0);
        m_selectedSimulationWidget = nullptr;

        emit enableSimulationButtons(false);
    }
}
