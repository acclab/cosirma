/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ENTRYEDITORWIDGET_H
#define ENTRYEDITORWIDGET_H

#include "api/entryeditor.h"

#include "gui/widgets/actionwidgets.h"
#include "gui/widgets/atomselectorwidget.h"
#include "gui/widgets/boxselectorwidget.h"
#include "gui/widgets/typetable.h"

namespace Ui {
class EntryEditorWidget;
}

/*!
 * \brief QWidget for modifying the atom data.
 *
 * Can be used for example to change the dimensions of the simulation cell,
 * rotate or crop the cell, fix atoms in a volume, or modify the atom types.
 */
class EntryEditorWidget : public QWidget {
    Q_OBJECT

  public:
    explicit EntryEditorWidget(EntryEditor* editor, QWidget* parent = nullptr);
    ~EntryEditorWidget() override;

    QWidget* lastWidget();

  private:
    Ui::EntryEditorWidget* ui;

    AtomSelectorWidget* m_selector;
    TypeTable* m_typeTable;

    EntryEditor* m_editor;

    QVector<AbstractActionWidget*> m_actionWidgets;

  private slots:
    void on_updateAtoms();
    void on_updateBoundingBox();
    void on_updateTypes();
    void on_changeAction(int index);

  signals:
    void tabOrderUpdated(QWidget* lastWidget);
    void entryAndFrameSelected(int entry, int frame);
};

#endif  // ENTRYEDITORWIDGET_H
