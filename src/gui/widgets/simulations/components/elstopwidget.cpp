/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "elstopwidget.h"
#include "ui_elstopwidget.h"

#include "api/data/generators/elstopgenerator.h"


ElstopWidget::ElstopWidget(QWidget* parent) : AbstractComponentWidget(parent), ui(new Ui::ElstopWidget) {
    ui->setupUi(this);

    const QDoubleValidator* validator = new QDoubleValidator;
    ui->lineEdit_min->setValidator(validator);
    ui->lineEdit_max->setValidator(validator);
    ui->lineEdit_step->setValidator(validator);
    ui->lineEdit_density->setValidator(validator);
}


ElstopWidget::~ElstopWidget() {
    delete ui;
}


void ElstopWidget::setParameters(api::BaseParameters parameters) {
    qWarning() << "Not yet implemented!";
}


api::BaseParameters ElstopWidget::parameters() {
    qWarning() << "Not yet implemented!";
}


void ElstopWidget::setEntry(ExtendedEntryData* entry) {
    AbstractComponentWidget::setEntry(entry);

    const QHash<int, AtomType> atomTypes = entry->data.types.typeList;
    for (int key : atomTypes.keys()) {
        qDebug() << key << atomTypes[key];
    }
}


void ElstopWidget::createFiles() {
    QRegularExpression regexp("[A-Z][^A-Z]*");
    QRegularExpressionMatchIterator match = regexp.globalMatch(ui->lineEdit_substrate->text());

    QList<QString> vec;
    while (match.hasNext()) vec.append(match.next().capturedTexts());

    qDebug() << vec;


    //    double totalMass = 0;
    //    for (const Atom& atom : m_entry.frame.atoms) {
    //        totalMass += m_entry.data.types.typeList[atom.type].mass;
    //    }
    //    totalMass = totalMass * 1.6605390e-24;

    //    double volume = m_entry.frame.atomLims.dx() * m_entry.frame.atomLims.dy() * m_entry.frame.atomLims.dz();
    //    double rho = totalMass / volume * (1e8 * 1e8 * 1e8);
    //    qDebug() << "Cell Density: " << rho;

    //    ElstopGenerator generator;
    //    generator.generate(ui->lineEdit_path->text(), m_entry, rho);
}
