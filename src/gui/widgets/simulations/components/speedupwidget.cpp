/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "speedupwidget.h"
#include "ui_speedupwidget.h"

#include "api/project.h"

#include "gui/widgets/utils/wheelguard.h"

#include <QComboBox>
#include <QLabel>
#include <QLineEdit>

#include <algorithm>


SpeedupWidget::SpeedupWidget(QWidget* parent) : AbstractComponentWidget(parent), ui(new Ui::SpeedupWidget) {
    ui->setupUi(this);

    QList<QWidget*> wheelGuardedWidgets = {ui->checkBox_useSpeedup,          ui->doubleSpinBox_allMoveTime,
                                           ui->doubleSpinBox_forceCriteria,  ui->doubleSpinBox_forceCriteriaFactor,
                                           ui->doubleSpinBox_energyCriteria, ui->doubleSpinBox_energyCriteriaFactor,
                                           ui->doubleSpinBox_hotCutoff,      ui->doubleSpinBox_coldCutoff,
                                           ui->doubleSpinBox_fixedCutoff};

    WheelGuard* guard = new WheelGuard(this);
    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(guard);
    }

    setFocusProxy(ui->checkBox_useSpeedup);


    connect(ui->checkBox_useSpeedup, &QCheckBox::clicked, this, [=] { emit modified(); });

    connect(ui->doubleSpinBox_allMoveTime, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });

    connect(ui->doubleSpinBox_forceCriteria, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_forceCriteriaFactor, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });

    connect(ui->doubleSpinBox_energyCriteria, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_energyCriteriaFactor, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });

    connect(ui->groupBox_overrideActivationCutoffs, QOverload<bool>::of(&QGroupBox::clicked), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_hotCutoff, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_coldCutoff, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_fixedCutoff, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
}


SpeedupWidget::~SpeedupWidget() {
    delete ui;
}


void SpeedupWidget::setParameters(api::BaseParameters parameters) {
    QSignalBlocker blocker1(ui->checkBox_useSpeedup);
    QSignalBlocker blocker2(ui->doubleSpinBox_allMoveTime);
    QSignalBlocker blocker3(ui->doubleSpinBox_forceCriteria);
    QSignalBlocker blocker4(ui->doubleSpinBox_forceCriteriaFactor);
    QSignalBlocker blocker5(ui->doubleSpinBox_energyCriteria);
    QSignalBlocker blocker6(ui->doubleSpinBox_energyCriteriaFactor);
    //    QSignalBlocker blocker7(ui->checkBox_useSpeedup); //! \todo type mapping!
    QSignalBlocker blocker8(ui->doubleSpinBox_hotCutoff);
    QSignalBlocker blocker9(ui->doubleSpinBox_coldCutoff);
    QSignalBlocker blocker10(ui->doubleSpinBox_fixedCutoff);


    ui->checkBox_useSpeedup->setChecked(parameters[api::parameters::speedup::ON].value().toBool());

    ui->doubleSpinBox_allMoveTime->setValue(parameters[api::parameters::speedup::ALL_MOVE_TIME].value().toDouble());

    ui->doubleSpinBox_forceCriteria->setValue(parameters[api::parameters::speedup::FORCE_CRITERIA].value().toDouble());
    ui->doubleSpinBox_forceCriteriaFactor->setValue(
        parameters[api::parameters::speedup::FORCE_CRITERIA_FACTOR].value().toDouble());
    ui->doubleSpinBox_energyCriteria->setValue(
        parameters[api::parameters::speedup::ENERGY_CRITERIA].value().toDouble());
    ui->doubleSpinBox_energyCriteriaFactor->setValue(
        parameters[api::parameters::speedup::ENERGY_CIRTERIA_FACTOR].value().toDouble());

    updateTypeMappingTable(parameters);

    ui->groupBox_overrideActivationCutoffs->setChecked(
        parameters[api::parameters::speedup::OVERRIDE_CUTOFFS_ON].value().toBool());
    ui->doubleSpinBox_hotCutoff->setValue(parameters[api::parameters::speedup::HOT_CUTOFF].value().toDouble());
    ui->doubleSpinBox_coldCutoff->setValue(parameters[api::parameters::speedup::COLD_CUTOFF].value().toDouble());
    ui->doubleSpinBox_fixedCutoff->setValue(parameters[api::parameters::speedup::FIXED_CUTOFF].value().toDouble());
}


api::BaseParameters SpeedupWidget::parameters() {
    api::BaseParameters params;

    qWarning() << "Not yet implemented!";

    params.set(api::parameters::speedup::ON, api::value_t(ui->checkBox_useSpeedup->isChecked()));
    params.set(api::parameters::speedup::ALL_MOVE_TIME, api::value_t(ui->doubleSpinBox_allMoveTime->value()));

    params.set(api::parameters::speedup::FORCE_CRITERIA, api::value_t(ui->doubleSpinBox_forceCriteria->value()));
    params.set(api::parameters::speedup::FORCE_CRITERIA_FACTOR,
               api::value_t(ui->doubleSpinBox_forceCriteriaFactor->value()));
    params.set(api::parameters::speedup::ENERGY_CRITERIA, api::value_t(ui->doubleSpinBox_energyCriteria->value()));
    params.set(api::parameters::speedup::ENERGY_CIRTERIA_FACTOR,
               api::value_t(ui->doubleSpinBox_energyCriteriaFactor->value()));

    QString typeMapString = "";
    for (int row = 0; row < ui->tableWidget_atomMappingTable->rowCount(); ++row) {
        QLineEdit* type1Line = static_cast<QLineEdit*>(ui->tableWidget_atomMappingTable->cellWidget(row, 1));
        QComboBox* type2Line = static_cast<QComboBox*>(ui->tableWidget_atomMappingTable->cellWidget(row, 3));
        QString type1Text = type1Line->text().split(" ")[0];
        type1Text = type1Text == "Ion" ? "0" : type1Text;
        QString type2Text = type2Line->currentText().split(" ")[0];
        // Create the map string.
        typeMapString += type1Text + " " + type2Text + ";";
    }
    params.set(api::parameters::speedup::CRITERIA_TYPE_MAP, api::value_t(typeMapString));

    params.set(api::parameters::speedup::OVERRIDE_CUTOFFS_ON,
               api::value_t(ui->groupBox_overrideActivationCutoffs->isChecked()));
    params.set(api::parameters::speedup::HOT_CUTOFF, api::value_t(ui->doubleSpinBox_hotCutoff->value()));
    params.set(api::parameters::speedup::COLD_CUTOFF, api::value_t(ui->doubleSpinBox_coldCutoff->value()));
    params.set(api::parameters::speedup::FIXED_CUTOFF, api::value_t(ui->doubleSpinBox_fixedCutoff->value()));

    return params;
}


void SpeedupWidget::updateTypeMappingTable(const api::BaseParameters& parameters) {
    if (m_entry == nullptr) return;

    // Clear the preivous table and disconnect the widgets there.
    ui->tableWidget_atomMappingTable->setRowCount(0);

    // Setup the new table by reading all loaded atom types
    QHash<int, AtomType> map;
    for (int key : m_entry->data.types.typeList.keys()) {
        if (key < 0) continue;
        map.insert(key, m_entry->data.types.typeList[key]);
    }
    QList<int> existingTypes = map.keys();
    std::sort(existingTypes.begin(), existingTypes.end());

    QList<int> types(existingTypes);
    types.insert(0, 0);
    std::sort(types.begin(), types.end());

    ui->tableWidget_atomMappingTable->setColumnCount(5);
    ui->tableWidget_atomMappingTable->setHorizontalHeaderItem(0, new QTableWidgetItem(""));
    ui->tableWidget_atomMappingTable->setHorizontalHeaderItem(1, new QTableWidgetItem(""));
    ui->tableWidget_atomMappingTable->setHorizontalHeaderItem(2, new QTableWidgetItem(""));
    ui->tableWidget_atomMappingTable->setHorizontalHeaderItem(3, new QTableWidgetItem(""));
    ui->tableWidget_atomMappingTable->setHorizontalHeaderItem(4, new QTableWidgetItem(""));

    ui->tableWidget_atomMappingTable->setRowCount(types.size());

    int tableHeight = 0;

    // Helper function to shrink the width of the QLineEdit to barely wrap the content.
    auto setLineEditWidth = [=](QLineEdit* lineEdit) {
        QString text = lineEdit->text();
        // use QFontMetrics this way;
        QFont font("", 0);
        QFontMetrics fm(font);
        int pixelsWide = fm.width(text) * 1.1;
        lineEdit->setFixedWidth(pixelsWide);
    };

    // Extract the type mapping from the parameters object
    QString typeMapString = parameters[api::parameters::speedup::CRITERIA_TYPE_MAP].value().toString();
    QStringList typeMapList = typeMapString.split(";", QString::SkipEmptyParts);
    QHash<int, int> criteriaMap;
    for (const QString& typeMap : typeMapList) {
        QStringList parts = typeMap.split(" ", QString::SkipEmptyParts);
        int type1 = parts[0].toInt();
        int type2 = parts[1].toInt();
        criteriaMap.insert(type1, type2);
    }

    for (auto currentType : types) {
        QLineEdit* column0 = new QLineEdit("Activate");
        column0->setReadOnly(true);
        column0->setFocusPolicy(Qt::NoFocus);
        column0->setAlignment(Qt::AlignLeft);
        column0->setFrame(false);
        setLineEditWidth(column0);

        // Type A
        QString typeA = currentType == 0 ? "Ion" : QString("%1 (%2)").arg(currentType).arg(map[currentType].symbol);
        QLineEdit* column1 = new QLineEdit(typeA);
        column1->setReadOnly(true);
        column1->setFocusPolicy(Qt::NoFocus);
        column1->setAlignment(Qt::AlignLeft);
        column1->setFrame(false);
        QFont font = column1->font();
        font.setWeight(QFont::Bold);
        column1->setFont(font);
        setLineEditWidth(column1);

        QLineEdit* column2 = new QLineEdit("with criteria of");
        column2->setReadOnly(true);
        column2->setFocusPolicy(Qt::NoFocus);
        column2->setAlignment(Qt::AlignLeft);
        column2->setFrame(false);
        setLineEditWidth(column2);

        // Type B
        QComboBox* column3 = new QComboBox();
        column3->setFocusPolicy(Qt::StrongFocus);
        column3->installEventFilter(new WheelGuard(column3));
        column3->setEditable(true);
        column3->lineEdit()->setReadOnly(true);
        column3->lineEdit()->setFocusPolicy(Qt::NoFocus);
        column3->lineEdit()->setAlignment(Qt::AlignCenter);
        column3->lineEdit()->setFrame(false);
        column3->setFont(font);
        // Only atom types present in the system are listed as valid options in the combo box.
        for (auto type : existingTypes) {
            column3->addItem(QString("%1 (%2)").arg(type).arg(map[type].symbol), type);
        }
        // default to the first available atom type
        column3->setCurrentIndex(0);

        // if there is a type map in the state, we want to set that as the current option
        int i = column3->findData(criteriaMap[currentType]);
        if (i >= 0) column3->setCurrentIndex(i);

        // Populate the table
        ui->tableWidget_atomMappingTable->setCellWidget(currentType, 0, column0);
        ui->tableWidget_atomMappingTable->setCellWidget(currentType, 1, column1);
        ui->tableWidget_atomMappingTable->setCellWidget(currentType, 2, column2);
        ui->tableWidget_atomMappingTable->setCellWidget(currentType, 3, column3);

        tableHeight += ui->tableWidget_atomMappingTable->rowHeight(currentType);
    }

    ui->tableWidget_atomMappingTable->resizeColumnsToContents();
    ui->tableWidget_atomMappingTable->resizeRowsToContents();
    ui->tableWidget_atomMappingTable->setFixedHeight(tableHeight);
    ui->tableWidget_atomMappingTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->tableWidget_atomMappingTable->horizontalHeader()->setVisible(false);
}
