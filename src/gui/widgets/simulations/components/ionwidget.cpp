/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "ionwidget.h"

#include "api/elementreader.h"
#include "api/util.h"
#include "gui/dialogs/chooseelementdialog.h"
#include "gui/widgets/utils/wheelguard.h"

#include <QHeaderView>
#include <QLayout>
#include <QMenu>
#include <QTableWidget>


IonWidget::IonWidget(QWidget* parent) : AbstractComponentWidget(parent) {

    m_comboBox_type = new QComboBox(this);

    connect(m_comboBox_type, QOverload<int>::of(&QComboBox::currentIndexChanged), [this] {
        int typeNumber = m_comboBox_type->currentData().toInt();
        if (m_entry->data.types.typeList.contains(typeNumber)) {
            const AtomType& type = m_entry->data.types.typeList[typeNumber];
            m_lineEdit_element->setText(type.symbol);
            m_doubleSpinBox_mass->setValue(type.mass);
            m_lineEdit_element->setEnabled(false);
            m_doubleSpinBox_mass->setEnabled(false);
        } else {
            m_lineEdit_element->setEnabled(true);
            m_doubleSpinBox_mass->setEnabled(true);
        }
        updateEntryTypes();
        emit modified();
    });


    m_lineEdit_element = new QLineEdit(this);
    m_lineEdit_element->setFixedWidth(30);
    connect(m_lineEdit_element, &QLineEdit::editingFinished, this, [=] {
        updateEntryTypes();
        emit modified();
    });

    m_doubleSpinBox_mass = new QDoubleSpinBox(this);
    m_doubleSpinBox_mass->setMaximum(9999.999999);
    m_doubleSpinBox_mass->setDecimals(6);
    connect(m_doubleSpinBox_mass, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [=] { emit modified(); });

    m_doubleSpinBox_energy = new QDoubleSpinBox(this);
    m_doubleSpinBox_energy->setDecimals(3);
    m_doubleSpinBox_energy->setMaximum(9999.999);
    connect(m_doubleSpinBox_energy, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });

    m_comboBox_energyUnit = new QComboBox(this);
    m_comboBox_energyUnit->addItem("eV", EnergyUnit::eV);
    m_comboBox_energyUnit->addItem("keV", EnergyUnit::keV);
    m_comboBox_energyUnit->addItem("MeV", EnergyUnit::MeV);
    connect(m_comboBox_energyUnit, &QComboBox::currentTextChanged, this, [=] { emit modified(); });

    m_checkBox_mergeWithBaseType = new QCheckBox(this);
    m_checkBox_mergeWithBaseType->setText("Merge");
    m_checkBox_mergeWithBaseType->setChecked(true);
    connect(m_checkBox_mergeWithBaseType, &QCheckBox::clicked, this, [=] {
        updateEntryTypes();
        emit modified();
    });

    connect(m_lineEdit_element, &QLineEdit::textEdited, [this](QString t) {
        if (t.length() > 2) m_lineEdit_element->setText(t.left(2));
    });

    QTableWidget* table = new QTableWidget(this);
    table->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
    table->setTabKeyNavigation(false);
    table->verticalHeader()->setVisible(false);
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table->setFocusPolicy(Qt::NoFocus);
    table->setSelectionMode(QAbstractItemView::NoSelection);
    table->setContextMenuPolicy(Qt::CustomContextMenu);
    table->setAutoScroll(false);

    table->setColumnCount(5);
    table->setRowCount(1);

    table->setFixedHeight(table->horizontalHeader()->height() + 62);
    table->setRowHeight(0, 60);

    table->setHorizontalHeaderItem(0, new QTableWidgetItem("Base Type"));
    table->setHorizontalHeaderItem(1, new QTableWidgetItem("Element"));
    table->setHorizontalHeaderItem(2, new QTableWidgetItem("Mass [u]"));
    table->setHorizontalHeaderItem(3, new QTableWidgetItem("Energy"));
    table->setHorizontalHeaderItem(4, new QTableWidgetItem(""));

    auto tableWidget = [=](QWidget* contentWidget) {
        QWidget* tableWidget = new QWidget(table);
        QHBoxLayout* contentLayout = new QHBoxLayout(table);
        contentLayout->addWidget(contentWidget);
        tableWidget->setLayout(contentLayout);
        return tableWidget;
    };

    QWidget* energyWidget = new QWidget(table);
    QHBoxLayout* energyLayout = new QHBoxLayout;
    energyLayout->addWidget(m_doubleSpinBox_energy);
    energyLayout->addWidget(m_comboBox_energyUnit);
    energyWidget->setLayout(energyLayout);

    QWidget* mergeWidget = new QWidget(table);
    QHBoxLayout* mergeLayout = new QHBoxLayout;
    mergeLayout->addWidget(m_checkBox_mergeWithBaseType);
    mergeLayout->addItem(new QSpacerItem(1, 20, QSizePolicy::Expanding, QSizePolicy::MinimumExpanding));
    mergeWidget->setLayout(mergeLayout);
    mergeWidget->setToolTip("Merge the ions with the selected base type at the end of the simulation?");

    table->setCellWidget(0, 0, tableWidget(m_comboBox_type));
    table->setCellWidget(0, 1, tableWidget(m_lineEdit_element));
    table->setCellWidget(0, 2, tableWidget(m_doubleSpinBox_mass));
    table->setCellWidget(0, 3, energyWidget);
    table->setCellWidget(0, 4, mergeWidget);

    table->resizeColumnsToContents();
    table->horizontalHeader()->setStretchLastSection(true);
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    connect(table, &QTableWidget::customContextMenuRequested, [this, table](const QPoint& pos) {
        QMenu contextMenu("Context menu", this);
        QAction* ptAction = new QAction("Periodic Table", &contextMenu);
        ptAction->setEnabled(m_lineEdit_element->isEnabled());
        contextMenu.addAction(ptAction);
        connect(ptAction, &QAction::triggered, [this] {
            QString symbol = m_lineEdit_element->text();
            double mass = m_doubleSpinBox_mass->value();
            ChooseElementDialog e(this);
            if (e.exec()) {
                ElementReader r;
                symbol = r.getSymbol(e.Z());
                mass = r.getMass(e.Z());
            }
            m_lineEdit_element->setText(symbol);
            m_doubleSpinBox_mass->setValue(mass);
        });
        contextMenu.exec(table->mapToGlobal(pos) + QPoint(0, contextMenu.sizeHint().height() / 2));
    });

    QVBoxLayout* l = new QVBoxLayout;
    l->addWidget(table);
    setLayout(l);


    QVector<QWidget*> wheelGuardedWidgets = {m_comboBox_type, m_comboBox_energyUnit, m_doubleSpinBox_mass,
                                             m_doubleSpinBox_energy};
    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(new WheelGuard(w));
    }
}


void IonWidget::setEntry(ExtendedEntryData* entry) {
    AbstractComponentWidget::setEntry(entry);

    QMap<int, QString> types;
    for (int i = 1; i < 10; ++i) {
        if (entry->data.types.typeList.contains(i)) {
            AtomType t = entry->data.types.typeList[i];
            types[i] = QString("%1 (%2)").arg(QString::number(i), t.symbol);
        } else {
            types[i] = QString::number(i);
        }
    }

    m_comboBox_type->clear();
    for (int i : types.keys()) {
        m_comboBox_type->addItem(types[i], i);
    }
}


void IonWidget::setParameters(api::BaseParameters parameters) {
    if (entry() == nullptr) return;

    QSignalBlocker blocker1(m_comboBox_type);
    QSignalBlocker blocker2(m_lineEdit_element);
    QSignalBlocker blocker3(m_doubleSpinBox_mass);
    QSignalBlocker blocker4(m_doubleSpinBox_energy);
    QSignalBlocker blocker5(m_comboBox_energyUnit);
    QSignalBlocker blocker6(m_checkBox_mergeWithBaseType);

    m_comboBox_type->setCurrentIndex(
        m_comboBox_type->findData(parameters[api::parameters::ion::BASE_TYPE].value().toInt()));

    //    double energy = m_state->data().energy;
    double energy = parameters[api::parameters::ion::ENERGY].value().toDouble();
    if (energy >= 1000) {
        m_comboBox_energyUnit->setCurrentText("keV");
        energy /= 1000;
        if (energy >= 1000) {
            m_comboBox_energyUnit->setCurrentText("MeV");
            energy /= 1000;
        }
    }

    util::updateDoubleSpinBoxValue(m_doubleSpinBox_energy, energy);

    ElementReader reader;
    int zBaseType = parameters[api::parameters::ion::BASE_TYPE_ATOM_NUMBER].value().toInt();
    m_lineEdit_element->setText(reader.getSymbol(zBaseType));
    util::updateDoubleSpinBoxValue(m_doubleSpinBox_mass, reader.getMass(zBaseType));

    m_checkBox_mergeWithBaseType->setChecked(parameters[api::parameters::ion::BASE_TYPE_MERGE_ON].value().toBool());

    updateEntryTypes();
}


api::BaseParameters IonWidget::parameters() {
    double energy = m_doubleSpinBox_energy->value();
    EnergyUnit unit = static_cast<EnergyUnit>(m_comboBox_energyUnit->currentData().toInt());
    if (unit == keV)
        energy *= 1000;
    else if (unit == MeV)
        energy *= 1000000;


    int atomNumber = -1;
    if (entry() != nullptr) {
        atomNumber = entry()->data.types.allTypes[0].atomNumber;
    }

    api::BaseParameters params;

    params.set(api::parameters::ion::ENERGY, api::value_t(energy));
    params.set(api::parameters::ion::BASE_TYPE, api::value_t(m_comboBox_type->currentData().toInt()));
    if (m_checkBox_mergeWithBaseType->isChecked()) {
        params.set(api::parameters::ion::BASE_TYPE_ATOM_NUMBER, api::value_t(atomNumber));
    } else {
        params.set(api::parameters::ion::BASE_TYPE_ATOM_NUMBER, api::value_t(-1));
    }
    params.set(api::parameters::ion::ION_TYPE, api::value_t(0));
    params.set(api::parameters::ion::ION_TYPE_ATOM_NUMBER, api::value_t(atomNumber));
    params.set(api::parameters::ion::BASE_TYPE_MERGE_ON, api::value_t(m_checkBox_mergeWithBaseType->isChecked()));

    return params;
}


void IonWidget::updateEntryTypes() {
    ElementReader reader;

    QString ionSymbol = m_lineEdit_element->text();
    int ionZ = reader.zFromSymbol(ionSymbol);

    m_entry->data.types.removeInjectedTypes();

    // If no ion is selected, we don't want to inject anything new.
    if (ionZ < 1) {
        return;
    }

    AtomType ionType;
    ionType.typeNumber = 0;
    ionType.atomNumber = ionZ;
    ionType.mass = reader.getMass(ionZ);
    ionType.symbol = ionSymbol;

    // Inject the ion type
    m_entry->data.types.injectAtomType(ionType);

    // If the ionMergeType is present, inject that too!
    if (m_checkBox_mergeWithBaseType->isChecked()) {
        AtomType mergeType;
        mergeType.typeNumber = m_comboBox_type->currentData().toInt();
        mergeType.atomNumber = ionZ;
        mergeType.mass = reader.getMass(ionZ);
        mergeType.symbol = ionSymbol;

        m_entry->data.types.injectAtomType(mergeType);
    }

    m_lineEdit_element->setText(ionType.symbol);
    m_doubleSpinBox_mass->setValue(ionType.mass);

    emit entryTypesUpdated();
}
