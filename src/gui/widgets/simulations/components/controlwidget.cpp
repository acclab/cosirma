/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "controlwidget.h"

#include "gui/widgets/utils/collapsiblegroupbox.h"

#include <QDebug>
#include <QLayout>


ControlWidget::ControlWidget(QWidget* parent) : AbstractComponentWidget(parent) {

    QVBoxLayout* layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);

    auto createCollapsibleGroupBox = [=](QWidget* contentWidget, const QString& title) {
        QVBoxLayout* contentLayout = new QVBoxLayout;
        contentLayout->addWidget(contentWidget);

        CollapsibleGroupBox* groupBox = new CollapsibleGroupBox(this);
        groupBox->setTitle(title);
        groupBox->setLayout(contentLayout);
        return groupBox;
    };

    m_timeWidget = new TimeWidget(this);
    m_temperatureWidget = new TemperatureControlWidget(this);
    m_pressureWidget = new PressureControlWidget(this);
    m_outputWidget = new OutputWidget(this);

    layout->addWidget(createCollapsibleGroupBox(m_timeWidget, "Time Parameters"));
    layout->addWidget(createCollapsibleGroupBox(m_temperatureWidget, "Temperature Control"));
    layout->addWidget(createCollapsibleGroupBox(m_pressureWidget, "Pressure Control"));
    layout->addWidget(createCollapsibleGroupBox(m_outputWidget, "Output Parameters"));

    setLayout(layout);

    // Passing through the modification signals
    connect(m_timeWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified time widget";
        emit modified();
    });
    connect(m_temperatureWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified temperature widget";
        emit modified();
    });
    connect(m_pressureWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified pressure widget";
        emit modified();
    });
    connect(m_outputWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified output widget";
        emit modified();
    });
}


void ControlWidget::setEntry(ExtendedEntryData* entry) {
    AbstractComponentWidget::setEntry(entry);
    m_temperatureWidget->setEntry(entry);
}


void ControlWidget::setParameters(api::BaseParameters parameters) {
    m_timeWidget->setParameters(parameters);
    m_temperatureWidget->setParameters(parameters);
    m_pressureWidget->setParameters(parameters);
    m_outputWidget->setParameters(parameters);
}


api::BaseParameters ControlWidget::parameters() {
    api::BaseParameters params;
    params.insert(m_timeWidget->parameters());
    params.insert(m_temperatureWidget->parameters());
    params.insert(m_pressureWidget->parameters());
    params.insert(m_outputWidget->parameters());
    return params;
}


void ControlWidget::connectToPlot(PlotCellGLWidget* glWidget) {
    m_temperatureWidget->connectToPlot(glWidget);
}


void ControlWidget::disconnectFromPlot(PlotCellGLWidget* glWidget) {
    m_temperatureWidget->disconnectFromPlot(glWidget);
}


void ControlWidget::showControlRegion(bool show) {
    m_temperatureWidget->showControlRegion(show);
}
