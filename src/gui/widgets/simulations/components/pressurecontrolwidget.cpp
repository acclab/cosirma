/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "pressurecontrolwidget.h"
#include "ui_pressurecontrolwidget.h"

#include "api/state/enums.h"
#include "api/util.h"

#include "gui/widgets/utils/wheelguard.h"

#include <QDebug>


PressureControlWidget::PressureControlWidget(QWidget* parent)
        : AbstractComponentWidget(parent), ui(new Ui::PressureControlWidget) {
    ui->setupUi(this);

    ui->comboBox_mode->addItem("None", api::enums::pressurecontrol::None);
    ui->comboBox_mode->addItem("Isobaric", api::enums::pressurecontrol::Isobaric);
    ui->comboBox_mode->addItem("Isotropic", api::enums::pressurecontrol::Isotropic);

    QList<QWidget*> wheelGuardedWidgets = {ui->comboBox_mode, ui->doubleSpinBox_beta, ui->doubleSpinBox_damping,
                                           ui->doubleSpinBox_pressure};

    WheelGuard* guard = new WheelGuard(this);
    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(guard);
    }

    setFocusProxy(ui->comboBox_mode);
    ui->comboBox_mode->setCurrentIndex(-1);

    connect(ui->comboBox_mode, QOverload<int>::of(&QComboBox::currentIndexChanged), [this] {
        if (static_cast<api::enums::pressurecontrol::Mode>(ui->comboBox_mode->currentData().toInt()) ==
            api::enums::pressurecontrol::None) {
            ui->groupBox_controls->setVisible(false);
        } else {
            ui->groupBox_controls->setVisible(true);
        }
        emit modified();
    });

    connect(ui->doubleSpinBox_pressure, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_damping, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_beta, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });

    ui->comboBox_mode->setCurrentIndex(api::enums::pressurecontrol::None);
}


PressureControlWidget::~PressureControlWidget() {
    delete ui;
}


void PressureControlWidget::setParameters(api::BaseParameters parameters) {
    QSignalBlocker blocker1(ui->comboBox_mode);
    QSignalBlocker blocker2(ui->doubleSpinBox_pressure);
    QSignalBlocker blocker3(ui->doubleSpinBox_damping);
    QSignalBlocker blocker4(ui->doubleSpinBox_beta);

    ui->comboBox_mode->setCurrentIndex(parameters[api::parameters::pressure::MODE].value().toInt());
    ui->doubleSpinBox_pressure->setValue(parameters[api::parameters::pressure::PRESSURE].value().toDouble());
    ui->doubleSpinBox_damping->setValue(parameters[api::parameters::pressure::DAMPING].value().toDouble());
    ui->doubleSpinBox_beta->setValue(parameters[api::parameters::pressure::BETA].value().toDouble());
}


api::BaseParameters PressureControlWidget::parameters() {
    api::BaseParameters params;
    params.set(api::parameters::pressure::MODE, api::value_t(ui->comboBox_mode->currentData().toInt()));
    params.set(api::parameters::pressure::PRESSURE, api::value_t(ui->doubleSpinBox_pressure->value()));
    params.set(api::parameters::pressure::DAMPING, api::value_t(ui->doubleSpinBox_damping->value()));
    params.set(api::parameters::pressure::BETA, api::value_t(ui->doubleSpinBox_beta->value()));
    return params;
}
