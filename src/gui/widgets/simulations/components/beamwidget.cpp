/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "beamwidget.h"
#include "ui_beamwidget.h"

#include "api/elementreader.h"
#include "api/state/enums.h"
#include "api/util.h"

#include "gui/dialogs/chooseelementdialog.h"
#include "gui/widgets/utils/wheelguard.h"

#include <QDebug>

#include <glm/vec3.hpp>

BeamWidget::BeamWidget(QWidget* parent) : AbstractComponentWidget(parent), ui(new Ui::BeamWidget) {
    ui->setupUi(this);

    ui->comboBox_beamType->addItem("Broad", api::enums::beam::Broad);
    ui->comboBox_beamType->addItem("Focused", api::enums::beam::Focused);
    connect(ui->comboBox_beamType, QOverload<int>::of(&QComboBox::currentIndexChanged), this, [=] {
        api::enums::beam::Type type = static_cast<api::enums::beam::Type>(ui->comboBox_beamType->currentData().toInt());
        switch (type) {
            case api::enums::beam::Broad:
                ui->widget_beamShapeSettings->setVisible(false);
                ui->groupBox_broad->setVisible(true);
                ui->groupBox_focused->setVisible(false);
                ui->comboBox_broadDirection->setVisible(true);
                break;
            case api::enums::beam::Focused:
                ui->widget_beamShapeSettings->setVisible(true);
                ui->groupBox_broad->setVisible(false);
                ui->groupBox_focused->setVisible(true);
                ui->comboBox_broadDirection->setVisible(false);
                break;
        }
    });
    ui->comboBox_beamType->setCurrentIndex(-1);

    ui->comboBox_beamShape->addItem("Circular", api::enums::beam::Circular);
    ui->comboBox_beamShape->addItem("Square", api::enums::beam::Square);

    ui->comboBox_broadDirection->addItem("Left", api::enums::beam::X_lower);
    ui->comboBox_broadDirection->addItem("Right", api::enums::beam::X_upper);
    ui->comboBox_broadDirection->addItem("Front", api::enums::beam::Y_lower);
    ui->comboBox_broadDirection->addItem("Back", api::enums::beam::Y_upper);
    ui->comboBox_broadDirection->addItem("Bottom", api::enums::beam::Z_lower);
    ui->comboBox_broadDirection->addItem("Top", api::enums::beam::Z_upper);

    // Protect against fields catching the scroll
    QVector<QWidget*> wheelGuardedWidgets = {
        ui->doubleSpinBox_xPosition, ui->doubleSpinBox_yPosition,  ui->doubleSpinBox_zPosition,
        ui->jumpSlider_phi,          ui->jumpSlider_theta,         ui->doubleSpinBox_phi,
        ui->doubleSpinBox_theta,     ui->comboBox_beamShape,       ui->comboBox_beamType,
        ui->doubleSpinBox_beamWidth, ui->jumpSlider_azimuth,       ui->jumpSlider_inclination,
        ui->doubleSpinBox_azimuth,   ui->doubleSpinBox_inclination};

    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(new WheelGuard(w));
    }

    connect(ui->comboBox_beamType, QOverload<int>::of(&QComboBox::currentIndexChanged), [this] {
        emit beamUpdated();
        emit modified();
    });
    connect(ui->comboBox_broadDirection, QOverload<int>::of(&QComboBox::currentIndexChanged), [this] {
        emit beamUpdated();
        emit modified();
    });
    connect(ui->comboBox_beamShape, QOverload<int>::of(&QComboBox::currentIndexChanged), [this] {
        emit beamUpdated();
        emit modified();
    });

    connect(ui->doubleSpinBox_beamWidth, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] {
        emit beamUpdated();
        emit modified();
    });


    connect(ui->doubleSpinBox_xPosition, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] {
        emit beamUpdated();
        emit modified();
    });
    connect(ui->doubleSpinBox_yPosition, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] {
        emit beamUpdated();
        emit modified();
    });
    connect(ui->doubleSpinBox_zPosition, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] {
        emit beamUpdated();
        emit modified();
    });


    connect(ui->jumpSlider_phi, &JumpSlider::valueChanged, [this](int value) {
        ui->doubleSpinBox_phi->setValue(value);
        // The signals are emitted from the other value changed function.
    });
    connect(ui->doubleSpinBox_phi, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double value) {
        ui->jumpSlider_phi->setValue(value);
        emit beamUpdated();
        emit modified();
    });

    connect(ui->jumpSlider_theta, &JumpSlider::valueChanged, [this](int value) {
        ui->doubleSpinBox_theta->setValue(value);
        // The signals are emitted from the other value changed function.
    });
    connect(ui->doubleSpinBox_theta, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double value) {
        ui->jumpSlider_theta->setValue(value);
        emit beamUpdated();
        emit modified();
    });

    connect(ui->jumpSlider_azimuth, &JumpSlider::valueChanged, [this](int value) {
        ui->doubleSpinBox_azimuth->setValue(value);
        // The signals are emitted from the other value changed function.
    });
    connect(ui->doubleSpinBox_azimuth, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double value) {
        ui->jumpSlider_azimuth->setValue(value);
        emit beamUpdated();
        emit modified();
    });

    connect(ui->jumpSlider_inclination, &JumpSlider::valueChanged, [this](int value) {
        ui->doubleSpinBox_inclination->setValue(value);
        // The signals are emitted from the other value changed function.
    });
    connect(ui->doubleSpinBox_inclination, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double value) {
        ui->jumpSlider_inclination->setValue(value);
        emit beamUpdated();
        emit modified();
    });
}


BeamWidget::~BeamWidget() {
    delete ui;
}


void BeamWidget::setParameters(api::BaseParameters parameters) {

    QSignalBlocker blocker1(ui->comboBox_beamType);
    QSignalBlocker blocker2(ui->comboBox_broadDirection);
    QSignalBlocker blocker3(ui->comboBox_beamShape);
    QSignalBlocker blocker4(ui->doubleSpinBox_beamWidth);
    QSignalBlocker blocker5(ui->doubleSpinBox_inclination);
    QSignalBlocker blocker6(ui->doubleSpinBox_azimuth);
    QSignalBlocker blocker7(ui->doubleSpinBox_xPosition);
    QSignalBlocker blocker8(ui->doubleSpinBox_yPosition);
    QSignalBlocker blocker9(ui->doubleSpinBox_zPosition);
    QSignalBlocker blocker10(ui->doubleSpinBox_phi);
    QSignalBlocker blocker11(ui->doubleSpinBox_theta);

    api::enums::beam::Type type =
        static_cast<api::enums::beam::Type>(parameters[api::parameters::beam::TYPE].value().toInt());
    api::enums::beam::Face face =
        static_cast<api::enums::beam::Face>(parameters[api::parameters::beam::FACE].value().toInt());
    api::enums::beam::Shape shape =
        static_cast<api::enums::beam::Shape>(parameters[api::parameters::beam::SHAPE].value().toInt());
    double width = parameters[api::parameters::beam::WIDTH].value().toDouble();
    double inclination = parameters[api::parameters::beam::INCLINATION].value().toDouble();
    double azimuth = parameters[api::parameters::beam::AZIMUTH].value().toDouble();

    double xPosition = parameters[api::parameters::ion_transform::X_POSITION].value().toDouble();
    double yPosition = parameters[api::parameters::ion_transform::Y_POSITION].value().toDouble();
    double zPosition = parameters[api::parameters::ion_transform::Z_POSITION].value().toDouble();
    double phi = parameters[api::parameters::ion_transform::PHI].value().toDouble();
    double theta = parameters[api::parameters::ion_transform::THETA].value().toDouble();

    ui->comboBox_beamType->setCurrentIndex(ui->comboBox_beamType->findData(type));
    ui->comboBox_broadDirection->setCurrentIndex(ui->comboBox_broadDirection->findData(face));
    ui->comboBox_beamShape->setCurrentIndex(ui->comboBox_beamShape->findData(shape));

    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_beamWidth, width);
    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_xPosition, xPosition);
    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_yPosition, yPosition);
    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_zPosition, zPosition);

    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_theta, theta);
    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_phi, phi);
    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_azimuth, azimuth);
    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_inclination, inclination);
}


api::BaseParameters BeamWidget::parameters() {

    api::BaseParameters params;

    params.set(api::parameters::beam::TYPE, api::value_t(ui->comboBox_beamType->currentData().toInt()));
    params.set(api::parameters::beam::FACE, api::value_t(ui->comboBox_broadDirection->currentData().toInt()));
    params.set(api::parameters::beam::SHAPE, api::value_t(ui->comboBox_beamShape->currentData().toInt()));
    params.set(api::parameters::beam::WIDTH, api::value_t(ui->doubleSpinBox_beamWidth->value()));
    params.set(api::parameters::beam::INCLINATION, api::value_t(ui->doubleSpinBox_inclination->value()));
    params.set(api::parameters::beam::AZIMUTH, api::value_t(ui->doubleSpinBox_azimuth->value()));

    params.set(api::parameters::ion_transform::X_POSITION, api::value_t(ui->doubleSpinBox_xPosition->value()));
    params.set(api::parameters::ion_transform::Y_POSITION, api::value_t(ui->doubleSpinBox_yPosition->value()));
    params.set(api::parameters::ion_transform::Z_POSITION, api::value_t(ui->doubleSpinBox_zPosition->value()));
    params.set(api::parameters::ion_transform::PHI, api::value_t(ui->doubleSpinBox_phi->value()));
    params.set(api::parameters::ion_transform::THETA, api::value_t(ui->doubleSpinBox_theta->value()));

    return params;
}
