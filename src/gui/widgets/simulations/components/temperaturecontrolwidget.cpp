/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "temperaturecontrolwidget.h"
#include "ui_temperaturecontrolwidget.h"

#include "api/project.h"
#include "api/state/enums.h"
#include "api/state/parameters/parameterkeys.h"
#include "api/util.h"

#include "gui/widgets/utils/wheelguard.h"

#include <QDebug>


TemperatureControlWidget::TemperatureControlWidget(QWidget* parent)
        : AbstractComponentWidget(parent), ui(new Ui::TemperatureControlWidget) {
    ui->setupUi(this);

    ui->comboBox_mode->addItem("None", api::enums::temperaturecontrol::None);
    ui->comboBox_mode->addItem("All Atoms", api::enums::temperaturecontrol::AllAtoms);
    ui->comboBox_mode->addItem("Borders", api::enums::temperaturecontrol::Borders);
    ui->comboBox_mode->setCurrentIndex(-1);

    connect(ui->comboBox_mode, QOverload<int>::of(&QComboBox::currentIndexChanged), [this] {
        switch (static_cast<api::enums::temperaturecontrol::Mode>(ui->comboBox_mode->currentData().toInt())) {
            case api::enums::temperaturecontrol::None:
                showNone();
                break;
            case api::enums::temperaturecontrol::AllAtoms:
                showLinear();
                break;
            case api::enums::temperaturecontrol::Borders:
                showBorder();
                break;
            default:
                showNone();
        }
        onChangeCoolingRegions();
        emit modified();
    });

    //! \todo: Use this for showing a short informative text to the user
    ui->label_information->setVisible(false);

    // Helper lambda for setting up all of the range slider boxes.
    auto setupRangeSlider = [=](RangeSliderBox* slider, bool mirrored, bool highlightInside, bool distanceFromLimit) {
        slider->setMirrored(mirrored);
        slider->setHighlightInside(highlightInside);
        slider->setShowDistanceFromLimit(distanceFromLimit);
        slider->setPrecision(2);
    };

    setupRangeSlider(ui->widget_rangeSliderX, true, false, true);
    setupRangeSlider(ui->widget_rangeSliderY, true, false, true);
    setupRangeSlider(ui->widget_rangeSliderZ, false, true, false);


    auto initializePushButton = [&](QPushButton* button) {
        button->setFixedWidth(40);
        button->setText("Hide");
        button->setChecked(true);
        connect(button, &QPushButton::clicked, [this, button](bool checked) {
            button->setText(checked ? "Hide" : "Show");
            onChangeCoolingRegions();
        });
    };

    initializePushButton(ui->pushButton_hideX);
    initializePushButton(ui->pushButton_hideY);
    initializePushButton(ui->pushButton_hideZ);


    QList<QWidget*> wheelGuardedWidgets{ui->comboBox_mode,
                                        ui->doubleSpinBox_quenchTime,
                                        ui->doubleSpinBox_quenchTemp,
                                        ui->doubleSpinBox_temperature,
                                        ui->doubleSpinBox_damping,
                                        ui->doubleSpinBox_quenchRate};

    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(new WheelGuard(w));
    }


    // Helper lambda for setting the checked state on either of the X, Y, or Z buttons.
    auto updatePushButtonState = [=](QPushButton* button) {
        button->setChecked(true);
        button->setText("Hide");
    };

    // Helper lambda for enabling or disabling the widget row consisting of a RangeSliderBox and a PushButton.
    auto enableWidgetRow = [=](bool checked, RangeSliderBox* rangeSlider, QPushButton* button) {
        rangeSlider->setEnabled(checked);
        button->setEnabled(checked);
        if (checked) {
            updatePushButtonState(button);
        }
        onChangeCoolingRegions();
    };


    // Connect to the enabling lambda
    connect(ui->checkBox_enableX, &QCheckBox::clicked, [=](bool checked) {
        enableWidgetRow(checked, ui->widget_rangeSliderX, ui->pushButton_hideX);
        emit modified();
    });
    connect(ui->checkBox_enableY, &QCheckBox::clicked, [=](bool checked) {
        enableWidgetRow(checked, ui->widget_rangeSliderY, ui->pushButton_hideY);
        emit modified();
    });
    connect(ui->checkBox_enableZ, &QCheckBox::clicked, [=](bool checked) {
        enableWidgetRow(checked, ui->widget_rangeSliderZ, ui->pushButton_hideZ);
        emit modified();
    });


    // Helper lambda for updating the button if it hasn't been clicked before sliding the range slider
    auto rangeSliderChanged = [=](QPushButton* button) {
        if (!button->isChecked()) updatePushButtonState(button);
        onChangeCoolingRegions();
    };

    // Connect to the range slider changed event listener
    connect(ui->widget_rangeSliderX, &RangeSliderBox::rangeEdited, [=] {
        rangeSliderChanged(ui->pushButton_hideX);
        emit modified();
    });
    connect(ui->widget_rangeSliderY, &RangeSliderBox::rangeEdited, [=] {
        rangeSliderChanged(ui->pushButton_hideY);
        emit modified();
    });
    connect(ui->widget_rangeSliderZ, &RangeSliderBox::rangeEdited, [=] {
        rangeSliderChanged(ui->pushButton_hideZ);
        emit modified();
    });


    connect(ui->groupBox_quench, &QGroupBox::clicked, this, [=] { emit modified(); });
    connect(ui->doubleSpinBox_quenchTime, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_quenchTemp, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_quenchRate, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_temperature, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_damping, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });


    ui->comboBox_mode->setCurrentIndex(api::enums::temperaturecontrol::AllAtoms);
}


TemperatureControlWidget::~TemperatureControlWidget() {
    // Remove the m_region from the canvas before deleting it. Otherwise the canvas will try to delete it again later.
    disconnectFromPlot(nullptr);
    delete m_region;
    delete ui;
}


void TemperatureControlWidget::onChangeCoolingRegions() {
    if (m_region == nullptr) {
        qDebug() << "the region hasn't been connected to the GL Widget yet!";
        return;
    }
    if (entry() == nullptr) {
        qDebug() << "The entry data is needed to get the extents of the cooling region, but it hasn't been set yet...";
        return;
    }

    const Box& lims = m_entry->frame.cellLims;
    double xCoolingWidth = lims.x2 - ui->widget_rangeSliderX->upperValue();
    double yCoolingWidth = lims.y2 - ui->widget_rangeSliderY->upperValue();
    double zCoolingLower = ui->widget_rangeSliderZ->lowerValue();
    double zCoolingUpper = ui->widget_rangeSliderZ->upperValue();

    bool xCooling = ui->checkBox_enableX->isChecked();
    bool yCooling = ui->checkBox_enableY->isChecked();
    bool zCooling = ui->checkBox_enableZ->isChecked();

    if (ui->comboBox_mode->currentData().toInt() == api::enums::temperaturecontrol::Borders) {
        m_region->onUpdateValues(lims, xCoolingWidth, yCoolingWidth, zCoolingLower, zCoolingUpper);
        m_region->onUpdateEnabledRegions(xCooling && ui->pushButton_hideX->isChecked(),
                                         yCooling && ui->pushButton_hideY->isChecked(),
                                         zCooling && ui->pushButton_hideZ->isChecked());
    } else {
        m_region->onUpdateEnabledRegions(false, false, false);
    }

    if (m_glWidget) m_glWidget->update();
}


void TemperatureControlWidget::showControlRegion(bool show) {
    m_region->onShow(show);
}


void TemperatureControlWidget::setTime(double time) {
    if (ui->doubleSpinBox_quenchTime->maximum() < 10 * time) ui->doubleSpinBox_quenchTime->setMaximum(10 * time);
}


void TemperatureControlWidget::connectToPlot(PlotCellGLWidget* glWidget) {
    m_glWidget = glWidget;

    // Create the ControlBox region model if it this already exist.
    if (m_region == nullptr) {
        m_region = new ControlRegionModel();
        // Update the GL Widget to make the regions visible.
        onChangeCoolingRegions();
    }

    m_glWidget->addModel(m_region);
}


void TemperatureControlWidget::disconnectFromPlot(PlotCellGLWidget* /*glWidget*/) {
    if (!m_glWidget) return;
    m_glWidget->removeModel(m_region);
    m_glWidget = nullptr;
}


void TemperatureControlWidget::setEntry(ExtendedEntryData* entry) {
    AbstractComponentWidget::setEntry(entry);

    Box b = entry->frame.cellLims;
    ui->widget_rangeSliderX->setLimits(b.x1, b.x2);
    ui->widget_rangeSliderY->setLimits(b.y1, b.y2);
    ui->widget_rangeSliderZ->setLimits(b.z1, b.z2);

    onChangeCoolingRegions();
}


void TemperatureControlWidget::setParameters(api::BaseParameters parameters) {
    if (m_entry == nullptr) return;

//    QSignalBlocker blocker1(ui->checkBox_enableX);
//    QSignalBlocker blocker2(ui->checkBox_enableY);
//    QSignalBlocker blocker3(ui->checkBox_enableZ);
//    QSignalBlocker blocker4(ui->pushButton_hideX);
//    QSignalBlocker blocker5(ui->widget_rangeSliderX);
//    QSignalBlocker blocker6(ui->pushButton_hideY);
//    QSignalBlocker blocker7(ui->widget_rangeSliderY);
//    QSignalBlocker blocker8(ui->pushButton_hideZ);
//    QSignalBlocker blocker9(ui->widget_rangeSliderZ);
//    QSignalBlocker blocker10(ui->widget_rangeSliderX);
//    QSignalBlocker blocker11(ui->widget_rangeSliderY);
//    QSignalBlocker blocker12(ui->widget_rangeSliderZ);
//    QSignalBlocker blocker13(ui->groupBox_quench);
//    QSignalBlocker blocker14(ui->doubleSpinBox_quenchTime);
//    QSignalBlocker blocker15(ui->doubleSpinBox_quenchTemp);
//    QSignalBlocker blocker16(ui->doubleSpinBox_quenchRate);
//    QSignalBlocker blocker17(ui->doubleSpinBox_temperature);
//    QSignalBlocker blocker18(ui->doubleSpinBox_damping);
//    QSignalBlocker blocker19(ui->comboBox_mode);

    ui->checkBox_enableX->setChecked(parameters[api::parameters::temperature::X_COOLING_ON].value().toBool());
    ui->checkBox_enableY->setChecked(parameters[api::parameters::temperature::Y_COOLING_ON].value().toBool());
    ui->checkBox_enableZ->setChecked(parameters[api::parameters::temperature::Z_COOLING_ON].value().toBool());

    ui->pushButton_hideX->setEnabled(parameters[api::parameters::temperature::X_COOLING_ON].value().toBool());
    ui->widget_rangeSliderX->setEnabled(parameters[api::parameters::temperature::X_COOLING_ON].value().toBool());

    ui->pushButton_hideY->setEnabled(parameters[api::parameters::temperature::Y_COOLING_ON].value().toBool());
    ui->widget_rangeSliderY->setEnabled(parameters[api::parameters::temperature::Y_COOLING_ON].value().toBool());

    ui->pushButton_hideZ->setEnabled(parameters[api::parameters::temperature::Z_COOLING_ON].value().toBool());
    ui->widget_rangeSliderZ->setEnabled(parameters[api::parameters::temperature::Z_COOLING_ON].value().toBool());

    Box b = m_entry->frame.cellLims;
    ui->widget_rangeSliderX->setUpperValue(
        b.x2 - parameters[api::parameters::temperature::X_COOLING_WIDTH].value().toDouble());
    ui->widget_rangeSliderY->setUpperValue(
        b.y2 - parameters[api::parameters::temperature::Y_COOLING_WIDTH].value().toDouble());
    ui->widget_rangeSliderZ->setLowerValue(
        parameters[api::parameters::temperature::Z_COOLING_LOWER].value().toDouble());
    ui->widget_rangeSliderZ->setUpperValue(
        parameters[api::parameters::temperature::Z_COOLING_UPPER].value().toDouble());

    ui->groupBox_quench->setChecked(parameters[api::parameters::temperature::QUENCH_ON].value().toBool());

    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_quenchTime,
                                   parameters[api::parameters::temperature::QUENCH_START_TIME].value().toDouble());
    util::updateDoubleSpinBoxValue(
        ui->doubleSpinBox_quenchTemp,
        parameters[api::parameters::temperature::QUENCH_TARGET_TEMPERATURE].value().toDouble());
    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_quenchRate,
                                   parameters[api::parameters::temperature::QUENCH_RATE].value().toDouble());
    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_temperature,
                                   parameters[api::parameters::temperature::TARGET_TEMPERATURE].value().toDouble());
    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_damping,
                                   parameters[api::parameters::temperature::DAMPING].value().toDouble());


    api::enums::temperaturecontrol::Mode mode = static_cast<api::enums::temperaturecontrol::Mode>(
        parameters[api::parameters::temperature::MODE].value().toInt());
    ui->comboBox_mode->setCurrentIndex(ui->comboBox_mode->findData(mode));

    onChangeCoolingRegions();
}


api::BaseParameters TemperatureControlWidget::parameters() {

    double xWidth = 0.0;
    double yWidth = 0.0;
    if (entry() != nullptr) {
        const Box& lims = m_entry->frame.cellLims;
        xWidth = lims.x2 - ui->widget_rangeSliderX->upperValue();
        yWidth = lims.y2 - ui->widget_rangeSliderY->upperValue();
    }

    api::BaseParameters params;

    params.set(api::parameters::temperature::MODE, api::value_t(ui->comboBox_mode->currentData().toInt()));
    params.set(api::parameters::temperature::X_COOLING_ON, api::value_t(ui->checkBox_enableX->isChecked()));
    params.set(api::parameters::temperature::Y_COOLING_ON, api::value_t(ui->checkBox_enableY->isChecked()));
    params.set(api::parameters::temperature::Z_COOLING_ON, api::value_t(ui->checkBox_enableZ->isChecked()));
    params.set(api::parameters::temperature::X_COOLING_WIDTH, api::value_t(xWidth));
    params.set(api::parameters::temperature::Y_COOLING_WIDTH, api::value_t(yWidth));
    params.set(api::parameters::temperature::Z_COOLING_UPPER, api::value_t(ui->widget_rangeSliderZ->upperValue()));
    params.set(api::parameters::temperature::Z_COOLING_LOWER, api::value_t(ui->widget_rangeSliderZ->lowerValue()));
    params.set(api::parameters::temperature::QUENCH_ON, api::value_t(ui->groupBox_quench->isChecked()));
    params.set(api::parameters::temperature::QUENCH_START_TIME, api::value_t(ui->doubleSpinBox_quenchTime->value()));
    params.set(api::parameters::temperature::QUENCH_TARGET_TEMPERATURE,
               api::value_t(ui->doubleSpinBox_quenchTemp->value()));
    params.set(api::parameters::temperature::QUENCH_RATE, api::value_t(ui->doubleSpinBox_quenchRate->value()));
    params.set(api::parameters::temperature::TARGET_TEMPERATURE, api::value_t(ui->doubleSpinBox_temperature->value()));
    params.set(api::parameters::temperature::DAMPING, api::value_t(ui->doubleSpinBox_damping->value()));

    return params;
}


void TemperatureControlWidget::showNone() {
    ui->groupBox_controls->setVisible(false);
    ui->groupBox_quench->setVisible(false);
    ui->groupBox_borderRegions->setVisible(false);
}


void TemperatureControlWidget::showLinear() {
    ui->groupBox_controls->setVisible(true);
    ui->groupBox_quench->setVisible(true);
    ui->groupBox_borderRegions->setVisible(false);
}


void TemperatureControlWidget::showBorder() {
    ui->groupBox_controls->setVisible(true);
    ui->groupBox_quench->setVisible(true);
    ui->groupBox_borderRegions->setVisible(true);
}
