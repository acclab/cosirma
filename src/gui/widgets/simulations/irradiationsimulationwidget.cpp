/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "irradiationsimulationwidget.h"
#include "ui_irradiationsimulationwidget.h"

#include "api/programsettings.h"
#include "api/project.h"
#include "api/simulation/parcas/parcasirradiationcontroller.h"
#include "api/util.h"

#include "gui/widgets/simulations/components/controlwidget.h"
#include "gui/widgets/simulations/components/speedupwidget.h"
#include "gui/widgets/utils/collapsiblegroupbox.h"

#include <QDebug>
#include <QDir>
#include <QSpacerItem>
#include <QTextDocument>


IrradiationSimulationWidget::IrradiationSimulationWidget(QWidget* parent)
        : AbstractSimulationWidget(parent), ui(new Ui::IrradiationSimulationWidget) {
    ui->setupUi(this);

    // Tab General
    QVBoxLayout* l1 = new QVBoxLayout;
    m_generalIrradiationWidget = new GeneralIrradiationWidget();
    l1->addWidget(m_generalIrradiationWidget);
    l1->addItem(new QSpacerItem(40, 10000, QSizePolicy::Expanding, QSizePolicy::Maximum));
    ui->contentGeneralPage->setLayout(l1);

    // Tab Cascade
    QVBoxLayout* l2 = new QVBoxLayout;

    CollapsibleGroupBox* beamGB = new CollapsibleGroupBox;
    beamGB->setTitle("Beam");
    beamGB->setLayout(new QVBoxLayout);
    m_beamWidget = new BeamWidget();
    beamGB->layout()->addWidget(m_beamWidget);
    l2->addWidget(beamGB);

    CollapsibleGroupBox* ionGB = new CollapsibleGroupBox;
    ionGB->setTitle("Ion");
    ionGB->setLayout(new QVBoxLayout);
    m_ionWidget = new IonWidget();
    ionGB->layout()->addWidget(m_ionWidget);
    l2->addWidget(ionGB);

    m_cascadeControlWidget = new ControlWidget();
    l2->addWidget(m_cascadeControlWidget);
    l2->addItem(new QSpacerItem(40, 10000, QSizePolicy::Expanding, QSizePolicy::Maximum));
    ui->contentCascadePage->setLayout(l2);

    // Tab Relaxation
    QVBoxLayout* l3 = new QVBoxLayout;
    m_relaxationControlWidget = new ControlWidget();
    l3->addWidget(m_relaxationControlWidget);
    l3->addItem(new QSpacerItem(40, 10000, QSizePolicy::Expanding, QSizePolicy::Maximum));
    ui->contentRelaxationPage->setLayout(l3);

    connect(m_generalIrradiationWidget, &GeneralIrradiationWidget::relaxationEnabled, this, [=](bool enabled) {
        ui->tabWidget->setTabEnabled(ui->tabWidget->indexOf(ui->relxationTab), enabled);
        ui->tabWidget->setTabToolTip(ui->tabWidget->indexOf(ui->relxationTab),
                                     enabled ? "" : "Enable the relaxation under the General Tab");
    });

    // Tab Interactions
    QVBoxLayout* l4 = new QVBoxLayout;
    m_interactionWidget = new InteractionWidget();
    l4->addWidget(m_interactionWidget);
    ui->contentInteractionPage->setLayout(l4);

    // Tab Speedup
    QVBoxLayout* speedupLayout = new QVBoxLayout();
    m_speedupWidget = new SpeedupWidget();
    speedupLayout->addWidget(m_speedupWidget);
    speedupLayout->addItem(new QSpacerItem(40, 1000, QSizePolicy::Expanding, QSizePolicy::Maximum));
    ui->tab_speedupContent->setLayout(speedupLayout);

    connect(ui->tabWidget, &QTabWidget::currentChanged, this, &IrradiationSimulationWidget::onChangeTab);


    util::decorateSplitterHandle(ui->splitter, 1);


    connect(m_generalIrradiationWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified general irradiation widget";
        updateCanvas();
        setModified(true);
        setTabStatusIcon(ui->generalTab, loadIcon(gui::enums::state::Modified));
    });
    connect(m_beamWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified beam widget";
        updateCanvas();
        setModified(true);
        setTabStatusIcon(ui->cascadeTab, loadIcon(gui::enums::state::Modified));
    });
    connect(m_ionWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified ion widget";
        updateCanvas();
        setModified(true);
        setTabStatusIcon(ui->cascadeTab, loadIcon(gui::enums::state::Modified));
    });
    connect(m_cascadeControlWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified cascade control widget";
        updateCanvas();
        setModified(true);
        setTabStatusIcon(ui->cascadeTab, loadIcon(gui::enums::state::Modified));
    });
    connect(m_relaxationControlWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified relaxation control widget";
        updateCanvas();
        setModified(true);
        setTabStatusIcon(ui->relxationTab, loadIcon(gui::enums::state::Modified));
    });

    connect(m_interactionWidget, &AbstractComponentWidget::modified, this, [=](bool errors) {
        qDebug() << "Modified interaction widget";
        setModified(true);
        setErrors(errors);
        if (errors) {
            setTabStatusIcon(ui->interactionTab, loadIcon(gui::enums::state::Status::Error));
        } else {
            setTabStatusIcon(ui->interactionTab, loadIcon(gui::enums::state::Status::Modified));
        }
    });
    connect(m_interactionWidget, &AbstractComponentWidget::needsAttention, this, [=](bool errors) {
        setErrors(errors);
        if (errors) {
            setTabStatusIcon(ui->interactionTab, loadIcon(gui::enums::state::Status::Error));
        } else {
            setTabStatusIcon(ui->interactionTab, loadIcon(gui::enums::state::Status::Attention));
        }
    });

    connect(m_speedupWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified speedup widget";
        updateCanvas();
        setModified(true);
        setTabStatusIcon(ui->tab_speedup, loadIcon(gui::enums::state::Modified));
    });
}


IrradiationSimulationWidget::~IrradiationSimulationWidget() {
    delete m_generalIrradiationWidget;
    delete m_beamWidget;
    delete m_ionWidget;
    delete m_cascadeControlWidget;
    delete m_relaxationControlWidget;
    delete m_interactionWidget;
    delete m_speedupWidget;

    delete ui;
}


void IrradiationSimulationWidget::connectToPlot(PlotCellGLWidget* glWidget) {
    m_relaxationControlWidget->connectToPlot(glWidget);
    m_cascadeControlWidget->connectToPlot(glWidget);

    connect(this, &IrradiationSimulationWidget::changePlotMode, glWidget, &PlotCellGLWidget::setPlotMode);
    connect(m_beamWidget, &BeamWidget::beamUpdated, this, [=] {

        api::BaseParameters parameters = m_beamWidget->parameters();

        BeamData beamData;
        beamData.type = static_cast<api::enums::beam::Type>(parameters[api::parameters::beam::TYPE].value().toInt());
        beamData.face = static_cast<api::enums::beam::Face>(parameters[api::parameters::beam::FACE].value().toInt());
        beamData.shape = static_cast<api::enums::beam::Shape>(parameters[api::parameters::beam::SHAPE].value().toInt());
        beamData.width = parameters[api::parameters::beam::WIDTH].value().toDouble();
        beamData.position = glm::dvec3(parameters[api::parameters::ion_transform::X_POSITION].value().toDouble(),
                                       parameters[api::parameters::ion_transform::Y_POSITION].value().toDouble(),
                                       parameters[api::parameters::ion_transform::Z_POSITION].value().toDouble());
        beamData.phi = parameters[api::parameters::ion_transform::PHI].value().toDouble();
        beamData.theta = parameters[api::parameters::ion_transform::THETA].value().toDouble();
        beamData.inclination = parameters[api::parameters::beam::INCLINATION].value().toDouble();
        beamData.azimuth = parameters[api::parameters::beam::AZIMUTH].value().toDouble();

        glWidget->setBeam(beamData, entry().frame.cellLims);
    });

    updateCanvas();

    onChangeTab(0);
}


void IrradiationSimulationWidget::disconnectFromPlot(PlotCellGLWidget* glWidget) {
    m_relaxationControlWidget->disconnectFromPlot(glWidget);
    m_cascadeControlWidget->disconnectFromPlot(glWidget);

    disconnect(this, &IrradiationSimulationWidget::changePlotMode, glWidget, &PlotCellGLWidget::setPlotMode);

    glWidget->setPlotMode(PlotCellGLWidget::Cooling);
}


void IrradiationSimulationWidget::setControllerParametersToWidget(ParcasBaseController* controller) {
    // Remove the unsaved indicator from the tabs
    setModified(false);
    setErrors(false);
    setNeedsAttention(false);

    setTabStatusIcon(ui->generalTab, loadIcon(gui::enums::state::Ok));
    setTabStatusIcon(ui->cascadeTab, loadIcon(gui::enums::state::Ok));
    setTabStatusIcon(ui->relxationTab, loadIcon(gui::enums::state::Ok));
    setTabStatusIcon(ui->interactionTab, loadIcon(gui::enums::state::Ok));
    setTabStatusIcon(ui->tab_speedup, loadIcon(gui::enums::state::Ok));


    ParcasIrradiationController* irradiationController = static_cast<ParcasIrradiationController*>(controller);

    // Cascade related parameters
    m_unmodifiedCascadeParameters = irradiationController->cascadeParameters();
    m_cascadeControlWidget->setParameters(irradiationController->cascadeParameters());

    // Relaxation related parameters
    m_unmodifiedRelaxParameters = irradiationController->relaxParameters();
    m_relaxationControlWidget->setParameters(irradiationController->relaxParameters());

    // Common parameters
    m_unmodifiedParameters = irradiationController->parameters();
    m_generalIrradiationWidget->setParameters(irradiationController->parameters());
    m_ionWidget->setParameters(irradiationController->parameters());
    m_beamWidget->setParameters(irradiationController->parameters());
    m_interactionWidget->setParameters(irradiationController->parameters());
    m_speedupWidget->setParameters(irradiationController->parameters());
}


void IrradiationSimulationWidget::setWidgetParametersToController(ParcasBaseController* controller) {

    ParcasIrradiationController* irradiationController = static_cast<ParcasIrradiationController*>(controller);

    // Cascade related parameters
    irradiationController->insertCascadeParameters(m_cascadeControlWidget->parameters());

    // Relaxation related parameters
    irradiationController->insertRelaxParameters(m_relaxationControlWidget->parameters());

    // Common parameters
    irradiationController->insertParameters(m_generalIrradiationWidget->parameters());
    irradiationController->insertParameters(m_beamWidget->parameters());
    irradiationController->insertParameters(m_ionWidget->parameters());
    irradiationController->insertParameters(m_interactionWidget->parameters());
    irradiationController->insertParameters(m_speedupWidget->parameters());
}


// bool IrradiationSimulationWidget::isModified() {
//    api::BaseParameters tempParams;
//    tempParams.insert(m_cascadeControlWidget->parameters());
//    for (const api::key_t& key : tempParams.values().keys()) {
//        if (tempParams[key].value() != m_unmodifiedCascadeParameters[key].value()) {
//            return true;
//        }
//    }

//    tempParams.clear();
//    tempParams.insert(m_relaxationControlWidget->parameters());
//    for (const api::key_t& key : tempParams.values().keys()) {
//        if (tempParams[key].value() != m_unmodifiedRelaxParameters[key].value()) {
//            return true;
//        }
//    }

//    tempParams.clear();
//    tempParams.insert(m_generalIrradiationWidget->parameters());
//    tempParams.insert(m_beamWidget->parameters());
//    tempParams.insert(m_ionWidget->parameters());
//    tempParams.insert(m_interactionWidget->parameters());
//    tempParams.insert(m_speedupWidget->parameters());
//    for (const api::key_t& key : tempParams.values().keys()) {
//        if (tempParams[key].value() != m_unmodifiedParameters[key].value()) {
//            return true;
//        }
//    }

//    return false;
//}


void IrradiationSimulationWidget::setComponentEntry(ExtendedEntryData* componentEntry) {
    m_interactionWidget->setEntry(componentEntry);
    m_cascadeControlWidget->setEntry(componentEntry);
    m_relaxationControlWidget->setEntry(componentEntry);
    m_speedupWidget->setEntry(componentEntry);
    m_beamWidget->setEntry(componentEntry);
    m_ionWidget->setEntry(componentEntry);
}


void IrradiationSimulationWidget::enableSpeedupModule(bool enabled) {
    ui->tabWidget->setTabEnabled(SpeedupTab, enabled);
    ui->tabWidget->setTabToolTip(SpeedupTab,
                                 enabled ? "" : "Enable the speedup from the backend settings \"edit/settings/\"");
}


void IrradiationSimulationWidget::setTabStatusIcon(QWidget* tabWidget, const QIcon& icon) {
    ui->tabWidget->setTabIcon(ui->tabWidget->indexOf(tabWidget), icon);
}


void IrradiationSimulationWidget::onChangeTab(int /*index*/) {
    QWidget* current = ui->tabWidget->currentWidget();

    ui->graphicsView->setVisible(current != ui->interactionTab);

    emit changePlotMode((current == ui->cascadeTab) ? PlotCellGLWidget::Beam : PlotCellGLWidget::Cooling);

    m_relaxationControlWidget->showControlRegion(current == ui->relxationTab);
    m_cascadeControlWidget->showControlRegion(current == ui->cascadeTab);
}


void IrradiationSimulationWidget::updateCanvas() {
    api::BaseParameters parameters = m_generalIrradiationWidget->parameters();
    api::BaseParameters relaxParameters = m_relaxationControlWidget->parameters();
    api::BaseParameters cascadeParameters = m_cascadeControlWidget->parameters();

    bool startWithRelax = parameters[api::parameters::irradiation::START_WITH_RELAX_ON].value().toBool();
    float relaxationTime = relaxParameters[api::parameters::time::TIME].value().toFloat();
    int relaxationInterval = parameters[api::parameters::irradiation::RELAX_INTERVAL].value().toInt();
    float cascadeTime = cascadeParameters[api::parameters::time::TIME].value().toFloat();
    bool useRelax = parameters[api::parameters::irradiation::RELAX_ON].value().toBool();

    ui->graphicsView->setStartWithRelax(startWithRelax);
    ui->graphicsView->setRelaxTime(relaxationTime);
    ui->graphicsView->setRepeatCounter(relaxationInterval);
    ui->graphicsView->setCascadeTime(cascadeTime);
    ui->graphicsView->setUseRelax(useRelax);
    ui->graphicsView->draw();
}
