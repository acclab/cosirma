/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef RELAXATIONSIMULATIONWIDGET_H
#define RELAXATIONSIMULATIONWIDGET_H

#include "gui/widgets/simulationlistwidget.h"
#include "gui/widgets/simulations/abstractsimulationwidget.h"
#include "gui/widgets/simulations/components/controlwidget.h"
#include "gui/widgets/simulations/components/interactionwidget.h"
#include "gui/widgets/simulations/components/timewidget.h"

#include <QWidget>


namespace Ui {
class RelaxationSimulationWidget;
}

class RelaxationSimulationWidget : public AbstractSimulationWidget {
    Q_OBJECT

  public:
    explicit RelaxationSimulationWidget(QWidget* parent = nullptr);
    ~RelaxationSimulationWidget();

    void connectToPlot(PlotCellGLWidget* glWidget) override;
    void disconnectFromPlot(PlotCellGLWidget* glWidget) override;

    void setControllerParametersToWidget(ParcasBaseController* controller) override;
    void setWidgetParametersToController(ParcasBaseController* controller) override;

  private:
    void setComponentEntry(ExtendedEntryData* componentEntry) override;

    Ui::RelaxationSimulationWidget* ui;

    ControlWidget* m_controlWidget;
    InteractionWidget* m_interactionWidget;

    api::BaseParameters m_unmodifiedParameters;

  private slots:
    void setTabStatusIcon(QWidget* tabWidget, const QIcon& icon);

  signals:
    void plottablesChanged();
};

#endif  // RELAXATIONSIMULATIONWIDGET_H
