/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ABSTRACTSIMULATIONWIDGET_H
#define ABSTRACTSIMULATIONWIDGET_H

#include "api/simulation/parcas/parcasbasecontroller.h"

#include "gui/state/enums.h"
#include "gui/widgets/plotcellglwidget.h"

#include <QWidget>


class QIcon;


class AbstractSimulationWidget : public QWidget {
    Q_OBJECT

  public:
    explicit AbstractSimulationWidget(QWidget* parent = nullptr) : QWidget(parent) {}
    ~AbstractSimulationWidget() {}

    /**
     * @brief Creates a mutable local copy of the selected entry. This can be modified by the widgets and accessed
     * through the \sa entry() funtion.
     *
     * @param entry
     */
    void setEntry(const ExtendedEntryData* entry);
    ExtendedEntryData entry() { return *m_componentEntry; }

    virtual void setControllerParametersToWidget(ParcasBaseController* controller) = 0;
    virtual void setWidgetParametersToController(ParcasBaseController* controller) = 0;

    virtual void connectToPlot(PlotCellGLWidget*) {}
    virtual void disconnectFromPlot(PlotCellGLWidget*) {}

    void setModified(bool modified);
    bool isModified();

    void setErrors(bool errors);
    bool hasErrors();

    void setNeedsAttention(bool attention);
    bool needsAttention();

  protected:
    QIcon loadIcon(gui::enums::state::Status status);

  private:
    /**
     * @brief This function lets the extended class use the entry data shared among all the widgets.
     *
     * This function should not be called explicitly in the extended class!
     *
     * @param componentEntry
     */
    virtual void setComponentEntry(ExtendedEntryData* componentEntry) = 0;

    ExtendedEntryData* m_componentEntry = nullptr;

    bool m_modified = false;
    bool m_errors = false;
    bool m_attention = false;

  public slots:
    virtual void enableSpeedupModule(bool /*enabled*/) {}

  signals:
    void changePlotMode(PlotCellGLWidget::PlotMode mode);
};

#endif  // ABSTRACTSIMULATIONWIDGET_H
