/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "typeconflictsolverwidget.h"
#include "ui_typeconflictsolverwidget.h"

#include "api/elementreader.h"
#include "api/programsettings.h"
#include "gui/dialogs/chooseelementdialog.h"

#include <QMessageBox>

#include <cmath>

TypeConflictSolverWidget::TypeConflictSolverWidget(QWidget* parent)
        : QWidget(parent), ui(new Ui::TypeConflictSolverWidget) {
    ui->setupUi(this);

    connect(&ProgramSettings::getInstance().visualSettings(), &VisualSettings::atomTypeColorsChanged, this,
            [this] { updateColors(ProgramSettings::getInstance().visualSettings().atomTypeColors()); });

    connect(ui->resolveAllPB, &QPushButton::clicked, this, &TypeConflictSolverWidget::resolveAllProblems);

    setFocusProxy(ui->groupBox);
    ui->resolveAllPB->setAutoDefault(false);
}

TypeConflictSolverWidget::~TypeConflictSolverWidget() {
    delete ui;
}

void TypeConflictSolverWidget::changeEntry(ExtendedEntryData* newEntry) {
    entry = newEntry;
    writeTypeTable();
}

bool TypeConflictSolverWidget::isResolved() {
    return entry && conflictingTypes.isEmpty();
}

void TypeConflictSolverWidget::writeTypeTable() {

    if (!entry) return;

    for (QWidget* w : tableWidgets) w->deleteLater();
    tableWidgets.clear();
    typeRows.clear();
    conflictingTypes.clear();

    QStringList availableTypes;
    for (int i = -9; i < 10; ++i)
        if (i != 0) availableTypes << QString::number(i);

    QVector<int> typeNumbers = entry->frame.amounts.keys().toVector();
    std::sort(typeNumbers.begin(), typeNumbers.end(), [](const int& i1, const int& i2) -> bool {
        if (abs(i1) == abs(i2)) return i1 > 0;
        return abs(i1) < abs(i2);
    });

    QIcon errorIcon;
    errorIcon.addFile(":/icons/exclamation-triangle-orange", QSize(15, 15));

    int n = 0;
    bool first = true;
    QWidget* last = nullptr;
    for (int i : typeNumbers) {

        const AtomType& type = entry->data.types.typeList[i];
        TypeRow row;

        row.typeCB = new QComboBox(this);
        if (i < -9 || i > 9) row.typeCB->addItem(QString::number(i));
        row.typeCB->addItems(availableTypes);
        row.typeCB->setCurrentText(QString::number(i));
        row.typeCB->setToolTip("Type number");
        if (first) {
            ui->groupBox->setFocusProxy(row.typeCB);
            first = false;
        }

        row.colorLabel = new QLabel(this);
        row.colorLabel->setMinimumWidth(15);

        row.symbolLE = new QLineEdit(this);
        row.symbolLE->setMaximumWidth(30);
        row.symbolLE->setText(type.symbol);
        row.symbolLE->setToolTip("Element symbol");

        row.ptPB = new QPushButton(this);
        row.ptPB->setAutoDefault(false);
        row.ptPB->setMaximumWidth(20);
        row.ptPB->setText("+");
        row.ptPB->setToolTip("Open periodic table.");
        row.ptPB->setAutoDefault(false);

        row.massSB = new QDoubleSpinBox(this);
        row.massSB->setDecimals(6);
        row.massSB->setRange(0.0, 9999.999999);
        row.massSB->setSuffix(" u");
        row.massSB->setValue(type.mass);
        if (row.massSB->value() > 0) row.massSB->setMinimum(0.000001);
        row.massSB->setToolTip("Type mass");

        row.iconLabel = new QLabel(this);
        row.iconLabel->setPixmap(errorIcon.pixmap(QSize(15, 15)));
        row.iconLabel->setVisible(false);

        row.resolvePB = new QPushButton("Resolve", this);
        row.resolvePB->setVisible(false);
        row.resolvePB->setToolTip("Resolve conflict");
        row.resolvePB->setAutoDefault(false);

        ui->typeGrid->addWidget(row.typeCB, n, 0);
        ui->typeGrid->addWidget(row.colorLabel, n, 1);
        ui->typeGrid->addWidget(row.symbolLE, n, 2);
        ui->typeGrid->addWidget(row.ptPB, n, 3);
        ui->typeGrid->addWidget(row.massSB, n, 4);
        ui->typeGrid->addWidget(row.iconLabel, n, 5);
        ui->typeGrid->addWidget(row.resolvePB, n, 6);

        tableWidgets << row.typeCB << row.colorLabel << row.symbolLE << row.ptPB << row.massSB << row.iconLabel
                     << row.resolvePB;
        typeRows[i] = row;

        connectRow(row, true);
        last = row.resolvePB;
        ++n;
    }

    updateStatuses();
    updateColors(ProgramSettings::getInstance().visualSettings().atomTypeColors());
    setTabOrder(last, ui->resolveAllPB);
    emit tableWritten(ui->resolveAllPB);
}

bool TypeConflictSolverWidget::attemptToChangeType(int fromType, int toType, bool ask) {

    TypeRow row = typeRows[fromType];
    QComboBox* cb = row.typeCB;

    if (!cb || !entry) return false;

    connectRow(row, false);

    bool proceed = true;
    bool merge = entry->data.types.typeList.contains(toType);


    if (ask && merge) {
        QMessageBox::StandardButton reply;
        QString message1 = QString("Merge to type %1?").arg(toType);
        QString message2 = QString("Do you want to merge the atoms with type %1 to type %2?").arg(fromType).arg(toType);
        reply = QMessageBox::question(this, message1, message2, QMessageBox::Yes | QMessageBox::No);
        proceed = (reply == QMessageBox::Yes);
    }

    if (!proceed) {
        cb->setCurrentText(QString::number(fromType));
        connectRow(row, true);
        return false;
    }

    int atomsFound = 0;
    for (Atom& a : entry->frame.atoms) {
        if (a.type == fromType) {
            a.type = toType;
            atomsFound++;
        }
    }

    entry->frame.amounts[toType] += atomsFound;
    entry->frame.amounts.remove(fromType);
    conflictingTypes.remove(fromType);

    if (!merge) {
        entry->data.types.typeList[toType] = entry->data.types.typeList[fromType];
        entry->data.types.typeList[toType].typeNumber = toType;
        typeRows[toType] = typeRows[fromType];

        entry->data.types.typeList.remove(fromType);
        typeRows.remove(fromType);


        if (cb) {
            cb->disconnect();
            if (fromType < -9 || fromType > 9) {
                cb->removeItem(cb->findText(QString::number(fromType)));
            }
        }

    } else {
        entry->data.types.typeList.remove(fromType);
        writeTypeTable();
        emit entryChanged();
        return true;
    }

    cb->setCurrentText(QString::number(toType));
    updateStatus(toType);
    updateStatus(-toType);
    updateStatus(-fromType);
    updateColors(ProgramSettings::getInstance().visualSettings().atomTypeColors());
    emit entryChanged();

    connectRow(row, true);

    return true;
}

void TypeConflictSolverWidget::showPeriodicTable(int type) {
    ChooseElementDialog pb(this);
    if (pb.exec()) {
        ElementReader reader;
        updateSymbol(type, reader.getSymbol(pb.Z()));
    }
}

void TypeConflictSolverWidget::updateSymbol(int type, QString symbol, bool ask) {
    TypeRow row = typeRows[type];
    QLineEdit* le = row.symbolLE;

    if (!le || !entry) return;

    connectRow(row, false);

    entry->data.types.typeList[type].symbol = symbol;
    ElementReader reader;
    int z = reader.zFromSymbol(symbol);
    if (z > 0) {
        entry->data.types.typeList[type].symbol = reader.getSymbol(z);
        attemptToReplaceMass(type, reader.getMass(z), ask);
    }
    if (typeRows[type].symbolLE && (entry->data.types.typeList[type].symbol != (typeRows[type].symbolLE->text())))
        typeRows[type].symbolLE->setText(entry->data.types.typeList[type].symbol);

    updateStatus(type);
    updateStatus(-type);

    QString s = entry->data.types.typeList[type].symbol;
    for (Atom& a : entry->frame.atoms) {
        if (a.type == type) a.symbol = s;
    }

    emit entryChanged();

    connectRow(row, true);
}

void TypeConflictSolverWidget::updateMass(int type, double mass) {
    if (!entry) return;
    if (typeRows.contains(type) && typeRows[type].massSB) typeRows[type].massSB->setValue(mass);

    if (typeRows[type].massSB->value() > 0) typeRows[type].massSB->setMinimum(0.000001);

    if (entry->data.types.typeList.contains(type)) entry->data.types.typeList[type].mass = mass;

    updateStatus(type);
    updateStatus(-type);
}

void TypeConflictSolverWidget::attemptToReplaceMass(int type, double newMass, bool ask) {
    if (!entry) return;

    double oldMass = entry->data.types.typeList[type].mass;
    if (std::abs(oldMass - newMass) < 1e-6) return;

    bool proceed = true;
    if (ask) {
        QMessageBox::StandardButton reply;
        QString message1 = QString("Replace mass?");
        QString message2 = QString("Do you want to replace the current mass of type %1 with the default mass of %2.")
                               .arg(type)
                               .arg(entry->data.types.typeList[type].symbol);
        reply = QMessageBox::question(this, message1, message2, QMessageBox::Yes | QMessageBox::No);
        proceed = (reply == QMessageBox::Yes);
    }

    if (proceed) {
        entry->data.types.typeList[type].mass = newMass;
        updateMass(type, newMass);
    }
}

QList<TypeConflictSolverWidget::Problem> TypeConflictSolverWidget::problems(int type) {
    QList<Problem> ret;
    if (!entry || !entry->data.types.typeList.contains(type)) return ret;

    if (type < -9 || type > 9 || type == 0) ret << IllegalType;

    const AtomType& typeStruct = entry->data.types.typeList.value(type);

    if (typeStruct.mass <= 0) ret << IllegalMass;

    if (typeStruct.symbol.isEmpty() || typeStruct.symbol.length() > 2) ret << IllegalSymbol;

    bool hasPositive = (type < 0 && entry->frame.amounts.contains(-type));
    if (hasPositive) {
        const AtomType& positiveType = entry->data.types.typeList.value(-type);
        if (typeStruct.symbol != positiveType.symbol) ret << SymbolConflict;
        if (std::abs(typeStruct.mass - positiveType.mass) > 1e-6) ret << MassConflict;
    }

    return ret;
}

bool TypeConflictSolverWidget::updateStatus(int type) {

    if (!entry || !entry->data.types.typeList.contains(type)) {
        conflictingTypes.remove(type);
        checkIfResolved();
        return true;
    }

    QList<Problem> probs = problems(type);
    QString errorMessage;

    for (Problem c : probs) {
        switch (c) {
            case IllegalType:
                errorMessage += "Type number is not in range [-9,-1] or [1,9]\n";
                break;

            case IllegalSymbol:
                errorMessage += "Element symbol does not have 1-2 characters\n";
                break;

            case IllegalMass:
                errorMessage += "Mass is non-positive\n";
                break;

            case SymbolConflict:
                errorMessage += QString("Element symbol differs from type %1\n").arg(-type);
                break;

            case MassConflict:
                errorMessage += QString("Mass differs from type %1\n").arg(-type);
                break;

            default:
                break;
        }
    }

    if (typeRows.contains(type)) {
        QLabel* l = typeRows[type].iconLabel;
        QPushButton* pb = typeRows[type].resolvePB;
        if (l) {
            l->setVisible(!errorMessage.isEmpty());
            l->setToolTip(errorMessage.trimmed());
        }
        if (pb) pb->setVisible(!errorMessage.isEmpty());
    }

    if (errorMessage.isEmpty()) {
        conflictingTypes.remove(type);
    } else {
        conflictingTypes.insert(type);
    }
    checkIfResolved();
    return errorMessage.isEmpty();
}

void TypeConflictSolverWidget::updateStatuses() {
    for (int type : typeRows.keys()) updateStatus(type);
}

void TypeConflictSolverWidget::resolveProblems(int type) {
    if (!entry) return;

    QList<Problem> probs = problems(type);

    ResolveAction action = findAction(type, entry->data.types);

    bool proceed = false;
    QString messageTitle = QString("Resolving type %1?").arg(type);
    if (action.action != ResolveAction::DoNothing) {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, messageTitle, action.message, QMessageBox::Yes | QMessageBox::No);
        proceed = reply == QMessageBox::Yes;
    } else {
        QMessageBox::critical(this, messageTitle, action.message, QMessageBox::Ok);
    }

    if (!proceed) return;

    switch (action.action) {
        case ResolveAction::ChangeType:
            attemptToChangeType(type, action.targetType, false);
            break;

        case ResolveAction::CopyProperties:
            updateSymbol(type, entry->data.types.typeList[action.targetType].symbol, false);
            updateMass(type, entry->data.types.typeList[action.targetType].mass);
            break;

        case ResolveAction::ChangeSymbol:
            updateSymbol(type, action.newSymbol, entry->data.types.typeList[type].mass <= 0);
            break;

        case ResolveAction::ChangeMass:
            updateMass(type, action.newMass);
            break;

        default:
            break;
    }
}

void TypeConflictSolverWidget::resolveAllProblems() {

    if (!entry) return;

    TypeData tempTypes = entry->data.types;

    QString message;
    QVector<ResolveAction> actions;

    for (int type : conflictingTypes) {
        ResolveAction action = findAction(type, tempTypes);
        switch (action.action) {
            case ResolveAction::ChangeType:
                tempTypes.typeList[action.targetType] = tempTypes.typeList[type];
                tempTypes.typeList.remove(type);
                message += action.message + "\n";
                actions.push_back(action);
                break;

            case ResolveAction::CopyProperties:
                tempTypes.typeList[type].symbol = tempTypes.typeList[action.targetType].symbol;
                tempTypes.typeList[type].mass = tempTypes.typeList[action.targetType].mass;
                message += action.message + "\n";
                actions.push_back(action);
                break;

            case ResolveAction::ChangeSymbol:
                tempTypes.typeList[type].symbol = action.newSymbol;
                message += action.message + "\n";
                actions.push_back(action);
                break;

            case ResolveAction::ChangeMass:
                tempTypes.typeList[type].mass = action.newMass;
                message += action.message + "\n";
                actions.push_back(action);
                break;

            default:
                break;
        }
    }

    bool proceed = false;

    if (actions.count() != 0) {
        QMessageBox::StandardButton reply;
        reply =
            QMessageBox::question(this, "Resolve conflicts?", message.trimmed(), QMessageBox::Yes | QMessageBox::No);
        proceed = reply == QMessageBox::Yes;
    } else {
        QMessageBox::critical(this, "Resolve conflicts.", "No resolving actions found!", QMessageBox::Ok);
    }

    if (!proceed) return;

    for (const ResolveAction& action : actions) {
        switch (action.action) {
            case ResolveAction::ChangeType:
                attemptToChangeType(action.type, action.targetType, false);
                break;

            case ResolveAction::CopyProperties:
                updateSymbol(action.type, entry->data.types.typeList[action.targetType].symbol, false);
                updateMass(action.type, entry->data.types.typeList[action.targetType].mass);
                break;

            case ResolveAction::ChangeSymbol:
                updateSymbol(action.type, action.newSymbol, entry->data.types.typeList[action.type].mass <= 0);
                break;

            case ResolveAction::ChangeMass:
                updateMass(action.type, action.newMass);
                break;

            default:
                break;
        }
    }
}

TypeConflictSolverWidget::ResolveAction TypeConflictSolverWidget::findAction(int type, const TypeData& types) {
    ResolveAction action;
    action.type = type;
    bool canResolve = false;

    QList<Problem> probs = problems(type);

    if (probs.contains(IllegalType)) {
        // Finding the positive or negative type that is closest to zero and not used
        int direction = (type >= 0) * 2 - 1;  // sign(type)
        action.targetType = direction;
        while (types.typeList.contains(action.targetType) || types.typeList.contains(-action.targetType))
            action.targetType += direction;

        if (action.targetType < -9 || action.targetType > 9) {
            action.targetType = direction;
            while (types.typeList.contains(action.targetType)) action.targetType += direction;
        }

        if (action.targetType < -9 || action.targetType > 9) {
            action.targetType = -direction;
            while (types.typeList.contains(action.targetType)) action.targetType += -direction;
        }

        if (action.targetType < -9 || action.targetType > 9) {
            action.message = "No available type numbers in range [-9,9]";
            canResolve = false;
            action.action = ResolveAction::DoNothing;
        } else {
            action.message = QString("Changing type %1 to %2").arg(type).arg(action.targetType);
            canResolve = true;
            action.action = ResolveAction::ChangeType;
        }
    } else if (probs.contains(SymbolConflict) || probs.contains(MassConflict)) {
        if (types.typeList.contains(-type)) {
            action.message = QString("Copying the parameters for type %1 from type %2").arg(type).arg(-type);
            action.targetType = -type;
            action.action = ResolveAction::CopyProperties;
            canResolve = true;
        } else {
            canResolve = false;
        }
    } else if (probs.contains(IllegalSymbol)) {
        if (types.typeList.value(type).symbol.isEmpty()) {
            action.newSymbol = QString::number(std::abs(type));
        } else if (types.typeList.value(type).symbol.length() > 2) {
            action.newSymbol = types.typeList.value(type).symbol.left(2);
        }
        action.message = QString("Changing the element symbol of type %1 to %2").arg(type).arg(action.newSymbol);
        action.action = ResolveAction::ChangeSymbol;
        canResolve = true;
    } else if (probs.contains(IllegalMass)) {
        ElementReader reader;
        int z = reader.zFromSymbol(types.typeList.value(type).symbol);
        if (z > 0) {
            action.newMass = reader.getMass(z);
        }
        action.message =
            QString("Changing the mass of type %1 to %2 u").arg(type).arg(QString::number(action.newMass, 'f', 6));
        action.action = ResolveAction::ChangeMass;
        canResolve = true;
    }

    if (!canResolve && action.message.isEmpty()) action.message = QString("Can not resolve type %1").arg(type);

    return action;
}

void TypeConflictSolverWidget::updateColors(const QHash<int, QColor>& colors) {
    for (int type : typeRows.keys()) {
        TypeRow row = typeRows[type];
        QColor c = colors[type];
        row.colorLabel->setStyleSheet(QString("background-color: rgb(%1,%2,%3);border: 1px solid black;")
                                          .arg(c.red())
                                          .arg(c.green())
                                          .arg(c.blue()));
    }
}

void TypeConflictSolverWidget::connectRow(TypeConflictSolverWidget::TypeRow row, bool c) {
    if (!c) {
        if (row.typeCB) row.typeCB->disconnect();
        if (row.symbolLE) row.symbolLE->disconnect();
        if (row.ptPB) row.ptPB->disconnect();
        if (row.massSB) row.massSB->disconnect();
        if (row.resolvePB) row.resolvePB->disconnect();
        return;
    } else {
        QComboBox* cb = row.typeCB;
        QLineEdit* le = row.symbolLE;
        QPushButton* pb = row.ptPB;
        QDoubleSpinBox* sb = row.massSB;
        QPushButton* res = row.resolvePB;

        if (!cb || !le || !pb || !sb || !res) return;

        int type = cb->currentText().toInt();

        connect(cb, &QComboBox::currentTextChanged, [this, type](QString s) { attemptToChangeType(type, s.toInt()); });

        connect(le, &QLineEdit::textEdited, [this, type, le](QString s) {
            le->setText(s.left(2));
            le->disconnect();
            connect(le, &QLineEdit::textEdited, [le](QString s) { le->setText(s.left(2)); });
            connect(le, &QLineEdit::editingFinished, [this, type, le] { updateSymbol(type, le->text()); });
        });

        connect(pb, &QPushButton::clicked, [this, type] { showPeriodicTable(type); });

        connect(sb, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
                [this, type](double d) { updateMass(type, d); });

        connect(res, &QPushButton::clicked, [this, type] { resolveProblems(type); });

        return;
    }
}

void TypeConflictSolverWidget::checkIfResolved() {
    ui->resolveAllPB->setVisible(conflictingTypes.size() > 0);
    emit conflictsResolved(entry && conflictingTypes.isEmpty());
}
