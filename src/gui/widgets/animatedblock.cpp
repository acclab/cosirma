/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "animatedblock.h"

#include <QDebug>
#include <QLayout>

#include <cmath>
#include <limits>


AnimatedBlock::AnimatedBlock(QWidget* parent) : QWidget(parent) {

    QVBoxLayout* l = new QVBoxLayout;
    m_plotWidget = new PlotCellGLWidget(this);
    l->addWidget(m_plotWidget);
    setLayout(l);

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &AnimatedBlock::rotateCamera);
    connect(m_plotWidget, &PlotCellGLWidget::mouseDragged, timer, &QTimer::stop);

    m_plotWidget->usePerspective(true);
}

/*!
 * Draws a 3D plot of atoms using PlotCellGLWidget. Starts a timer that will rotate the plot.
 *
 * \sa PlotCellGLWidget
 */
void AnimatedBlock::draw(const XyzData& cell, const VisualSettings& visualSettings) {
    m_plotWidget->setBackgroundColor(visualSettings.backgroundColor());
    m_plotWidget->setModelOrientation(270, 0, 0);

    m_plotWidget->setVisualSettings(visualSettings);
    m_plotWidget->setFrame(cell);

    timer->start(30);
}

/*!
 * Rotates the camera around the z-axis using PlotCellGLWidget::rotate().
 */
void AnimatedBlock::rotateCamera() {
    m_plotWidget->rotate(glm::vec3(0, 0, 1), 1);
}
