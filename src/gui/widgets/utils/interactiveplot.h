/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef INTERACTIVEPLOT_H
#define INTERACTIVEPLOT_H

#include "qcustomplot.h"

#include <QPoint>

#include <glm/vec2.hpp>

/*!
 * \brief QCustomPlot with reimplemented interactions.
 *
 * Left mouse button now zooms to a rectangle and the right mouse button drags.
 */
class InteractivePlot : public QCustomPlot {

  public:
    explicit InteractivePlot(QWidget* parent = nullptr);
    ~InteractivePlot();

  protected:
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    glm::vec2 grabbedCoordinate;
    QPoint lastCursorPos;
    QCPItemRect* zoomRect;
};

#endif  // INTERACTIVEPLOT_H
