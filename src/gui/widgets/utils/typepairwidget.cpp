/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "typepairwidget.h"

#include "api/programsettings.h"
#include "clickablelabel.h"

#include <QIcon>
#include <QLayout>

TypePairWidget::TypePairWidget(const AtomType& type1, const AtomType& type2, QWidget* parent) : QWidget(parent) {

    QString text1 = QString::number(type1.typeNumber) + " (" + type1.symbol + ")";
    QString text2 = QString::number(type2.typeNumber) + " (" + type2.symbol + ")";
    QColor c1 = ProgramSettings::getInstance().visualSettings().atomTypeColors()[type1.typeNumber];
    QColor c2 = ProgramSettings::getInstance().visualSettings().atomTypeColors()[type2.typeNumber];


    ClickableLabel* c1Label = new ClickableLabel;
    c1Label->setFixedSize(20, 20);
    c1Label->setColor(c1);

    ClickableLabel* c2Label = new ClickableLabel;
    c2Label->setFixedSize(20, 20);
    c2Label->setColor(c2);

    QLabel* arrowLabel = new QLabel;
    arrowLabel->setFixedSize(16, 16);
    arrowLabel->setPixmap(QIcon(":/icons/right_on_left_arrow.svg").pixmap(arrowLabel->size()));

    QHBoxLayout* l = new QHBoxLayout;
    l->setContentsMargins(0, 0, 0, 0);
    l->addWidget(new QLabel(text1));
    l->addWidget(c1Label);
    l->addWidget(arrowLabel);
    l->addWidget(c2Label);
    l->addWidget(new QLabel(text2));
    setLayout(l);

    connect(&ProgramSettings::getInstance().visualSettings(), &VisualSettings::atomTypeColorsChanged, this, [=] {
        c1Label->setColor(ProgramSettings::getInstance().visualSettings().atomTypeColors()[type1.typeNumber]);
        c2Label->setColor(ProgramSettings::getInstance().visualSettings().atomTypeColors()[type2.typeNumber]);
    });
}
