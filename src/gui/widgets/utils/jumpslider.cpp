/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "jumpslider.h"
#include <QStyleOptionSlider>

#include <QDebug>


/*!
 * Overrided mousePressEvent.
 * When the mouse is pressed on the slider,
 * the value of the slider is changed to the one closest to the mouse event.
 */
void JumpSlider::mousePressEvent(QMouseEvent* event) {
    QStyleOptionSlider opt;
    initStyleOption(&opt);
    QRect sr = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);

    if (event->button() == Qt::LeftButton && sr.contains(event->pos()) == false) {
        int newVal;
        if (orientation() == Qt::Vertical) {
            newVal = minimum() + ((maximum() - minimum()) * (height() - event->y())) / height();
        } else {
            newVal = minimum() + ((float((maximum() - minimum())) * event->x()) / width() + 0.5);
        }

        if (invertedAppearance() == true) {
            setValue(maximum() - newVal);
        } else {
            setValue(newVal);
        }
        event->accept();
        QSlider::mousePressEvent(event);

        if (invertedAppearance() == true) {
            setValue(maximum() - newVal);
        } else {
            setValue(newVal);
        }
        event->accept();

    } else {
        QSlider::mousePressEvent(event);
    }
}
