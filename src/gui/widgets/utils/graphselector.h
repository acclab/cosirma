/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef GRAPHSELECTOR_H
#define GRAPHSELECTOR_H

#include "gui/widgets/ratio.h"

#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QWidget>


// Forward declaration of the class
class Ratio;

// Example:
//  GraphSelector *selector = new GraphSelector(this);
//  ui->QLayout->addWidget(selector);
//  selector->setRes(3);
//  selector->init();
//

/*!
 * \brief QGraphicsView for selecting a bin in the simulation cell.
 *
 * Shows rectangular view of the simulation cell that
 * has been sliced into bins.
 * The selected square of the grid is highlighted and clicking a square
 * makes the clicked square selected.
 *
 * \sa Ratio
 *
 * \todo Could be a good idea to change the selecton into one arbitrary
 * square that can be moved by interacting with the picture.
 */
class GraphSelector : public QGraphicsView {
    Q_OBJECT

  public:
    explicit GraphSelector(QWidget* parent = nullptr);
    ~GraphSelector();
    void draw(int);
    void setRes(int);
    void init();
    int getJ();
    int getI();

  private:
    QGraphicsScene* scene;
    QGraphicsRectItem* selectedRect;

    int res, i, j;

  private slots:
    void mousePressEvent(QMouseEvent* event);

  signals:
    void selectionChanged(int, int);
};

#endif  // GRAPHSELECTOR_H
