/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "lineeditdoubledelegate.h"

#include <QLineEdit>

LineEditDoubleDelegate::LineEditDoubleDelegate(QObject* parent) : QStyledItemDelegate(parent) {}


QWidget* LineEditDoubleDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /* option */,
                                              const QModelIndex& /* index */) const {
    QLineEdit* editor = new QLineEdit(parent);
    editor->setFrame(false);
    editor->setValidator(new QDoubleValidator(parent));
    return editor;
}


void LineEditDoubleDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const {
    double value = index.model()->data(index, Qt::EditRole).toDouble();

    QLineEdit* lineEdit = static_cast<QLineEdit*>(editor);
    lineEdit->setText(QString::number(value));
}


void LineEditDoubleDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const {
    QLineEdit* lineEdit = static_cast<QLineEdit*>(editor);
    double value = lineEdit->text().toDouble();

    model->setData(index, value, Qt::EditRole);
}


void LineEditDoubleDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option,
                                                  const QModelIndex& /* index */) const {
    editor->setGeometry(option.rect);
}
