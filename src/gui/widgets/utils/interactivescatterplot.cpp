/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "interactivescatterplot.h"


InteractiveScatterPlot::InteractiveScatterPlot(QWidget* parent) : QCustomPlot(parent) {
    pointSize = 10;
    grabbedPoint = -1;
    selectedPoint = -1;
    highlightedSection = 0;
    axisMinx = -10;
    axisMiny = -10;

    setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    QPen pen;

    addGraph();
    pen.setColor(QColor(100, 150, 255));
    pen.setWidthF(2);
    graph(0)->setPen(pen);
    graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, pointSize));

    addGraph();
    pen.setColor(QColor(0, 0, 255));
    pen.setWidthF(3);
    graph(1)->setPen(pen);
    graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, pointSize));

    addGraph();
    pen.setColor(QColor(255, 0, 0));
    graph(2)->setPen(pen);
    graph(2)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, pointSize));

    connect(this, &InteractiveScatterPlot::pointsChanged, this, &InteractiveScatterPlot::plotScatter);

    rescalingTimer = new QTimer(this);
    connect(rescalingTimer, &QTimer::timeout, this, &InteractiveScatterPlot::cursorOutside);

    zoomRect = new QCPItemRect(this);

    zoomRect->setVisible(false);
}

/*!
 * Overrided mousePressEvent.
 * Checks if a point or a line is grabbed. Saves the event position.
 */
void InteractiveScatterPlot::mousePressEvent(QMouseEvent* event) {
    double x = event->pos().x();
    double y = event->pos().y();

    for (int i = 0; i < points.size(); i++) {
        double pointX = xAxis->coordToPixel(points[i].x);
        double pointY = yAxis->coordToPixel(points[i].y);
        if ((pointX - x) * (pointX - x) + (pointY - y) * (pointY - y) < pointSize * pointSize) {
            grabbedPoint = i;
            selectedPoint = i;
            plotScatter();
            return;
        }
    }

    for (int i = 0; i < points.size() - 1; i++) {
        double pointX = xAxis->coordToPixel(points[i].x);
        double pointY = yAxis->coordToPixel(points[i].y);
        double dx = xAxis->coordToPixel(points[i + 1].x) - pointX;
        double dy = yAxis->coordToPixel(points[i + 1].y) - pointY;
        if (pointX > x || pointX + dx < x) continue;
        double a = ((x - pointX) * dx + (y - pointY) * dy) / (dx * dx + dy * dy);
        double d2 = std::pow((x - pointX - a * dx), 2) + std::pow(y - pointY - a * dy, 2);
        if (d2 < 25) {
            if (event->buttons() == Qt::LeftButton) {
                glm::dvec2 p;
                p.x = xAxis->pixelToCoord(x);
                p.y = yAxis->pixelToCoord(y);
                points.insert(i + 1, p);
                emit sectionSplitted(i);
                grabbedPoint = i + 1;
                selectedPoint = i + 1;
                emit pointsChanged();
            } else if (event->buttons() == Qt::RightButton) {
                emit sectionChanged(i);
                setHighlightedSection(i);
            }

            return;
        }
    }

    grabbedPoint = -1;
    grabbedCoordinate.x = xAxis->pixelToCoord(event->pos().x());
    grabbedCoordinate.y = yAxis->pixelToCoord(event->pos().y());
    QCustomPlot::mousePressEvent(event);
    lastCursorPos = event->pos();
}


/*!
 * Overrided mouseReleaseEvent.
 * Releases the grabbed point. Completes the "zoom to rectangle" interaction if it was initiated.
 */
void InteractiveScatterPlot::mouseReleaseEvent(QMouseEvent* event) {
    if (zoomRect->visible()) {
        xAxis->setRangeLower(zoomRect->topLeft->coords().x());
        yAxis->setRangeLower(zoomRect->bottomRight->coords().y());
        xAxis->setRangeUpper(zoomRect->bottomRight->coords().x());
        yAxis->setRangeUpper(zoomRect->topLeft->coords().y());
    }
    grabbedPoint = -1;
    zoomRect->setVisible(false);
    replot();
    QCustomPlot::mouseReleaseEvent(event);
}


/*!
 * Overrided mouseMoveEvent.
 * Moves the grabbed point as close to the event position as allowed. Starts rescaling the axes if a point is being
 * dragged and the event position is ouside the plotted area.
 */
void InteractiveScatterPlot::mouseMoveEvent(QMouseEvent* event) {
    if (event->buttons() == Qt::LeftButton) {
        if (grabbedPoint > 0) {
            movePointToPos(grabbedPoint, event->pos());

            double x = xAxis->pixelToCoord(event->pos().x());
            double y = yAxis->pixelToCoord(event->pos().y());
            if ((x > xAxis->range().upper) | (x < xAxis->range().lower) | (y > yAxis->range().upper) |
                (y < yAxis->range().lower)) {
                rescalingTimer->start(50);
            } else {
                rescalingTimer->stop();
            }
        } else if (grabbedPoint == -1) {
            double topLeftx = std::min(grabbedCoordinate.x, xAxis->pixelToCoord(event->x()));
            double topLefty = std::max(grabbedCoordinate.y, yAxis->pixelToCoord(event->y()));
            double bottomRightx = std::max(grabbedCoordinate.x, xAxis->pixelToCoord(event->x()));
            double bottomRighty = std::min(grabbedCoordinate.y, yAxis->pixelToCoord(event->y()));

            topLeftx = std::max(topLeftx, axisMinx);
            topLefty = std::max(topLefty, axisMiny);
            bottomRightx = std::max(bottomRightx, axisMinx);
            bottomRighty = std::max(bottomRighty, axisMiny);

            zoomRect->topLeft->setCoords(topLeftx, topLefty);
            zoomRect->bottomRight->setCoords(bottomRightx, bottomRighty);
            zoomRect->setVisible(true);
            replot();
        }
    } else if (event->buttons() == Qt::RightButton) {
        double width = xAxis->range().upper - xAxis->range().lower;
        double height = yAxis->range().upper - yAxis->range().lower;
        double x1 = xAxis->pixelToCoord(event->pos().x()) - xAxis->range().lower;
        double y1 = yAxis->pixelToCoord(event->pos().y()) - yAxis->range().lower;

        double newMinx = xAxis->pixelToCoord(lastCursorPos.x()) - x1;
        double newMiny = yAxis->pixelToCoord(lastCursorPos.y()) - y1;

        xAxis->setRangeLower(std::max(newMinx, axisMinx));
        yAxis->setRangeLower(std::max(newMiny, axisMiny));
        xAxis->setRangeUpper(std::max(newMinx, axisMinx) + width);
        yAxis->setRangeUpper(std::max(newMiny, axisMiny) + height);
        replot();
    }
    lastCursorPos = event->pos();
}

/*!
 * Overrided wheelEvent.
 * Changes the wheelEvent so that the coordinates below axisMinx and axisMiny are not shown while zooming.
 */
void InteractiveScatterPlot::wheelEvent(QWheelEvent* event) {
    QCustomPlot::wheelEvent(event);
    xAxis->setRangeLower(std::max(xAxis->range().lower, axisMinx));
    yAxis->setRangeLower(std::max(yAxis->range().lower, axisMiny));
    replot();
}

void InteractiveScatterPlot::movePointToPos(int point, QPoint pos) {
    double x = xAxis->pixelToCoord(pos.x());
    double y = yAxis->pixelToCoord(pos.y());

    if (point > 0) {
        x = std::max(x, points[point - 1].x);
    } else {
        x = std::max(x, 0.0);
    }

    if (point < points.length() - 1) {
        x = std::min(x, points[point + 1].x);
    }

    x = std::min(x, xAxis->range().upper);
    x = std::max(x, xAxis->range().lower);

    y = std::min(y, yAxis->range().upper);
    y = std::max(y, yAxis->range().lower);

    y = std::max(y, 0.0);
    points[grabbedPoint].x = x;
    points[grabbedPoint].y = y;
    emit pointsChanged();
}


/*!
 * Rescales the axes to the data and adds some space around it.
 */
void InteractiveScatterPlot::fitToData() {
    rescaleAxes();

    double pixWidthX = xAxis->pixelToCoord(1) - xAxis->pixelToCoord(0);
    double pixWidthY = yAxis->pixelToCoord(0) - yAxis->pixelToCoord(1);

    xAxis->setRangeLower(std::max(xAxis->range().lower - 10 * pixWidthX, axisMinx));
    yAxis->setRangeLower(std::max(yAxis->range().lower - 10 * pixWidthY, axisMiny));

    xAxis->setRangeUpper(xAxis->range().upper + 10 * pixWidthX);
    yAxis->setRangeUpper(yAxis->range().upper + 10 * pixWidthY);

    replot();
}


/*!
 * Rescales the axes if the cursor is outside the plotting area.
 */
void InteractiveScatterPlot::cursorOutside() {
    if (grabbedPoint != -1) {
        double dx = xAxis->range().upper - xAxis->range().lower;
        double dy = yAxis->range().upper - yAxis->range().lower;

        if (xAxis->pixelToCoord(lastCursorPos.x()) > xAxis->range().upper) {
            xAxis->setRangeUpper(xAxis->range().upper + dx * 0.01);
        } else if (xAxis->pixelToCoord(lastCursorPos.x()) < xAxis->range().lower) {
            xAxis->setRangeLower(std::max(xAxis->range().lower - dx * 0.01, axisMinx));
        }

        if (yAxis->pixelToCoord(lastCursorPos.y()) > yAxis->range().upper) {
            yAxis->setRangeUpper(yAxis->range().upper + dy * 0.01);
        } else if (yAxis->pixelToCoord(lastCursorPos.y()) < yAxis->range().lower) {
            yAxis->setRangeLower(std::max(yAxis->range().lower - dy * 0.01, axisMiny));
        }

        movePointToPos(grabbedPoint, lastCursorPos);
        plotScatter();
    } else {
        rescalingTimer->stop();
    }
}

/*!
 * Adds a point (x,y) to the scatter plot.
 */
void InteractiveScatterPlot::addPoint(double x, double y) {
    glm::dvec2 p;
    p.x = x;
    p.y = y;

    int i = 0;
    for (; i < points.length() - 1; i++) {
        if (x > points[i].x && x < points[i + 1].x) break;
    }
    points.insert(i + 1, p);
    if (points.length() > 2) {
        emit sectionSplitted(i);
    } else if (points.length() == 2) {
        emit sectionAdded();
    }

    emit pointsChanged();
}

/*!
 * Sets the highlighte section that will be plotted with different color.
 */
void InteractiveScatterPlot::setHighlightedSection(int i) {
    highlightedSection = i;
    plotScatter();
}

/*!
 * Changes the coordinates of the ending points of a section.
 */
void InteractiveScatterPlot::modifySection(int section, double x1, double x2, double y1, double y2) {
    points[section].x = x1;
    points[section].y = y1;
    points[section + 1].x = x2;
    points[section + 1].y = y2;
    emit plotScatter();
}


/*!
 * Removes a point from the scatter plot.
 */
bool InteractiveScatterPlot::removePoint(int point) {
    if ((point < 0) | (point > points.length() - 1)) return false;

    points.removeAt(point);
    selectedPoint = std::min(selectedPoint, points.length() - 1);
    if (highlightedSection >= point) {
        highlightedSection--;
    }
    plotScatter();
    return true;
}


/*!
 * Returns the index of the selected point.
 */
int InteractiveScatterPlot::getSelectedPoint() {
    return selectedPoint;
}


/*!
 * Plots the data and makes sure that the correct order of points with the same x-coordinate remains.
 * Also draws draws the highlighted section and the selected point.
 */
void InteractiveScatterPlot::plotScatter() {
    QVector<double> x, y;
    x.push_back(points[0].x);
    y.push_back(points[0].y);
    for (int i = 1; i < points.length(); ++i) {
        x.push_back(std::max(x[i - 1] + 1e-4 * (xAxis->pixelToCoord(1) - xAxis->pixelToCoord(0)), points[i].x));
        y.push_back(points[i].y);
    }
    graph(0)->setData(x, y);

    if (highlightedSection + 1 < points.length()) {
        QVector<double> hlx, hly;
        hlx.push_back(x[highlightedSection]);
        hlx.push_back(x[highlightedSection + 1]);
        hly.push_back(y[highlightedSection]);
        hly.push_back(y[highlightedSection + 1]);
        graph(1)->setData(hlx, hly);
    } else {
        graph(1)->clearData();
    }

    if (selectedPoint > -1) {
        QVector<double> sx, sy;
        sx.push_back(x[selectedPoint]);
        sy.push_back(y[selectedPoint]);
        graph(2)->setData(sx, sy);
    } else {
        graph(2)->clearData();
    }

    replot();
}

/*!
 * Returns the points as a QList.
 */
QList<glm::dvec2> InteractiveScatterPlot::getPoints() {
    return points;
}

/*!
 * Sets the points.
 */
void InteractiveScatterPlot::setPoints(QList<glm::dvec2> pointList) {
    points = pointList;
    emit pointsChanged();
}
