/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "particlegrid.h"

#include <QDebug>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>

#include <cmath>

ParticleGrid::ParticleGrid(const XyzData& frame, bool initialize) : m_atoms(frame) {
    if (initialize) update();
}

void ParticleGrid::update() {
    const Box& limits = m_atoms.atomLims;

    // Generating so many bins that they have 20 atoms on average (assuming homogenious bulk)
    unsigned int bins = static_cast<unsigned int>(m_atoms.atoms.size() / 20);

    // Solving the number of bins in each direction so that bins are cubic.
    // x*y*z = bins
    // x/dx = y/dy = z/dz

    bool xDim = limits.dx() > 1e-8;
    bool yDim = limits.dy() > 1e-8;
    bool zDim = limits.dz() > 1e-8;

    double x = 1;
    double y = 1;
    double z = 1;

    // Taking into account the possibility that the system is only 2 or 1 dimensional.
    if (xDim && yDim && zDim) {
        x = std::pow(bins * limits.dx() * limits.dx() / (limits.dy() * limits.dz()), 1.0 / 3);
        y = x * limits.dy() / limits.dx();
        z = x * limits.dz() / limits.dx();
    } else if (xDim && yDim) {
        x = std::pow(bins * limits.dx() * limits.dx() / (limits.dy()), 1.0 / 2);
        y = x * limits.dy() / limits.dx();
    } else if (xDim && zDim) {
        x = std::pow(bins * limits.dx() * limits.dx() / (limits.dz()), 1.0 / 2);
        z = x * limits.dz() / limits.dx();
    } else if (yDim && zDim) {
        y = std::pow(bins * limits.dy() * limits.dy() / (limits.dz()), 1.0 / 2);
        z = x * limits.dz() / limits.dy();
    } else if (xDim) {
        x = bins;
    } else if (yDim) {
        y = bins;
    } else if (zDim) {
        z = bins;
    }

    unsigned int resX = std::max(1, static_cast<int>(round(x)));
    unsigned int resY = std::max(1, static_cast<int>(round(y)));
    unsigned int resZ = std::max(1, static_cast<int>(round(z)));

    update(resX, resY, resZ);
}

void ParticleGrid::update(unsigned int resX, unsigned int resY, unsigned int resZ) {
    const Box& limits = m_atoms.atomLims;

    m_resX = resX;
    m_resY = resY;
    m_resZ = resZ;

    m_grid.clear();
    m_grid.resize(m_resX);
    for (unsigned int i = 0; i < m_resX; ++i) {
        m_grid[i].resize(m_resY);

        for (unsigned int j = 0; j < m_resY; ++j) {
            m_grid[i][j].resize(m_resZ);
        }
    }

    for (const Atom& a : m_atoms.atoms) {
        glm::dvec3 d = a.position - glm::dvec3(limits.x1, limits.y1, limits.z1);
        unsigned int xInd = std::max(0, std::min<int>(d.x / limits.dx() * m_resX, m_resX - 1));
        unsigned int yInd = std::max(0, std::min<int>(d.y / limits.dy() * m_resY, m_resY - 1));
        unsigned int zInd = std::max(0, std::min<int>(d.z / limits.dz() * m_resZ, m_resZ - 1));
        m_grid[xInd][yInd][zInd].push_back(&a);
    }
}

/*!
 * Returns a pointer to the atom whose center point is closest to the given coordinate. If radii is given, the closest
 * atom has to be less than the type specific radius away from the position.
 */
const Atom* ParticleGrid::closestAtom(const glm::dvec3& position, const QHash<int, float>* radii) const {

    const Box& limits = m_atoms.atomLims;
    glm::dvec3 d = position - glm::dvec3(limits.x1, limits.y1, limits.z1);
    unsigned int xInd = std::max(0, std::min<int>(d.x / limits.dx() * m_resX, m_resX - 1));
    unsigned int yInd = std::max(0, std::min<int>(d.y / limits.dy() * m_resY, m_resY - 1));
    unsigned int zInd = std::max(0, std::min<int>(d.z / limits.dz() * m_resZ, m_resZ - 1));

    const Atom* closest = nullptr;
    double closestDist2 = 1e10;

    unsigned int x1 = xInd == 0 ? 0 : xInd - 1;
    unsigned int x2 = std::min(xInd + 2, m_resX);
    unsigned int y1 = yInd == 0 ? 0 : yInd - 1;
    unsigned int y2 = std::min(yInd + 2, m_resY);
    unsigned int z1 = zInd == 0 ? 0 : zInd - 1;
    unsigned int z2 = std::min(zInd + 2, m_resZ);

    // Loops over the bin that "position" is in and all the bins next to it.
    for (unsigned int i = x1; i < x2; ++i) {
        for (unsigned int j = y1; j < y2; ++j) {
            for (unsigned int k = z1; k < z2; ++k) {
                for (const Atom* a : m_grid[i][j][k]) {
                    glm::dvec3 d = a->position - position;
                    double dist2 = glm::length2(d);
                    if (dist2 < closestDist2) {
                        if (radii) {
                            double rad = static_cast<double>(radii->value(a->type));
                            if (dist2 > rad * rad) continue;
                        }
                        closest = a;
                        closestDist2 = dist2;
                    }
                }
            }
        }
    }

    return closest;
}
