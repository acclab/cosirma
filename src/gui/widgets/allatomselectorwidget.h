/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ALLATOMSELECTORWIDGET_H
#define ALLATOMSELECTORWIDGET_H

#include "api/atomselections.h"
#include "gui/widgets/abstractatomselectorwidget.h"

class AllAtomSelectorWidget : public AbstractAtomSelectorWidget {
    Q_OBJECT

  public:
    explicit AllAtomSelectorWidget(PlotCellGLWidget* context, QWidget* parent = nullptr)
            : AbstractAtomSelectorWidget(context, parent) {}

    const AbstractAtomSelection& selector() override { return m_selector; }

    void connectToPlot() override {}

  private:
    AllAtomSelection m_selector;
};

#endif  // ALLATOMSELECTORWIDGET_H
