/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TYPECONFLICTSOLVERWIDGET_H
#define TYPECONFLICTSOLVERWIDGET_H

#include "api/entry.h"

#include <QComboBox>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QWidget>

#include <memory>

namespace Ui {
class TypeConflictSolverWidget;
}

class TypeConflictSolverWidget : public QWidget {
    Q_OBJECT

  public:
    explicit TypeConflictSolverWidget(QWidget* parent = nullptr);
    ~TypeConflictSolverWidget();

    enum Problem { IllegalType, IllegalMass, IllegalSymbol, SymbolConflict, MassConflict };

    struct ResolveAction {
        enum Action { DoNothing, ChangeType, ChangeSymbol, ChangeMass, CopyProperties };

        Action action = DoNothing;
        int type = 0;
        int targetType = 0;
        double newMass = 1.0;
        QString newSymbol;
        QString message = "";
    };

    void changeEntry(ExtendedEntryData* newEntry);
    bool isResolved();

  private:
    struct TypeRow {
        QComboBox* typeCB = 0;
        QLineEdit* symbolLE = 0;
        QDoubleSpinBox* massSB = 0;
        QPushButton* ptPB = 0;
        QLabel* iconLabel = 0;
        QLabel* colorLabel = 0;
        QPushButton* resolvePB = 0;
    };

    Ui::TypeConflictSolverWidget* ui;

    QSet<int> conflictingTypes;

    QHash<int, TypeRow> typeRows;
    QList<QWidget*> tableWidgets;

    ExtendedEntryData* entry = nullptr;

    void writeTypeTable();

    bool attemptToChangeType(int fromType, int toType, bool ask = true);
    void attemptToReplaceMass(int type, double newMass, bool ask = true);

    void updateSymbol(int type, QString symbol, bool ask = true);
    void updateMass(int type, double mass);
    void showPeriodicTable(int type);

    QList<Problem> problems(int type);
    bool updateStatus(int type);
    void updateStatuses();

    ResolveAction findAction(int type, const TypeData& types);

    void updateColors(const QHash<int, QColor>& colors);

    void connectRow(TypeRow row, bool c);

    void checkIfResolved();

  private slots:
    void resolveProblems(int type);
    void resolveAllProblems();

  signals:
    void entryChanged();
    void conflictsResolved(bool);
    void tableWritten(QWidget* lastInTabOrder);
};

#endif  // TYPECONFLICTSOLVERWIDGET_H
