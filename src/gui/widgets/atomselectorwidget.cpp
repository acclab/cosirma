/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "atomselectorwidget.h"
#include "ui_atomselectorwidget.h"

#include "allatomselectorwidget.h"
#include "atomselectorarraywidget.h"
#include "boxselectorwidget.h"
#include "cylinderselectorwidget.h"
#include "sphereselectorwidget.h"
#include "typeatomselectorwidget.h"
#include "utils/wheelguard.h"

AtomSelectorWidget::AtomSelectorWidget(PlotCellGLWidget* context, QWidget* parent)
        : AbstractAtomSelectorWidget(context, parent), ui(new Ui::AtomSelectorWidget) {
    ui->setupUi(this);

    ui->atomSelectorOption1CB->addItem("All", AtomSelectorGeneralType::All);
    ui->atomSelectorOption1CB->addItem("Combination", AtomSelectorGeneralType::Combination);
    ui->atomSelectorOption1CB->addItem("Type", AtomSelectorGeneralType::Type);
    ui->atomSelectorOption1CB->addItem("Volume", AtomSelectorGeneralType::Volume);
    zero();

    QVector<QWidget*> wheelGuardedWidgets = {ui->atomSelectorOption1CB, ui->atomSelectorOption2CB};
    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(new WheelGuard(this));
    }

    connect(ui->atomSelectorOption1CB, QOverload<int>::of(&QComboBox::currentIndexChanged), this,
            &AtomSelectorWidget::option1Changed);
    connect(ui->atomSelectorOption2CB, QOverload<int>::of(&QComboBox::currentIndexChanged), this,
            &AtomSelectorWidget::option2Changed);

    connect(ui->closePB, &QPushButton::clicked, this, &AtomSelectorWidget::closeRequested);
}

AtomSelectorWidget::~AtomSelectorWidget() {
    delete ui;
}

const AbstractAtomSelection& AtomSelectorWidget::selector() {

    if (!m_selectedWidget) return m_nullSelector;

    if (ui->invertPB->isChecked()) {
        m_invertedSelector.setAtomSelection(&m_selectedWidget->selector());
        return m_invertedSelector;
    } else {
        return m_selectedWidget->selector();
    }
}

void AtomSelectorWidget::setEntry(const ExtendedEntryData* entry) {
    m_entry = entry;
    if (m_selectedWidget) {
        m_selectedWidget->setEntry(entry);
    }
}

void AtomSelectorWidget::init() {
    if (m_selectedWidget) m_selectedWidget->init();
}

void AtomSelectorWidget::updateAtoms() {
    if (m_selectedWidget) m_selectedWidget->updateAtoms();
}

void AtomSelectorWidget::updateCellLims() {
    if (m_selectedWidget) m_selectedWidget->updateCellLims();
}

void AtomSelectorWidget::updateTypes() {
    if (m_selectedWidget) m_selectedWidget->updateTypes();
}

void AtomSelectorWidget::connectToPlot() {
    if (m_selectedWidget) m_selectedWidget->connectToPlot();
}

void AtomSelectorWidget::hidePlottables() {
    if (m_selectedWidget) m_selectedWidget->hidePlottables();
}

void AtomSelectorWidget::zero() {
    if (m_selectedWidget) {
        delete m_selectedWidget;
        m_selectedWidget = nullptr;
    }
    ui->atomSelectorOption1CB->blockSignals(true);
    //    ui->atomSelectorOption1CB->setCurrentText("Volume");
    ui->atomSelectorOption1CB->setCurrentText("All");
    ui->atomSelectorOption1CB->blockSignals(false);

    option1Changed();
}

void AtomSelectorWidget::setClosable(bool closable) {
    ui->closePB->setVisible(closable);
}

AtomSelectorWidget::AtomSelectorType AtomSelectorWidget::currentType() {
    AtomSelectorGeneralType generalType =
        static_cast<AtomSelectorGeneralType>(ui->atomSelectorOption1CB->currentData().toInt());

    switch (generalType) {
        case All:
            return AllSelector;

        case Type:
            return TypeSelector;

        default:
            return static_cast<AtomSelectorType>(ui->atomSelectorOption2CB->currentData().toInt());
    }
}

void AtomSelectorWidget::option1Changed() {
    AtomSelectorGeneralType newType =
        static_cast<AtomSelectorGeneralType>(ui->atomSelectorOption1CB->currentData().toInt());

    ui->atomSelectorOption2CB->blockSignals(true);
    ui->atomSelectorOption2CB->clear();

    switch (newType) {
        case Volume:
            ui->atomSelectorOption2CB->addItem("Box", AtomSelectorType::BoxSelector);
            ui->atomSelectorOption2CB->addItem("Cylinder", AtomSelectorType::CylinderSelector);
            ui->atomSelectorOption2CB->addItem("Sphere", AtomSelectorType::SphereSelector);
            break;

        case Combination:
            ui->atomSelectorOption2CB->addItem("Union", AtomSelectorType::UnionArray);
            ui->atomSelectorOption2CB->addItem("Intersection", AtomSelectorType::IntersectionArray);
            break;

        default:
            break;
    }

    ui->atomSelectorOption2CB->setVisible(ui->atomSelectorOption2CB->count() > 0);
    option2Changed();
    ui->atomSelectorOption2CB->blockSignals(false);

    emit modeChanged();
}

void AtomSelectorWidget::option2Changed() {
    AtomSelectorType newType = currentType();
    AbstractAtomSelectorWidget* newWidget = nullptr;
    bool deleteOld = true;

    switch (newType) {
        case BoxSelector: {
            newWidget = new BoxSelectorWidget(m_context);
            break;
        }
        case CylinderSelector: {
            newWidget = new CylinderSelectorWidget(m_context);
            break;
        }
        case SphereSelector: {
            newWidget = new SphereSelectorWidget(m_context);
            break;
        }
        case UnionArray: {
            deleteOld = false;
            AtomSelectorArrayWidget* tempWidget = qobject_cast<AtomSelectorArrayWidget*>(m_selectedWidget);
            if (!tempWidget) {
                tempWidget = new AtomSelectorArrayWidget(m_context);
                tempWidget->addSelector();
                tempWidget->addSelector();
                deleteOld = true;
            }
            tempWidget->setMode(ArrayAtomSelection::Union);
            newWidget = tempWidget;
            break;
        }
        case IntersectionArray: {
            deleteOld = false;
            AtomSelectorArrayWidget* tempWidget = qobject_cast<AtomSelectorArrayWidget*>(m_selectedWidget);
            if (!tempWidget) {
                tempWidget = new AtomSelectorArrayWidget(m_context);
                tempWidget->addSelector();
                tempWidget->addSelector();
                deleteOld = true;
            }
            tempWidget->setMode(ArrayAtomSelection::Intersection);
            newWidget = tempWidget;
            break;
        }
        case AllSelector: {
            newWidget = new AllAtomSelectorWidget(m_context);
            break;
        }
        case TypeSelector: {
            newWidget = new TypeAtomSelectorWidget(m_context);
            break;
        }
    }

    if (m_selectedWidget && deleteOld) {
        delete m_selectedWidget;
    }

    if (newWidget && deleteOld) {
        newWidget->setEntry(m_entry);
        newWidget->connectToPlot();
        ui->selectorPlaceHolder->layout()->addWidget(newWidget);
    }

    m_selectedWidget = newWidget;

    emit modeChanged();
}
