/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "typetable.h"

#include "api/entryeditor.h"
#include "api/programsettings.h"
#include "gui/dialogs/chooseelementdialog.h"
#include "gui/widgets/utils/clickablelabel.h"
#include "gui/widgets/utils/wheelguard.h"

#include <QAction>
#include <QCheckBox>
#include <QColorDialog>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QHeaderView>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>

TypeTable::TypeTable(EntryEditor* editor, QWidget* parent) : QWidget(parent), m_editor(editor) {

    m_table = new QTableWidget;
    m_addTypePB = new QPushButton("+");

    QVBoxLayout* l1 = new QVBoxLayout;
    l1->setContentsMargins(0, 0, 0, 0);
    QHBoxLayout* l2 = new QHBoxLayout;
    l2->setContentsMargins(0, 0, 0, 0);
    l2->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::MinimumExpanding));
    l2->addWidget(m_addTypePB);
    l1->addWidget(m_table);
    l1->addItem(l2);
    setLayout(l1);

    m_table->setStyleSheet("QTableWidget::item:selected{ background-color: lightblue }");
    m_table->setAutoScroll(false);
    m_table->setTabKeyNavigation(false);
    m_table->setDragDropOverwriteMode(false);
    m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_table->setFocusPolicy(Qt::NoFocus);
    m_table->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_table->setSelectionMode(QAbstractItemView::SingleSelection);
    m_table->setContextMenuPolicy(Qt::CustomContextMenu);

    m_table->verticalHeader()->setVisible(false);

    m_table->horizontalHeader()->setHighlightSections(false);
    m_table->setColumnCount(4);
    m_table->setHorizontalHeaderItem(0, new QTableWidgetItem("Type"));
    m_table->setHorizontalHeaderItem(1, new QTableWidgetItem("Element"));
    m_table->setHorizontalHeaderItem(2, new QTableWidgetItem("Mass [u]"));
    m_table->setHorizontalHeaderItem(3, new QTableWidgetItem("Atoms"));

    // Adding one row to the table and resizing it to the contents in order to make sure that the column widths are good
    // on any platform
    AtomType tempType;
    m_table->setRowCount(1);
    addTypeRow(tempType, 0, 60);
    m_table->resizeColumnsToContents();
    m_table->setRowCount(0);
    m_table->horizontalHeader()->setStretchLastSection(true);
    m_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);


    // Initializing and connecting the type table context menu
    m_contextMenu = new QMenu("Context menu", this);
    QAction* ptAction = new QAction("Periodic Table", this);
    QAction* deleteAction = new QAction("Delete", this);
    m_contextMenu->addAction(ptAction);
    m_contextMenu->addAction(deleteAction);

    connect(ptAction, &QAction::triggered, [this] {
        int row = m_table->currentRow();
        if (row >= m_sortedTypes.count()) return;
        int typeNumber = m_sortedTypes[row];
        AtomType oldType = m_editor->modifiedEntry().data.types.typeList.value(typeNumber);
        AtomType newType = oldType;
        ChooseElementDialog e(this);
        if (e.exec()) {
            ElementReader r;
            newType.symbol = r.getSymbol(e.Z());
            newType.mass = r.getMass(e.Z());
        }
        if (oldType != newType) {
            EntryEditor::TypeEditAction a(typeNumber, newType);
            m_editor->edit(a);
        }
    });

    connect(deleteAction, &QAction::triggered, [this] {
        int row = m_table->currentRow();
        if (row >= m_sortedTypes.count()) return;
        int typeNumber = m_sortedTypes[row];
        int amount =
            m_editor->modifiedEntry().frame.amounts[typeNumber] + m_editor->modifiedEntry().frame.amounts[-typeNumber];
        if (amount > 0) {
            QMessageBox::StandardButton reply =
                QMessageBox::warning(this, QString("Delete type %1?").arg(typeNumber),
                                     QString("There are %1 atoms of type %2. They will be removed from the structure.")
                                         .arg(QString::number(amount), QString::number(typeNumber)),
                                     QMessageBox::Yes | QMessageBox::No);
            if (reply != QMessageBox::Yes) return;
        }
        EntryEditor::TypeDeleteAction a(typeNumber);
        m_editor->edit(a);
    });

    connect(m_table, &QTableWidget::customContextMenuRequested, [this](const QPoint& pos) {
        m_contextMenu->popup(m_table->mapToGlobal(pos) + QPoint(0, m_contextMenu->sizeHint().height() / 2));
    });

    connect(m_addTypePB, &QPushButton::clicked, [this] {
        int nextFreeNumber = 1;
        for (;; ++nextFreeNumber) {
            if (!m_editor->modifiedEntry().data.types.typeList.contains(nextFreeNumber)) break;
            if (nextFreeNumber == 10) return;
        }
        ElementReader r;
        AtomType newType;
        newType.typeNumber = nextFreeNumber;
        newType.symbol = r.getSymbol(1);
        newType.mass = r.getMass(1);
        EntryEditor::TypeAddAction action(newType);
        m_editor->edit(action);
    });

    updateTypes();

    connect(m_editor, &EntryEditor::typesChanged, this, &TypeTable::updateTypes);
    connect(m_editor, &EntryEditor::atomsChanged, this, &TypeTable::updateTypes);
    connect(m_editor, &EntryEditor::entryChanged, this, &TypeTable::updateTypes);
}

TypeTable::~TypeTable() {
    delete m_contextMenu;
}

void TypeTable::updateTypes() {
    int i = 0;

    m_sortedTypes = m_editor->modifiedEntry().data.types.typeList.keys().toVector();
    std::sort(m_sortedTypes.begin(), m_sortedTypes.end());

    m_table->clearSelection();
    m_table->setRowCount(m_sortedTypes.length());
    int tableHeight = m_table->horizontalHeader()->height() + 2;
    int rowHeight = 60;

    for (int typeNumber : m_sortedTypes) {
        if (typeNumber < 0) continue;
        AtomType type = m_editor->modifiedEntry().data.types.typeList.value(qAbs(typeNumber));

        addTypeRow(type, i, rowHeight);
        tableHeight += rowHeight;
        ++i;
    }
    m_table->setFixedHeight(tableHeight);

    m_addTypePB->setEnabled(m_sortedTypes.length() < 9);
}

void TypeTable::addTypeRow(const AtomType& type, int row, int rowHeight) {
    int typeNumber = type.typeNumber;

    QWidget* typeWidget = new QWidget;
    QVBoxLayout* typeLayout = new QVBoxLayout;
    QComboBox* typeCB = new QComboBox;
    typeCB->setFocusPolicy(Qt::StrongFocus);
    typeCB->installEventFilter(new WheelGuard(typeCB));
    typeCB->setFixedWidth(35);
    for (int i = 1; i < 10; ++i) {
        if (i == typeNumber || !m_editor->modifiedEntry().data.types.typeList.contains(i)) {
            typeCB->addItem(QString::number(i), i);
        }
    }
    typeCB->setCurrentIndex(typeCB->findData(typeNumber));
    typeLayout->addWidget(typeCB);
    typeWidget->setLayout(typeLayout);

    QWidget* symbolWidget = new QWidget;
    QVBoxLayout* symbolLayout = new QVBoxLayout;
    QLineEdit* symbolLE = new QLineEdit(type.symbol);
    symbolLE->setFixedWidth(30);
    symbolLayout->addWidget(symbolLE);
    symbolWidget->setLayout(symbolLayout);

    QWidget* massWidget = new QWidget;
    QVBoxLayout* massLayout = new QVBoxLayout;
    QDoubleSpinBox* massSB = new QDoubleSpinBox;
    massSB->setFocusPolicy(Qt::StrongFocus);
    massSB->installEventFilter(new WheelGuard(massSB));
    massSB->setMaximum(9999.999999);
    massSB->setDecimals(6);
    massSB->setValue(type.mass);
    massLayout->addWidget(massSB);
    massWidget->setLayout(massLayout);

    QWidget* atomsWidget = new QWidget;
    QGridLayout* atomsLayout = new QGridLayout;

    ClickableLabel* freeColor = new ClickableLabel;
    freeColor->setColor(ProgramSettings::getInstance().visualSettings().atomTypeColors()[typeNumber]);
    freeColor->setFixedSize(20, 20);

    ClickableLabel* fixedColor = new ClickableLabel;
    fixedColor->setColor(ProgramSettings::getInstance().visualSettings().atomTypeColors()[-typeNumber]);
    fixedColor->setFixedSize(20, 20);

    int freeAmount = m_editor->modifiedEntry().frame.amounts.value(typeNumber);
    int fixedAmount = m_editor->modifiedEntry().frame.amounts.value(-typeNumber);

    atomsLayout->addWidget(freeColor, 0, 0);
    atomsLayout->addWidget(new QLabel(QString("%1 free").arg(freeAmount)), 0, 1);

    atomsLayout->addWidget(fixedColor, 1, 0);
    atomsLayout->addWidget(new QLabel(QString("%1 fixed").arg(fixedAmount)), 1, 1);

    atomsWidget->setLayout(atomsLayout);

    m_table->setRowHeight(row, rowHeight);
    m_table->setCellWidget(row, 0, typeWidget);
    m_table->setCellWidget(row, 1, symbolWidget);
    m_table->setCellWidget(row, 2, massWidget);
    m_table->setCellWidget(row, 3, atomsWidget);

    // Connections
    connect(typeCB, QOverload<int>::of(&QComboBox::currentIndexChanged), [this, type, typeCB] {
        AtomType newType = type;
        newType.typeNumber = typeCB->currentData().toInt();
        if (newType != type) {
            EntryEditor::TypeEditAction a(type.typeNumber, newType);
            m_editor->edit(a);
        }
    });

    connect(symbolLE, &QLineEdit::textEdited, [symbolLE](QString t) {
        if (t.length() > 2) symbolLE->setText(t.left(2));
    });

    connect(symbolLE, &QLineEdit::editingFinished, [this, type, symbolLE] {
        ElementReader r;
        AtomType newType = type;
        int z = r.zFromSymbol(symbolLE->text());
        if (z > 0) {
            newType.symbol = r.getSymbol(z);
            newType.mass = r.getMass(z);
        } else {
            newType.symbol = symbolLE->text();
        }
        if (newType != type) {
            EntryEditor::TypeEditAction a(type.typeNumber, newType);
            m_editor->edit(a);
        }
    });

    connect(massSB, &QDoubleSpinBox::editingFinished, [this, type, massSB] {
        AtomType newType = type;
        if (newType.mass != massSB->value()) {
            newType.mass = massSB->value();
            EntryEditor::TypeEditAction a(type.typeNumber, newType);
            m_editor->edit(a);
        }
    });

    connect(freeColor, &ClickableLabel::clicked, [this, typeNumber] { typeColorChangeEvent(typeNumber); });
    connect(fixedColor, &ClickableLabel::clicked, [this, typeNumber] { typeColorChangeEvent(-typeNumber); });
    connect(&ProgramSettings::getInstance().visualSettings(), &VisualSettings::atomTypeColorsChanged, freeColor,
            [typeNumber, freeColor] {
                freeColor->setColor(ProgramSettings::getInstance().visualSettings().atomTypeColors().value(typeNumber));
            });
    connect(
        &ProgramSettings::getInstance().visualSettings(), &VisualSettings::atomTypeColorsChanged, fixedColor,
        [typeNumber, fixedColor] {
            fixedColor->setColor(ProgramSettings::getInstance().visualSettings().atomTypeColors().value(-typeNumber));
        });
}

void TypeTable::typeColorChangeEvent(int typeNumber) {
    QColorDialog dialog(this);
    dialog.setOption(QColorDialog::DontUseNativeDialog, true);
    dialog.setWindowTitle("Color Picker");
    dialog.setCurrentColor(ProgramSettings::getInstance().visualSettings().atomTypeColors()[typeNumber]);

    if (dialog.exec() == QDialog::Accepted) {
        QHash<int, QColor> colors = ProgramSettings::getInstance().visualSettings().atomTypeColors();
        if (colors.value(typeNumber) != dialog.selectedColor()) {
            colors[typeNumber] = dialog.selectedColor();
            ProgramSettings::getInstance().visualSettings().changeTypeColors(colors);
        }
    }
}
