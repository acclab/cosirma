/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "rangesliderbox.h"

#include "api/util.h"
#include "gui/widgets/utils/distancefromlimitsb.h"
#include "gui/widgets/utils/wheelguard.h"

#include <QDebug>
#include <QLayout>
#include <QtMath>

RangeSliderBox::RangeSliderBox(QWidget* parent) : QWidget(parent) {

    m_doubleSpinBox_lower = new DistanceFromLimitSB(this);
    m_doubleSpinBox_upper = new DistanceFromLimitSB(this);

    m_slider = new QxtSpanSlider(this);
    m_slider->setOrientation(Qt::Horizontal);
    m_slider->setHandleMovementMode(QxtSpanSlider::NoCrossing);
    m_slider->setRange(0, 1000);

    QHBoxLayout* l = new QHBoxLayout;
    l->setContentsMargins(0, 0, 0, 0);
    l->addWidget(m_doubleSpinBox_lower);
    l->addWidget(m_slider);
    l->addWidget(m_doubleSpinBox_upper);
    setLayout(l);

    WheelGuard* guard = new WheelGuard(this);
    m_doubleSpinBox_lower->installEventFilter(guard);
    m_doubleSpinBox_lower->setFocusPolicy(Qt::StrongFocus);
    m_doubleSpinBox_upper->installEventFilter(guard);
    m_doubleSpinBox_upper->setFocusPolicy(Qt::StrongFocus);
    m_slider->setFocusPolicy(Qt::ClickFocus);
    setFocusPolicy(Qt::NoFocus);

    // Connecting the widgets
    connect(m_doubleSpinBox_lower, QOverload<double>::of(&DistanceFromLimitSB::valueChanged), [this](double val) {
        if (val == m_doubleSpinBox_lower->minimum()) val = m_min;
        setLowerValue(val);
        emit rangeEdited(m_lower, m_upper);
    });
    connect(m_doubleSpinBox_upper, QOverload<double>::of(&DistanceFromLimitSB::valueChanged), [this](double val) {
        if (val == m_doubleSpinBox_upper->maximum()) val = m_max;
        setUpperValue(val);
        emit rangeEdited(m_lower, m_upper);
    });
    connect(m_slider, &QxtSpanSlider::lowerPositionChanged, [this](int val) {
        int sliderMin = m_slider->minimum();
        int sliderMax = m_slider->maximum();
        setLowerValue(m_min + (m_max - m_min) * (val - sliderMin) / (sliderMax - sliderMin));
        emit rangeEdited(m_lower, m_upper);
    });
    connect(m_slider, &QxtSpanSlider::upperPositionChanged, [this](int val) {
        int sliderMin = m_slider->minimum();
        int sliderMax = m_slider->maximum();
        setUpperValue(m_min + (m_max - m_min) * (val - sliderMin) / (sliderMax - sliderMin));
        emit rangeEdited(m_lower, m_upper);
    });

    updateWidgets();
}

/*!
 * Sets the amount of decimals in the DistanceFromLimitSBs.
 */
void RangeSliderBox::setPrecision(int decimals) {
    m_doubleSpinBox_lower->setDecimals(decimals);
    m_doubleSpinBox_upper->setDecimals(decimals);
}

/*!
 * If the mode is set to mirrored, changing one value of the widget will automatically
 * mirror the other value so hat it is as far from the limit as the one that was changed.
 */
void RangeSliderBox::setMirrored(bool b) {
    if (m_mirrored != b) {
        m_mirrored = b;
        m_upper = m_max + m_min - m_lower;
        updateWidgets();
    }
}

/*!
 * If the parameter is true, the part between the handles in the slider will be filled.
 * If false, the parts between handles and the limits are filled.
 */
void RangeSliderBox::setHighlightInside(bool b) {
    m_slider->setColorMode(b ? QxtSpanSlider::Inside : QxtSpanSlider::Outside);
}

/*!
 * If the parameter is true, sets the widget into a mode where the
 * spin boxes show the distance from limit instead of the actual value.
 */
void RangeSliderBox::setShowDistanceFromLimit(bool b) {
    if (b) {
        m_doubleSpinBox_lower->setDisplayMode(DistanceFromLimitSB::DistanceFromMinimum);
        m_doubleSpinBox_upper->setDisplayMode(DistanceFromLimitSB::DistanceFromMaximum);
    } else {
        m_doubleSpinBox_lower->setDisplayMode(DistanceFromLimitSB::Normal);
        m_doubleSpinBox_upper->setDisplayMode(DistanceFromLimitSB::Normal);
    }
}

/*!
 * Returns the lower value.
 */
double RangeSliderBox::lowerValue() {
    return m_lower;
}

/*!
 * Returns the upper value.
 */
double RangeSliderBox::upperValue() {
    return m_upper;
}

/*!
 * Sets the lower value.
 */
void RangeSliderBox::setLowerValue(double value) {
    value = std::min(m_max, std::max(m_min, value));
    if (m_mirrored) value = std::min(value, 0.5 * (m_min + m_max));
    if (m_lower != value) {
        m_lower = value;
        if (m_mirrored) m_upper = m_max - m_lower + m_min;
        updateWidgets();
    }
}

/*!
 * Sets the upper value.
 */
void RangeSliderBox::setUpperValue(double value) {
    value = std::min(m_max, std::max(m_min, value));
    if (m_mirrored) value = std::max(value, 0.5 * (m_min + m_max));
    if (m_upper != value) {
        m_upper = value;
        if (m_mirrored) m_lower = m_min + m_max - m_upper;
        updateWidgets();
    }
}

/*!
 * Sets the lower and upper values. If the "mirrored" mode is on, the lower value is prioritized.
 */
void RangeSliderBox::setRange(double value1, double value2) {
    double newLower = std::min(m_max, std::max(m_min, value1));
    double newUpper = std::min(m_max, std::max(m_min, value2));
    if (m_mirrored) newUpper = m_max - newLower + m_min;

    if (newLower != m_lower || newUpper != m_upper) {
        m_lower = value1;
        m_upper = value2;
        updateWidgets();
    }
}

/*!
 * Sets the limits that will be used for both the slider and the spinboxes.
 */
void RangeSliderBox::setLimits(double minValue, double maxValue) {
    double oldLower = m_lower;
    double oldUpper = m_upper;

    double d1 = m_lower - m_min;

    m_min = minValue;
    m_max = maxValue;

    m_lower = std::max(m_lower, m_min);
    m_upper = std::min(m_upper, m_max);

    if (m_mirrored) {
        d1 = std::min(d1, (m_max - m_min) / 2);
        m_lower = m_min + d1;
        m_upper = m_max - d1;
    }

    updateWidgets();

    if (oldLower != m_lower || oldUpper != m_upper) emit updateWidgets();
}

/*!
 * Updates the values and limits of the two QSpinBoxes and the RangeSlider.
 */
void RangeSliderBox::updateWidgets() {
    m_doubleSpinBox_lower->blockSignals(true);
    m_doubleSpinBox_upper->blockSignals(true);
    m_slider->blockSignals(true);

    m_doubleSpinBox_lower->setMinimum(m_min);
    m_doubleSpinBox_upper->setMaximum(m_max);

    if (m_mirrored) {
        m_doubleSpinBox_lower->setMaximum(0.5 * (m_min + m_max));
        m_doubleSpinBox_upper->setMinimum(0.5 * (m_min + m_max));
    } else {
        m_doubleSpinBox_lower->setMaximum(m_doubleSpinBox_upper->value());
        m_doubleSpinBox_upper->setMinimum(m_doubleSpinBox_lower->value());
    }

    util::updateDoubleSpinBoxValue(m_doubleSpinBox_lower, m_lower);
    util::updateDoubleSpinBoxValue(m_doubleSpinBox_upper, m_upper);

    double percentageLower = (m_lower - m_min) / (m_max - m_min);
    double percentageUpper = (m_upper - m_min) / (m_max - m_min);
    int sliderMin = m_slider->minimum();
    int sliderMax = m_slider->maximum();

    m_slider->setLowerValue(sliderMin + percentageLower * (sliderMax - sliderMin));
    m_slider->setUpperValue(sliderMin + percentageUpper * (sliderMax - sliderMin));

    m_doubleSpinBox_lower->blockSignals(false);
    m_doubleSpinBox_upper->blockSignals(false);
    m_slider->blockSignals(false);
}
