/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "frameselector.h"
#include "ui_frameselector.h"

#include <QDebug>

FrameSelector::FrameSelector(QWidget* parent) : QWidget(parent), ui(new Ui::FrameSelector) {
    ui->setupUi(this);
    frame = -1;
    frames = 0;
    ui->frameSelectorHS->setMinimum(0);
    ui->frameSelectorHS->setValue(0);
    ui->frameSelectorLE->blockSignals(true);
    ui->frameSelectorLE->setText("0");
    ui->frameSelectorLE->blockSignals(false);

    connect(ui->frameSelectorHS, &JumpSlider::valueChanged, this, &FrameSelector::update);
}

FrameSelector::~FrameSelector() {
    delete ui;
}

void FrameSelector::setNumberOfFrames(int count) {
    if (frames != count) {
        frames = count;
        ui->frameSelectorHS->setMaximum(frames - 1);
        ui->frameSelectorHS->setValue(frame);
        update();
        ui->frameSelectorLabel->setText("/" + QString::number(frames - 1));
    }
}

void FrameSelector::update() {
    int number = ui->frameSelectorHS->value();
    ui->frameSelectorLE->blockSignals(true);
    ui->frameSelectorLE->setText(QString::number(number));
    ui->frameSelectorLE->blockSignals(false);
    if (frame != number) {
        frame = number;
        emit selectionChanged(frame);
    }
}

void FrameSelector::setValue(int number) {
    frame = number;  // selectionChanged() will not be emitted
    ui->frameSelectorHS->setValue(number);
}

void FrameSelector::on_frameSelectorPrevPB_clicked() {
    if (ui->frameSelectorHS->value() > ui->frameSelectorHS->minimum())
        ui->frameSelectorHS->setValue(ui->frameSelectorHS->value() - 1);
}

void FrameSelector::on_frameSelectorNextPB_clicked() {
    if (ui->frameSelectorHS->value() < ui->frameSelectorHS->maximum())
        ui->frameSelectorHS->setValue(ui->frameSelectorHS->value() + 1);
}

/*!
 * Finds the closest frame and makes it active.
 */
void FrameSelector::on_frameSelectorLE_editingFinished() {
    int number = ui->frameSelectorLE->text().toInt();
    if (number < 0) number = 0;
    if (number > frames - 1) number = frames - 1;

    ui->frameSelectorHS->setValue(number);
    ui->frameSelectorLE->blockSignals(true);
    ui->frameSelectorLE->setText(QString::number(number));
    ui->frameSelectorLE->blockSignals(false);
}
