#include "primaries.h"

LinePrimary::LinePrimary(QVector3D e1, QVector3D e2, QVector4D c)
{
    end1 = e1;
    end2 = e2;
    color_m = c;
}

std::vector<LinePrimary> LinePrimary::lines()
{
    std::vector<LinePrimary> vec = {LinePrimary(end1, end2, color_m)};
}
