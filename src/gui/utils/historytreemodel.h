/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Follows mostly the guide in https://doc.qt.io/qt-5/qtwidgets-itemviews-simpletreemodel-example.html.
 */


#ifndef HISTORYTREEMODEL_H
#define HISTORYTREEMODEL_H

#include "api/history.h"
#include "gui/utils/treeitem.h"

#include <QAbstractItemModel>
#include <QVector>

/*!
 * \brief A tree model for showing the project history.
 *
 * Follows mostly the guide in https://doc.qt.io/qt-5/qtwidgets-itemviews-simpletreemodel-example.html. Only the
 * constructor has been modified to model the desired data.
 *
 * Contains the project entries, frame counts, and frames.
 */
class HistoryTreeModel : public QAbstractItemModel {
    Q_OBJECT

  public:
    HistoryTreeModel();

    explicit HistoryTreeModel(const History& history, QObject* parent = nullptr);
    ~HistoryTreeModel() override;

    QVariant data(const QModelIndex& index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    void addEntry(const Entry* entry);
    void removeEntry(int entryIndex);
    void addFrameToEntry(const QString& frameInfo, int entryIndex, int frameIndex);

  private:
    TreeItem* m_rootItem;

  private slots:
    void frameAddedToEntry(int entryIndex, int frameIndex);
};

#endif  // HISTORYTREEMODEL_H
