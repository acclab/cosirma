/*
 * Taken from https://doc.qt.io/qt-5/qtwidgets-itemviews-simpletreemodel-example.html
 */


#ifndef TREEITEM_H
#define TREEITEM_H

#include <QList>
#include <QVariant>

class TreeItem {
  public:
    explicit TreeItem(const QList<QVariant>& data, TreeItem* parent = nullptr);
    ~TreeItem();

    void appendChild(TreeItem* child);
    bool removeChildren(int position, int count);

    TreeItem* child(int row);
    int childCount() const;
    int columnCount() const;

    QVariant data(int column) const;
    bool setData(int column, const QVariant& value);

    int row() const;
    TreeItem* parent();

  private:
    QList<TreeItem*> m_childItems;
    QList<QVariant> m_itemData;
    TreeItem* m_parentItem;
};

#endif  // TREEITEM_H
