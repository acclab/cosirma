/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "entryeditor.h"

#include "api/analysis/cascadelocationpredictor.h"
#include "api/analysis/clusteranalyzer.h"
#include "api/project.h"

#include <functional>
#include <glm/vec3.hpp>


/*!
 * \brief Constructs an object without data. Before the object can be used, an entry has to be set using setEntry().
 * \param parent Parent object
 */
EntryEditor::EntryEditor(QObject* parent) : QObject(parent) {}

/*!
 * \brief Edits the entry by applying \a action on it.
 * \param action The action to apply in the entry
 */
void EntryEditor::edit(const EntryEditor::Action& action) {
    action.act(m_entry);
    if (action.atomsChanged()) {
        m_atomsChanged = true;
        emit atomsChanged();
    }
    if (action.typesChanged()) {
        m_typesChanged = true;
        emit typesChanged();
    }
    if (action.cellLimsChanged()) {
        m_cellLimsChanged = true;
        emit cellLimsChanged();
    }
    if (action.periodicBoundariesChanged()) {
        m_periodicBoundariesChanged = true;
        emit periodicBoundariesChanged();
    }
    if (action.atomsChanged() || action.typesChanged() || action.cellLimsChanged() ||
        action.periodicBoundariesChanged()) {
        emit changed(true);
    }
}

/*!
 * \brief Sets the entry that the modification actions will be applied on.
 * \param entry ExtendedEntryData object to be modified
 */
void EntryEditor::setEntry(const ExtendedEntryData& entry) {
    qDebug() << this << "::setEntry()";
    qDebug() << "    " << entry.frame.atoms.count();
    //    discardChanges();            // Discard the previous changes before updating the entry.
    m_unmodifiedEntry = &entry;  // Reference to the new object.
    m_entry = entry;             // copy of the new object.
    emit entryChanged();
    emit changed(false);
}

/*!
 * \brief Accesses the modified entry
 * \return const reference to the modified entry
 */
const ExtendedEntryData& EntryEditor::modifiedEntry() const {
    return m_entry;
}

/*!
 * \brief Reverts the changes made to the modified entry.
 *
 * Note this class only stores a reference (pointer) to the ExtendedEntryData given with setEntry(). Do not remove the
 * real object!
 */
void EntryEditor::discardChanges() {
    if (m_unmodifiedEntry) {
        m_entry = *m_unmodifiedEntry;
    } else {
        m_entry = ExtendedEntryData();
    }

    bool reverted = false;
    if (m_atomsChanged) {
        m_atomsChanged = false;
        emit atomsChanged();
        reverted = true;
    }
    if (m_typesChanged) {
        m_typesChanged = false;
        emit typesChanged();
        reverted = true;
    }
    if (m_cellLimsChanged) {
        m_cellLimsChanged = false;
        emit cellLimsChanged();
        reverted = true;
    }
    if (m_periodicBoundariesChanged) {
        m_periodicBoundariesChanged = false;
        emit periodicBoundariesChanged();
        reverted = true;
    }

    if (reverted) emit changed(false);
}


/*!
 * \brief Adds the modified entry to the project history.
 * \sa Project, History
 */
ExtendedEntryData* EntryEditor::saveToHistory() {
    EntryInfo info;
    info.type = EntryInfo::Modification;
    info.description = "Cell Modified";
    m_entry.data.info = info;

    Entry* entry = Project::getInstance().history.addNewEntry(m_entry);

    emit changed(false);

    //! \todo: possible memory leak...
    return new ExtendedEntryData(entry->extendedData());
}

void EntryEditor::saveEntryData(const QString& path) {
    m_entry.frame.write(path);
}


// Actions

/*!
 * \brief Constructs an action with
 * \param selection Selection of atoms that the action will affect
 */
EntryEditor::Action::Action(const AbstractAtomSelection* selection) : m_selection(selection) {}

EntryEditor::Action::~Action() {}

/*!
 * \brief Function for checking the change status of the atoms after acting on an entry
 * \return \c true if XyzData has changed (excluding cell limits)
 */
bool EntryEditor::Action::atomsChanged() const {
    return m_atomsChanged;
}

/*!
 * \brief Function for checking the change status of the atom types after acting on an entry
 * \return \c true if TypeData has changed
 */
bool EntryEditor::Action::typesChanged() const {
    return m_typesChanged;
}

/*!
 * \brief Function for checking the change status of the cell limits after acting on an entry
 * \return \c true if the cell limits have changed
 */
bool EntryEditor::Action::cellLimsChanged() const {
    return m_cellLimsChanged;
}

bool EntryEditor::Action::periodicBoundariesChanged() const {
    return m_periodicBoundariesChanged;
}

/*!
 * \brief Sets the atom selection that the action will affect
 * \param selection Selection of atoms that the action will affect
 */
void EntryEditor::Action::setAtomSelector(const AbstractAtomSelection& selection) {
    m_selection = &selection;
}


/*!
 * \brief Constructs an object that will merge the selected atoms to the specified atom type.
 * \param targetType Atom type that determines the type number and element symbol that the modified atoms will have.
 * \param conserveFix If \c true, the fix of the modified atoms is conserved (i.e. the sign of the type number remain
 * the same)
 * \param selection Selection of atoms that the action will affect
 */
EntryEditor::MergeToTypeAction::MergeToTypeAction(const AtomType& targetType, bool conserveFix,
                                                  const AbstractAtomSelection* selection)
        : Action(selection), m_targetType(targetType), m_conserveFix(conserveFix) {}

/*!
 * \brief Changes the type attributes of the selected atoms in \a entry
 * \param entry ExtendedEntryData object being modified
 */
void EntryEditor::MergeToTypeAction::act(ExtendedEntryData& entry) const {
    m_atomsChanged = false;
    if (m_selection) {
        for (Atom& atom : entry.frame.atoms) {
            if (m_selection->atomInSelection(atom)) {
                atom.type = ((m_conserveFix && (atom.type < 0)) ? -1 : 1) * m_targetType.typeNumber;
                atom.symbol = m_targetType.symbol;
                m_atomsChanged = true;
            }
        }
    }

    if (m_atomsChanged) {
        entry.frame.updateAmounts();
    }
}


/*!
 * \brief Constructs an object that will change the type \a fromType into \a targetType
 * \param fromType The number of the type that will be edited
 * \param targetType The target type attributes
 */
EntryEditor::TypeEditAction::TypeEditAction(int fromType, const AtomType& targetType)
        : m_fromType(fromType), m_targetType(targetType) {}

/*!
 * \brief Changes the attributes of the specified type in \a entry.
 *
 * All the atoms that belong to the type will also be affected.
 *
 * \param entry ExtendedEntryData object being modified
 */
void EntryEditor::TypeEditAction::act(ExtendedEntryData& entry) const {
    m_atomsChanged = false;
    m_typesChanged = false;

    AtomType oldType = entry.data.types.typeList.value(m_fromType);
    if (m_targetType == oldType) return;

    // Modify the types
    m_typesChanged = true;
    entry.data.types.typeList.insert(m_targetType.typeNumber, m_targetType);
    if (oldType.typeNumber != m_targetType.typeNumber) {
        entry.data.types.typeList.remove(oldType.typeNumber);
    }

    // Modify the atoms
    if (oldType.typeNumber != m_targetType.typeNumber || oldType.symbol != m_targetType.symbol) {
        ArrayAtomSelection s;
        TypeSelection freeS(m_fromType);
        TypeSelection fixedS(-m_fromType);
        s.addExternalSelection(&freeS);
        s.addExternalSelection(&fixedS);
        MergeToTypeAction atomAction(m_targetType, true, &s);
        atomAction.act(entry);
        m_atomsChanged = atomAction.atomsChanged();
    }
}


/*!
 * \brief Constructs an Action obkect that will delete the type \a typeNumber
 * \param typeNumber The number of the type that will be deleted
 */
EntryEditor::TypeDeleteAction::TypeDeleteAction(int typeNumber) : m_typeNumber(typeNumber) {}

/*!
 * \brief Deletes the specified atom type from \a entry. If there are atoms with this type number, they will also be
 * deleted.
 * \param entry ExtendedEntryData object being modified
 */
void EntryEditor::TypeDeleteAction::act(ExtendedEntryData& entry) const {
    m_atomsChanged = false;
    m_typesChanged = false;

    if (entry.data.types.typeList.contains(m_typeNumber)) {
        entry.data.types.typeList.remove(m_typeNumber);
        m_typesChanged = true;
    }

    if (entry.frame.amounts.value(m_typeNumber) + entry.frame.amounts.value(-m_typeNumber) > 0) {
        ArrayAtomSelection s;
        TypeSelection freeS(m_typeNumber);
        TypeSelection fixedS(-m_typeNumber);
        s.addExternalSelection(&freeS);
        s.addExternalSelection(&fixedS);
        AtomDeleteAction deleteAction(&s);
        deleteAction.act(entry);
        m_atomsChanged = deleteAction.atomsChanged();
        m_cellLimsChanged = deleteAction.cellLimsChanged();
        if (deleteAction.typesChanged()) m_typesChanged = true;
    }
}


/*!
 * \brief Constructs an object that will add \a type to an entry
 * \param type The new type
 */
EntryEditor::TypeAddAction::TypeAddAction(const AtomType& type) : m_type(type) {}

/*!
 * \brief Adds the specified atom type to \a entry if it does not already have a type with the same type number.
 * \param entry ExtendedEntryData object being modified
 */
void EntryEditor::TypeAddAction::act(ExtendedEntryData& entry) const {
    m_typesChanged = false;
    if (!entry.data.types.typeList.contains(m_type.typeNumber) &&
        entry.data.types.typeList.value(m_type.typeNumber) != m_type) {
        entry.data.types.typeList.insert(m_type.typeNumber, m_type);
        m_typesChanged = true;
    }
}


/*!
 * \brief Constructs an object that will delete the selected atoms.
 * \param selection Selection of atoms that the action will affect
 */
EntryEditor::AtomDeleteAction::AtomDeleteAction(const AbstractAtomSelection* selection) : Action(selection) {}

/*!
 * \brief Deletes the selected atoms from \a entry
 * \param entry ExtendedEntryData object being modified
 */
void EntryEditor::AtomDeleteAction::act(ExtendedEntryData& entry) const {
    m_atomsChanged = false;
    if (m_selection) {
        QVector<Atom> newAtoms;
        for (Atom& atom : entry.frame.atoms) {
            if (!m_selection->atomInSelection(atom)) newAtoms.push_back(atom);
        }
        if (newAtoms.count() != entry.frame.atoms.count()) {
            m_atomsChanged = true;
            entry.frame.atoms = newAtoms;
            entry.frame.updateAmounts();
            entry.frame.updateAtomLims();
        }
    }
}


/*!
 * \brief Constructs an object that will fix or unfix selected atoms.
 * \param fix If \c true, the selected atoms will be fixed. If \c false, they will be unfixed.
 * \param selection Selection of atoms that the action will affect
 */
EntryEditor::AtomFixAction::AtomFixAction(bool fix, const AbstractAtomSelection* selection)
        : Action(selection), m_fix(fix) {}

/*!
 * \brief Based on the class construction, either fixes or unfixes the selected atoms in \a entry. If the atoms are
 * fixed, all the atom types in the selection will be non-positive. The opposite is true for unfixing.
 * \param entry ExtendedEntryData object being modified
 */
void EntryEditor::AtomFixAction::act(ExtendedEntryData& entry) const {
    m_atomsChanged = false;
    if (m_selection) {
        for (Atom& atom : entry.frame.atoms) {
            if (m_selection->atomInSelection(atom)) {
                atom.type = (m_fix ? -1 : 1) * std::abs(atom.type);
                m_atomsChanged = true;
            }
        }
        if (m_atomsChanged) entry.frame.updateAmounts();
    }
}


/*!
 * \brief Constructs an object that will change the cell limits of an entry to \a cellLims
 * \param cellLims New cell limits
 */
EntryEditor::CellLimitsEditAction::CellLimitsEditAction(const Box& cellLims) : m_cellLims(cellLims) {}

/*!
 * \brief Changes the cell limits of \a entry
 * \param entry ExtendedEntryData object being modified
 */
void EntryEditor::CellLimitsEditAction::act(ExtendedEntryData& entry) const {
    m_cellLimsChanged = false;
    if (entry.frame.cellLims != m_cellLims) {
        entry.frame.cellLims = m_cellLims;
        m_cellLimsChanged = true;
    }
}


EntryEditor::PeriodicBoundaryEditAction::PeriodicBoundaryEditAction(const glm::bvec3& pbc) : m_pbc(pbc) {}

void EntryEditor::PeriodicBoundaryEditAction::act(ExtendedEntryData& entry) const {
    m_periodicBoundariesChanged = false;
    if (entry.frame.pbc != m_pbc) {
        entry.frame.pbc = m_pbc;
        m_periodicBoundariesChanged = true;
    }
}


/*!
 * \brief Constructs an object that will rotate the cell limits and the atoms of an entry.
 * \param r Determines the angle and axis of the rotation
 */
EntryEditor::CellRotationAction::CellRotationAction(EntryEditor::CellRotationAction::Rotation r) : m_rotation(r) {}

/*!
 * \brief Rotates the cell limits and atoms (positions and velocities) around a coordinate axis by a 90 degree angle.
 * \param entry ExtendedEntryData object being modified
 */
void EntryEditor::CellRotationAction::act(ExtendedEntryData& entry) const {
    m_atomsChanged = false;
    m_cellLimsChanged = false;

    std::function<void(glm::dvec3&)> rotateVector = [](glm::dvec3&) {};
    std::function<void(Box&)> rotateBox = [](Box&) {};

    switch (m_rotation) {
        case xClockwise:
            rotateVector = [](glm::dvec3& v) {
                std::swap(v.y, v.z);
                v.z = -v.z;
            };
            rotateBox = [](Box& b) { b = Box(b.x1, b.x2, b.z1, b.z2, -b.y2, -b.y1); };
            break;
        case xCounterClockwise:
            rotateVector = [](glm::dvec3& v) {
                std::swap(v.y, v.z);
                v.y = -v.y;
            };
            rotateBox = [](Box& b) { b = Box(b.x1, b.x2, -b.z2, -b.z1, b.y1, b.y2); };
            break;
        case yClockwise:
            rotateVector = [](glm::dvec3& v) {
                std::swap(v.x, v.z);
                v.x = -v.x;
            };
            rotateBox = [](Box& b) { b = Box(-b.z2, -b.z1, b.y1, b.y2, b.x1, b.x2); };
            break;
        case yCounterClockwise:
            rotateVector = [](glm::dvec3& v) {
                std::swap(v.x, v.z);
                v.z = -v.z;
            };
            rotateBox = [](Box& b) { b = Box(b.z1, b.z2, b.y1, b.y2, -b.x2, -b.x1); };
            break;
        case zClockwise:
            rotateVector = [](glm::dvec3& v) {
                std::swap(v.x, v.y);
                v.y = -v.y;
            };
            rotateBox = [](Box& b) { b = Box(b.y1, b.y2, -b.x2, -b.x1, b.z1, b.z2); };
            break;
        case zCounterClockwise:
            rotateVector = [](glm::dvec3& v) {
                std::swap(v.x, v.y);
                v.x = -v.x;
            };
            rotateBox = [](Box& b) { b = Box(-b.y2, -b.y1, b.x1, b.x2, b.z1, b.z2); };
            break;
    }

    for (Atom& a : entry.frame.atoms) {
        rotateVector(a.position);
        rotateVector(a.velocity);
    }

    rotateBox(entry.frame.cellLims);
    rotateBox(entry.frame.atomLims);

    m_atomsChanged = true;
    m_cellLimsChanged = true;
}


EntryEditor::ShiftCellAction::ShiftCellAction(const glm::dvec3& shift) : m_shift(shift) {}

void EntryEditor::ShiftCellAction::act(ExtendedEntryData& entry) const {

    for (Atom& atom : entry.frame.atoms) {
        atom.position.x += m_shift.x;
        atom.position.y += m_shift.y;
        atom.position.z += m_shift.z;

        // Handle periodic boundaries
        if (entry.frame.pbc.x) {
            while (atom.position.x > entry.frame.cellLims.x2) atom.position.x -= entry.frame.cellLims.dx();
            while (atom.position.x <= entry.frame.cellLims.x1) atom.position.x += entry.frame.cellLims.dx();
        }
        if (entry.frame.pbc.y) {
            while (atom.position.y > entry.frame.cellLims.y2) atom.position.y -= entry.frame.cellLims.dy();
            while (atom.position.y <= entry.frame.cellLims.y1) atom.position.y += entry.frame.cellLims.dy();
        }
        if (entry.frame.pbc.z) {
            while (atom.position.z > entry.frame.cellLims.z2) atom.position.z -= entry.frame.cellLims.dz();
            while (atom.position.z <= entry.frame.cellLims.z1) atom.position.z += entry.frame.cellLims.dz();
        }

        m_atomsChanged = true;
    }
    if (m_atomsChanged) {
        entry.frame.updateAmounts();
        entry.frame.updateAtomLims();
    }
}


EntryEditor::RemoveSputteredAction::RemoveSputteredAction(double cutoff) : m_cutoff(cutoff) {}

void EntryEditor::RemoveSputteredAction::act(ExtendedEntryData& entry) const {
    ClusterAnalyzer analyzer(entry, entry.frame.pbc);
    analyzer.analyze(m_cutoff);
    XyzData frame = analyzer.largestCluster();
    m_atomsChanged = false;
    m_cellLimsChanged = false;
    if (frame.atoms.size() != entry.frame.atoms.size()) {
        entry.frame = frame;
        m_atomsChanged = true;
        m_cellLimsChanged = true;
    }
}


EntryEditor::CenterCascadeAction::CenterCascadeAction(IonTransformData* ionTransform, double cutoff, int recursions)
        : m_ionTransform(ionTransform), m_cutoff(cutoff), m_recursions(recursions) {
    m_shiftOffset = new glm::dvec3(0, 0, 0);
}

EntryEditor::CenterCascadeAction::~CenterCascadeAction() {
    delete m_shiftOffset;
}

void EntryEditor::CenterCascadeAction::act(ExtendedEntryData& entry) const {
    CascadeLocationPredictor predictor(entry);
    glm::dvec3 cascadePosition =
        predictor.predict(m_ionTransform->position, m_ionTransform->theta, m_ionTransform->phi, m_cutoff, m_recursions);

    glm::dvec3 shiftVector;
    if (entry.frame.pbc.x) shiftVector.x = -cascadePosition.x;
    if (entry.frame.pbc.y) shiftVector.y = -cascadePosition.y;
    if (entry.frame.pbc.z) shiftVector.z = -cascadePosition.z;

    m_shiftOffset->x = shiftVector.x;
    m_shiftOffset->y = shiftVector.y;
    m_shiftOffset->z = shiftVector.z;

    if (shiftVector != glm::dvec3(0, 0, 0)) {
        ShiftCellAction shift(shiftVector);
        shift.act(entry);

        // Shift the ion so that the cascade will end up in the middle of the cell.
        m_ionTransform->position += shiftVector;
        // Handle periodic boundaries
        if (entry.frame.pbc.x) {
            while (m_ionTransform->position.x > entry.frame.cellLims.x2)
                m_ionTransform->position.x -= entry.frame.cellLims.dx();
            while (m_ionTransform->position.x <= entry.frame.cellLims.x1)
                m_ionTransform->position.x += entry.frame.cellLims.dx();
        }
        if (entry.frame.pbc.y) {
            while (m_ionTransform->position.y > entry.frame.cellLims.y2)
                m_ionTransform->position.y -= entry.frame.cellLims.dy();
            while (m_ionTransform->position.y <= entry.frame.cellLims.y1)
                m_ionTransform->position.y += entry.frame.cellLims.dy();
        }
        if (entry.frame.pbc.z) {
            while (m_ionTransform->position.z > entry.frame.cellLims.z2)
                m_ionTransform->position.z -= entry.frame.cellLims.dz();
            while (m_ionTransform->position.z <= entry.frame.cellLims.z1)
                m_ionTransform->position.z += entry.frame.cellLims.dz();
        }
    }
}

glm::dvec3 EntryEditor::CenterCascadeAction::shiftOffset() {
    return *m_shiftOffset;
}


/*!
 * \brief Constructs an empty array.
 */
EntryEditor::ArrayAction::ArrayAction() {}


/*!
 * \brief Applies all the added actions on \a entry. Modification statuses of the actions are combined.
 * \param entry ExtendedEntryData object being modified
 */
void EntryEditor::ArrayAction::act(ExtendedEntryData& entry) const {
    m_atomsChanged = false;
    m_typesChanged = false;
    m_cellLimsChanged = false;
    m_periodicBoundariesChanged = false;
    for (const Action* action : m_actions) {
        action->act(entry);
        if (action->atomsChanged()) m_atomsChanged = true;
        if (action->typesChanged()) m_typesChanged = true;
        if (action->cellLimsChanged()) m_cellLimsChanged = true;
        if (action->periodicBoundariesChanged()) m_periodicBoundariesChanged = true;
    }
}

/*!
 * \brief Adds \a action to the array.
 * \param action Action to be added
 */
void EntryEditor::ArrayAction::addAction(const EntryEditor::Action* action) {
    m_actions.push_back(action);
}
