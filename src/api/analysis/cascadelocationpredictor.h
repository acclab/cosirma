/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CASCADELOCATIONPREDICTOR_H
#define CASCADELOCATIONPREDICTOR_H

#include "api/entry.h"


class CascadeLocationPredictor {
  public:
    CascadeLocationPredictor(const ExtendedEntryData& entry);

    glm::dvec3 predict(glm::dvec3 ionPosition, double theta, double phi, double cutoff = 3.0, int recursions = 5);

  private:
    struct Intersection {
        Intersection(double _distance, bool _periodic, const glm::dvec3& _shift) {
            distance = _distance;
            periodic = _periodic;
            shift = _shift;
        }
        double distance;
        bool periodic;
        glm::dvec3 shift;
    };

    std::pair<bool, glm::dvec3> trace(const glm::dvec3& position, const glm::dvec3& direction, double cutoffSquared,
                                      int recursions);

    const ExtendedEntryData& m_entry;
};

#endif  // CASCADELOCATIONPREDICTOR_H
