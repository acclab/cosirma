/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef POTENTIALLIST_H
#define POTENTIALLIST_H

#include "potential.h"

#include <QVector>


/*!
 * \brief A singleton containing the list of available potentials
 *
 * The public interface is accessed through getInstance().
 *
 * \sa Potential
 *
 * \todo Add more potentials
 */
class PotentialList {

  public:
    static PotentialList& getInstance();

    QList<Potential> getPotentials(const QString& element1, const QString& element2) const;
    const QMap<int, Potential>& potentials() const;
    QList<Potential> filteredPotentials(const QStringList& elementTypes) const;
    Potential getPotential(int potmode) const;

  private:
    PotentialList();

    void addPotential(const Potential&);
    void addPotential(int newPotmode, int oldPotmode, const QString& info = "");

    QMap<int, Potential> m_potentials;
};

#endif  // POTENTIALLIST_H
