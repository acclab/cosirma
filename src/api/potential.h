/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef POTENTIAL_H
#define POTENTIAL_H

#include <QSet>
#include <QString>
#include <QStringList>
#include <QVariant>


/*!
 * \brief PARCAS potential
 *
 * In MD potential defines the interactions between atoms. PARCAS has many built-in potentials, such as EAM, Tersoff,
 * and Watanabe potentials. In PARCAS, the potential is selected with "potmode" integer parameter. Some potentials need
 * different kinds of input files.
 *
 * This class stores information on a potential, such as its name, "potmode" value, and the atom elements that it can be
 * used on.
 *
 * \todo An option for supporting all elements should be added.
 */
class Potential {
  public:
    struct AtomPair {

        AtomPair(QString e1, QString e2, QString info = "") {
            element1 = e1;
            element2 = e2;
            information = info;
        }

        QString element1;
        QString element2;
        QString information;

        /*!
         * \brief operator ==
         *
         * The AtomPairs are the same if both element types exists in both of the AtomPairs.
         * e.g. AtomPair(Si, Ge) == AtomPair(Si, Ge) or AtomPair(Si, Ge) == AtomPair(Ge, Si) are both valid
         *
         * \param other
         * \return
         */
        bool operator==(const AtomPair& other) const {
            return ((element1 == other.element1 && element2 == other.element2) ||
                    (element1 == other.element2 && element2 == other.element1));
        }
    };

    /*!
     * \brief The potential file enum.
     *
     * Tells what kinds of input files PARCAS needs for the potential.
     */
    enum InputType {
        NoFile,  //!< \brief No inputfiles required
        Reppot,  //!< \brief All interacting element pairs require reppot files.
        EAM      //!< \brief All interacting element pairs require eam files.
    };

    enum Formalism {
        StillingerWeber,  //!< \brief Stillinger-Weber type potentials
        Tersoff,          //!< \brief Tersoff type potentials
        Other             //!< \brief Default
    };

    Potential(const QString& name = "", int mode = 0, const QList<AtomPair>& supportedAtomPairs = QList<AtomPair>(),
              Formalism formalism = Formalism::Other, InputType file = NoFile, const QString& information = "");

    Potential(int mode, const Potential& other, const QString& info = "");

    bool supportsPair(const QString& element1, const QString& element2) const;

    const QString& name() const;
    int mode() const;
    const QSet<QString>& supportedElements() const;
    const QString& information() const;
    QList<AtomPair> pairs();

    InputType fileType() const;
    Formalism formalism() const;

  private:
    QList<AtomPair> m_pairs;
    QSet<QString> m_elements;
    int m_potmode;
    QString m_potentialName;
    QString m_info;
    Formalism m_formalism;
    InputType m_potFile;
};

Q_DECLARE_METATYPE(Potential)

#endif  // POTENTIAL_H
