/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "util.h"

#include <QDir>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <cmath>

namespace util {

/*!
 * \brief Copys a directory recursively
 * \param srcPath The source path
 * \param destPath The destination path
 * \return \c true if the copying is successful
 */
bool copyDirectory(const QString& srcPath, const QString& destPath) {
    QDir destDir(destPath);
    if (destDir.exists()) {
        return false;
    }
    destDir.mkpath(destPath);

    QDir srcDir(srcPath);
    QStringList fileNames = srcDir.entryList(QDir::Files | QDir::Hidden);
    foreach (const QString& fileName, fileNames) {
        const QString newSrcPath = srcPath + "/" + fileName;
        const QString newDestPath = destPath + "/" + fileName;
        if (!QFile::copy(newSrcPath, newDestPath)) {
            return false;
        }
    }

    QStringList dirNames = srcDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    foreach (const QString& dirName, dirNames) {
        const QString newSrcPath = srcPath + "/" + dirName;
        const QString newDestPath = destPath + "/" + dirName;
        if (!copyDirectory(newSrcPath, newDestPath)) {
            return false;
        }
    }

    return true;
}

/*!
 * \brief Updates the double-spinbox value value.
 *
 * A helper function for setting the QDoubleSpinBox value. If the spin box is in focus and the new value is close enough
 * to the old value for the displayed number to not change, the value is not updated. This function can be used to fix
 * the problem where the rest of the spin box is filled with zeros while editing.
 *
 * \param sb The updated spinbox
 * \param value The new value
 * \return \c true if the value was updated
 */
bool updateDoubleSpinBoxValue(QDoubleSpinBox* sb, double value) {
    if (sb->hasFocus() && (std::abs(sb->value() - value) < std::min(1e-10, std::pow(10, -sb->decimals())))) {
        return false;
    }
    sb->setValue(value);
    return true;
}

/*!
 * \brief Transforms color vector to QColor
 *
 * The rgba values are scaled from the range 0-1 to 0-255 and transformed into a QColor object.
 *
 * \param v The rgba vector
 * \return QColor object
 */
QColor vectorToColor(const glm::vec4& v) {
    glm::ivec4 scaled = v * 255.f;
    return QColor(scaled.x, scaled.y, scaled.z, scaled.w);
}

/*!
 * \brief Transforms QColor to color vector
 *
 * The rgba values are scaled from the range 0-55 to 0-1 and transformed into a rgba vector.
 *
 * \param c The Qcolor object
 * \return rgba vector with values in range 0-1
 */
glm::vec4 colorToVector(const QColor& c) {
    return glm::vec4(c.redF(), c.greenF(), c.blueF(), c.alphaF());
}


void decorateSplitterHandle(QSplitter* splitter, int index) {
    const int gripLength = 25;
    const int gripWidth = 1;
    const int grips = 3;

    //    splitter->setOpaqueResize(false);
    //    splitter->setChildrenCollapsible(false);

    splitter->setHandleWidth(7);
    QSplitterHandle* handle = splitter->handle(index);
    Qt::Orientation orientation = splitter->orientation();
    QHBoxLayout* layout = new QHBoxLayout(handle);
    layout->setSpacing(0);
    layout->setMargin(0);

    if (orientation == Qt::Horizontal) {
        for (int i = 0; i < grips; ++i) {
            QFrame* line = new QFrame(handle);
            line->setMinimumSize(gripWidth, gripLength);
            line->setMaximumSize(gripWidth, gripLength);
            line->setFrameShape(QFrame::StyledPanel);
            layout->addWidget(line);
        }
    } else {
        // this will center the vertical grip
        // add a horizontal spacer
        layout->addStretch();
        // create the vertical grip
        QVBoxLayout* vbox = new QVBoxLayout;
        for (int i = 0; i < grips; ++i) {
            QFrame* line = new QFrame(handle);
            line->setMinimumSize(gripLength, gripWidth);
            line->setMaximumSize(gripLength, gripWidth);
            line->setFrameShape(QFrame::StyledPanel);
            vbox->addWidget(line);
        }
        layout->addLayout(vbox);
        // add another horizontal spacer
        layout->addStretch();
    }
}

}  // namespace util
