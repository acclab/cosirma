/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "framereaderwriter.h"

#include "binarysettingsio.h"
#include "programsettings.h"

#include <QDateTime>
#include <QDir>
#include <QFile>


/*!
 * \brief Constructs an object that is connected to a directory.
 * \param dirPath Path to the directory
 */
FrameReaderWriter::FrameReaderWriter(const QString& dirPath) {
    m_path = dirPath;
}

/*!
 * \brief Writes the frame.
 *
 * \a xyzData is written into two files: binary and text. If the frame directory already exists, the writing fails.
 *
 * \param xyzData The XyzData that is written
 * \return \c true if the writing is successful
 */
bool FrameReaderWriter::write(const XyzData& xyzData) {
    if (m_path.isEmpty()) return false;
    QDir frameDir(m_path);
    if (frameDir.exists()) return false;
    if (!frameDir.mkpath(m_path)) return false;
    if (xyzData.write(m_path + m_xyzPath)) {
        if (ProgramSettings::getInstance().locationSettings().saveBinaryFrames) {
            BinarySettingsIO(m_path + m_binaryPath).save(xyzData);
        }
        return true;
    }

    frameDir.removeRecursively();
    return false;
}

/*!
 * \brief Reads the XyzData from the connected directory.
 *
 * If the text file has not been modified after the binary file, tries to read the binary file. If that fails, the text
 * file is read instead, and a corresponding binary file is created.
 *
 * \param The XyzData object that the data is read into.
 * \return \c true if the reading is successful
 */
bool FrameReaderWriter::read(XyzData& xyzData) {
    if (m_path.isEmpty()) return false;
    QFileInfo xyzInfo = QFileInfo(m_path + m_xyzPath);
    QFileInfo binaryInfo = QFileInfo(m_path + m_binaryPath);

    bool readBinary = false;
    if (!xyzInfo.exists() && !binaryInfo.exists()) {
        return false;
    } else if (!binaryInfo.exists()) {
        readBinary = false;
    } else if (!xyzInfo.exists()) {
        readBinary = true;
    } else {
        readBinary = QFileInfo(m_path + m_xyzPath).lastModified() < QFileInfo(m_path + m_binaryPath).lastModified();
    }

    BinarySettingsIO io(m_path + m_binaryPath);
    if (readBinary && io.load(xyzData) && xyzData.atoms.size() > 0) {
        return true;
    }

    if (xyzData.read(m_path + m_xyzPath)) {
        if (ProgramSettings::getInstance().locationSettings().saveBinaryFrames) io.save(xyzData);
        return true;
    }

    xyzData = XyzData();
    return false;
}
