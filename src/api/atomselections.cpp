/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "atomselections.h"


// AbstractAtomSelection

AbstractAtomSelection::~AbstractAtomSelection() {}


// AllAtomSelection

/*!
 * \brief A function that returns \c true for all atoms
 * \return Always \c true
 */
bool AllAtomSelection::atomInSelection(const Atom&) const {
    return true;
}


// NullAtomSelection

/*!
 * \brief A function that returns \c false for all atoms
 * \return Always \c false
 */
bool NullAtomSelection::atomInSelection(const Atom&) const {
    return false;
}


// InverseAtomSelection

/*!
 * Constructs InverseAtomSelection with underlying atom selection.
 * \param selection Selection to be inverted
 */
InverseAtomSelection::InverseAtomSelection(const AbstractAtomSelection* selection) : m_selection(selection) {}

/*!
 * \brief Determines if the \a atom is in the selection.
 * \param atom An atom
 * \return \c true if the underlying selection does not include \a atom
 */
bool InverseAtomSelection::atomInSelection(const Atom& atom) const {
    return !m_selection->atomInSelection(atom);
}

/*!
 * \brief Sets the underlying atom selection.
 * \param selection Selection to be inverted
 */
void InverseAtomSelection::setAtomSelection(const AbstractAtomSelection* selection) {
    m_selection = selection;
}


// ArrayAtomSelection

/*!
 * \brief Constructs an empty ArrayAtomSelection object.
 * \param mode Combination mode
 */
ArrayAtomSelection::ArrayAtomSelection(ArrayAtomSelection::Mode mode) : m_mode(mode) {}

ArrayAtomSelection::~ArrayAtomSelection() {
    clear();
}

/*!
 * \brief Determines if the \a atom is in the selection.
 * \param atom An atom
 * \return If combination mode \c Union is active, returns \c true if one of the added atom selections contains the \a
 * atom. If \c Intersection is active, returns \c true if all added atom selections contain the \a atom.
 */
bool ArrayAtomSelection::atomInSelection(const Atom& atom) const {
    for (const AbstractAtomSelection* s : m_selections)
        if (s->atomInSelection(atom)) {
            if (m_mode == Union) return true;
        } else {
            if (m_mode == Intersection) return false;
        }
    return m_mode != Union;
}

/*!
 * \brief Adds \a selection to the array.
 * \param selection The atom selection to be added
 */
void ArrayAtomSelection::addExternalSelection(const AbstractAtomSelection* selection) {
    m_selections.push_back(selection);
}

/*!
 * \brief Adds \a selection to the array and transfers the ownership of the selection.
 * \param selection The atom selection to be added
 */
void ArrayAtomSelection::addInternalSelection(AbstractAtomSelection* selection) {
    m_ownedSelections.push_back(selection);
    m_selections.push_back(selection);
}

/*!
 * \brief Sets the combination mode of the array.
 * \param mode Combination mode
 * \sa Mode
 */
void ArrayAtomSelection::setMode(ArrayAtomSelection::Mode mode) {
    m_mode = mode;
}

/*!
 * \brief Clears the added atom selections from the array. Selections that were added using addInternalSelection() are
 * deleted.
 */
void ArrayAtomSelection::clear() {
    m_selections.clear();
    for (AbstractAtomSelection* s : m_ownedSelections) delete s;
    m_ownedSelections.clear();
}


// BoxAtomSelection

/*!
 * \brief Constructs an object based on \a box geometry.
 * \param box The geometry that defines the selection
 */
BoxAtomSelection::BoxAtomSelection(const Box& box) : m_box(box) {}

/*!
 * \brief Determines if the \a atom is in the selection.
 * \param atom An atom
 * \return \c true if the coordinate vector of the \a atom is inside the box
 */
bool BoxAtomSelection::atomInSelection(const Atom& atom) const {
    bool insideX = ((atom.position.x >= m_box.x1) && (atom.position.x <= m_box.x2));
    bool insideY = ((atom.position.y >= m_box.y1) && (atom.position.y <= m_box.y2));
    bool insideZ = ((atom.position.z >= m_box.z1) && (atom.position.z <= m_box.z2));

    return (insideX && insideY && insideZ);
}


// SphereAtomSelection

/*!
 * \brief Constructs an object based on \a sphere geometry.
 * \param sphere The geometry that defines the selection
 */
SphereAtomSelection::SphereAtomSelection(const Sphere& sphere) {
    m_sphere = sphere;
    m_r2 = sphere.radius * sphere.radius;
}

/*!
 * \brief Determines if the \a atom is in the selection.
 * \param atom An atom
 * \return \c true if the coordinate vector of the \a atom is inside the sphere.
 */
bool SphereAtomSelection::atomInSelection(const Atom& atom) const {
    glm::dvec3 d = atom.position - m_sphere.center;
    return glm::length2(d) < m_r2;
}


// CylinderAtomSelection

/*!
 * \brief Constructs an object based on \a cylinder geometry.
 * \param cylinder The geometry that defines the selection
 */
CylinderAtomSelection::CylinderAtomSelection(const Cylinder& cylinder) {
    m_cylinder = cylinder;
    m_normedDirection = glm::normalize(cylinder.axis);
    m_r2 = cylinder.radius * cylinder.radius;
}

/*!
 * \brief Determines if the \a atom is in the selection.
 * \param atom An atom
 * \return \c true if the coordinate vector of the \a atom is inside the cylinder.
 */
bool CylinderAtomSelection::atomInSelection(const Atom& atom) const {
    glm::dvec3 d = atom.position - m_cylinder.center;
    double dAlongAxis = glm::dot(d, m_normedDirection);
    if (std::abs(dAlongAxis) > m_cylinder.length / 2) return false;

    double dFromAxis2 = glm::length2(d) - dAlongAxis * dAlongAxis;

    if (dFromAxis2 > m_r2) return false;

    return true;
}


// TypeSelection

/*!
 * \brief Constructs an object based on type number.
 * \param typeNumber Type number
 */
TypeSelection::TypeSelection(int typeNumber) : m_typeNumber(typeNumber) {}

/*!
 * \brief Determines if the \a atom is in the selection.
 * \param atom An atom
 * \return \c true if the type number of the atom is the same as the one of the selection
 */
bool TypeSelection::atomInSelection(const Atom& atom) const {
    return atom.type == m_typeNumber;
}
