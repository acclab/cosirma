/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "potentiallist.h"

#include <QDebug>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QList>


/*!
 * \brief Create a list of all available potentials in PARCAS. See the documentation of PARCAS for more information
 * about what they do.
 */
PotentialList::PotentialList() {

    QFile loadFile(":/files/potentials.json");
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }
    QJsonParseError error;
    QJsonObject json = QJsonDocument::fromJson(loadFile.readAll(), &error).object();
    loadFile.close();
    qDebug() << error.errorString();

    if (json.contains("potentials") && json["potentials"].isArray()) {
        QJsonArray potentialArray = json["potentials"].toArray();
        for (int i = 0; i < potentialArray.size(); i++) {
            QJsonObject potentialObject = potentialArray[i].toObject();

            int basePotmode = -1;
            QString name = "";
            QString comment = "";
            QList<Potential::AtomPair> atomPairs;
            Potential::InputType fileType = Potential::NoFile;
            Potential::Formalism potentialType = Potential::Other;

            if (potentialObject.contains("base") && potentialObject["base"].isDouble()) {
                basePotmode = potentialObject["base"].toInt();
            }

            if (basePotmode >= 0) {
                Potential base = getPotential(basePotmode);
                name = base.name();
                comment = base.information();
                atomPairs = base.pairs();
                fileType = base.fileType();
                potentialType = base.formalism();
            }

            int potmode = potentialObject["potmode"].toInt();

            if (potentialObject.contains("name") && potentialObject["name"].isString()) {
                name = potentialObject["name"].toString();
            }

            if (potentialObject.contains("comment") && potentialObject["comment"].isString()) {
                comment = potentialObject["comment"].toString();
            }

            if (potentialObject.contains("pairs") && potentialObject["pairs"].isArray()) {
                QJsonArray pairsArray = potentialObject["pairs"].toArray();
                atomPairs.clear();
                for (int j = 0; j < pairsArray.size(); j++) {
                    QJsonArray pairValues = pairsArray[j].toArray();
                    QString t1 = pairValues[0].toString();
                    QString t2 = pairValues[1].toString();
                    QString pairComment = pairValues[2].toString();
                    atomPairs << Potential::AtomPair(t1, t2, pairComment);
                }
            }

            if (potentialObject.contains("input-type") && potentialObject["input-type"].isString()) {
                QString inputFileType = potentialObject["input-type"].toString();
                if (inputFileType == "reppot") {
                    fileType = Potential::Reppot;
                } else if (inputFileType == "eam") {
                    fileType = Potential::EAM;
                } else if (inputFileType == "no") {
                    fileType = Potential::NoFile;
                }
            }

            if (potentialObject.contains("type") && potentialObject["type"].isString()) {
                QString formalismType = potentialObject["type"].toString();
                if (formalismType == "stillinger-weber") {
                    potentialType = Potential::StillingerWeber;
                } else if (formalismType == "tersoff") {
                    potentialType = Potential::Tersoff;
                }
            }

            addPotential(Potential(name, potmode, atomPairs, potentialType, fileType, comment));
        }
    }

    qDebug() << "Continue with the execution.";

    //! \todo check if the normal tersoff potmodes (5-8) can be ignored with tersoff_compound?


    //    //    //! \todo figure out which one is the most suitable... There can't be this many different?!?
    //    //    addPotential(Potential("Ohta", 200, QStringList({"Si", "O"}), Potential::Multi, Potential::Reppot));
    //    //    addPotential(Potential("Ohta", 201, QStringList({"Si", "O"}), Potential::Multi, Potential::Reppot));
    //    //    addPotential(Potential("Ohta", 202, QStringList({"Si", "O"}), Potential::Multi, Potential::Reppot));
    //    //    addPotential(Potential("Ohta", 203, QStringList({"Si", "O"}), Potential::Multi, Potential::Reppot));
    //    //    addPotential(Potential("Ohta", 204, QStringList({"Si", "O"}), Potential::Multi, Potential::Reppot));
    //    //    addPotential(Potential("Ohta", 205, QStringList({"Si", "O"}), Potential::Multi, Potential::Reppot));
    //    //    addPotential(Potential("Ohta", 206, QStringList({"Si", "O"}), Potential::Multi, Potential::Reppot));
    //    //    addPotential(Potential("Ohta", 207, QStringList({"Si", "O"}), Potential::Multi, Potential::Reppot));
    //    //    addPotential(Potential("Ohta", 208, QStringList({"Si", "O"}), Potential::Multi, Potential::Reppot));
    //    //    addPotential(Potential("Ohta", 209, QStringList({"Si", "O"}), Potential::Multi, Potential::Reppot));


    //! \todo check if potmode 303 supports O interactions
}

/*!
 * \brief Access the instance of this class.
 *
 * The instance is created if needed.
 */
PotentialList& PotentialList::getInstance() {
    static PotentialList instance;
    return instance;
}

/*!
 * \brief Adds a potential to the list.
 * \param p The potential
 */
void PotentialList::addPotential(const Potential& p) {
    m_potentials[p.mode()] = p;
}

void PotentialList::addPotential(int newPotmode, int oldPotmode, const QString& info) {
    addPotential(Potential(newPotmode, m_potentials[oldPotmode], info));
}

/*!
 * \brief All the potentials in the list
 * \return Map from PARCAS "potmode" integer to Potential object
 */
const QMap<int, Potential>& PotentialList::potentials() const {
    return m_potentials;
}


/*!
 * \brief Filter away potentials without any of the selected atom types.
 * \param elementTypes A list of all available elements in the system (given as element symbols, e.g. "Si", "Ge").
 * \return The filtered list of supported potentials.
 */
QList<Potential> PotentialList::filteredPotentials(const QStringList& elementTypes) const {
    QList<Potential> ret;

    for (Potential pot : m_potentials.values()) {
        bool ok = false;
        if (pot.supportsPair("", "")) {
            ok = true;
        } else {
            for (QString element : elementTypes) {
                if (pot.supportedElements().contains(element)) {
                    ok = true;
                    break;
                }
            }
        }
        if (ok) {
            ret.append(pot);
        }
    }

    return ret;
}

/*!
 * \brief Access one potential
 * \param potmode PARCAS "potmode" integer for the potential
 * \return Potential with mode \a potmode
 */
Potential PotentialList::getPotential(int potmode) const {
    return m_potentials.value(potmode);
}

/*!
 * \brief Potentials in the list that can be used for simulating the interaction between the specified elements
 * \param element1 The symbol of the first elements
 * \param element2 The symbol of the second element
 * \return List of potentials that support \a element1 and \a element2
 */
QList<Potential> PotentialList::getPotentials(const QString& element1, const QString& element2) const {
    QList<Potential> ret;
    for (Potential pot : m_potentials) {
        if (pot.supportsPair(element1, element2)) {
            ret << pot;
        }
    }
    return ret;
}
