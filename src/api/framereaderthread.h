/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef FRAMEREADERTHREAD_H
#define FRAMEREADERTHREAD_H

#include "framereaderwriter.h"

#include <QMutex>
#include <QThread>
#include <QWaitCondition>


/*!
 * \brief QThread for reading frame directories in parallel
 *
 * Starts reading when read() is called. Emits frameRead() signal when the reading is finished.
 */
class FrameReaderThread : public QThread {
    Q_OBJECT

  public:
    FrameReaderThread(QObject* parent = nullptr);
    ~FrameReaderThread() override;

    void read(const QString& frameDirPath);
    const XyzData& frame();

  protected:
    void run() override;

  private:
    XyzData m_xyzData;
    QWaitCondition m_condition;
    QString m_framePath;

  signals:
    /*!
     * \brief Emitted when the reading is finished.
     * \param successful \c true if the reading was successful
     */
    void frameReadFinished(bool successful);
};

#endif  // FRAMEREADERTHREAD_H
