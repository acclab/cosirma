/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef GEOMETRIES_H
#define GEOMETRIES_H

#include "abstractstreamable.h"

#include <glm/vec3.hpp>

/*!
 * \brief A struct with six double valued variables. Defines a rectangular box in 3D space.
 */
struct Box {
    double x1 = 0;  //!< \brief The lower limit of the box in x-direction
    double x2 = 0;  //!< \brief The upper limit of the box in x-direction
    double y1 = 0;  //!< \brief The lower limit of the box in y-direction
    double y2 = 0;  //!< \brief The upper limit of the box in y-direction
    double z1 = 0;  //!< \brief The lower limit of the box in z-direction
    double z2 = 0;  //!< \brief The upper limit of the box in z-direction

    /*!
     * \brief Constructs a box where all attributes are 0.
     */
    Box() {}

    /*!
     * \brief Constructs a box with given dimensions
     * \param xmin The lower limit of the box in x-direction
     * \param xmax The upper limit of the box in x-direction
     * \param ymin The lower limit of the box in y-direction
     * \param ymax The upper limit of the box in y-direction
     * \param zmin The lower limit of the box in z-direction
     * \param zmax The upper limit of the box in z-direction
     */
    Box(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
        x1 = xmin;
        x2 = xmax;
        y1 = ymin;
        y2 = ymax;
        z1 = zmin;
        z2 = zmax;
    }

    /*!
     * \brief The equality operator
     * \param b The box being compared to
     * \return \c true if all six attributes are \e exactly equal.
     */
    bool operator==(const Box& b) const {
        return (x1 == b.x1) && (x2 == b.x2) && (y1 == b.y1) && (y2 == b.y2) && (z1 == b.z1) && (z2 == b.z2);
    }

    /*!
     * \brief The inequality operator
     * \param b The box being compared to
     * \return \c true if all six attributes are not \e exactly equal.
     */
    bool operator!=(const Box& b) const { return !operator==(b); }

    /*!
     * \brief The width of the box in x-direction
     * \return x2 - x1
     */
    double dx() const { return x2 - x1; }

    /*!
     * \brief The width of the box in y-direction
     * \return y2 - y1
     */
    double dy() const { return y2 - y1; }

    /*!
     * \brief The width of the box in z-direction
     * \return z2 - z1
     */
    double dz() const { return z2 - z1; }

    /*!
     * \brief The vector going from the (x1, y1, z1) corner to the (x2, y2, z2) corner
     * \return (x2 - x1, y2 - y1, z2 - z1)
     */
    glm::dvec3 diagonal() const { return glm::dvec3(x2 - x1, y2 - y1, z2 - z1); }

    /*!
     * \brief The center point of the box
     * \return (x1 + x2, y1 + y2, z1 + z2)/2
     */
    glm::dvec3 center() const { return 0.5 * glm::dvec3(x1 + x2, y1 + y2, z1 + z2); }
};


/*!
 * \brief Operator for streaming \a b into QDataStream
 * \param out The stream
 * \param b The box being streamed
 * \return The stream afterwards
 */
inline QDataStream& operator<<(QDataStream& out, const Box& b) {
    return out << b.x1 << b.x2 << b.y1 << b.y2 << b.z1 << b.z2;
}

/*!
 * \brief Operator for streaming \a b from QDataStream
 * \param in The stream
 * \param b The box being streamed
 * \return Thre stream afterwards
 */
inline QDataStream& operator>>(QDataStream& in, Box& b) {
    return in >> b.x1 >> b.x2 >> b.y1 >> b.y2 >> b.z1 >> b.z2;
}


/*!
 * \brief A Struct defining a sphere.
 */
struct Sphere {
    glm::dvec3 center;  //!< \brief The center point of the sphere
    double radius = 0;  //!< \brief The radius of the sphere

    /*!
     * \brief Constructs a spere with (0,0,0) center point and 0 radius.
     */
    Sphere() {}

    /*!
     * \brief Constructs a sphere with given dimensions.
     * \param c The center point
     * \param r The radius
     */
    Sphere(const glm::dvec3& c, double r) {
        center = c;
        radius = r;
    }

    /*!
     * \brief The equality operator
     * \param s The sphere being compared to
     * \return \c true if all attributes are \e exactly equal.
     */
    bool operator==(const Sphere& s) const { return (radius == s.radius) && (center == s.center); }

    /*!
     * \brief The inequality operator
     * \param s The sphere being compared to
     * \return \c true if all attributes are not \e exactly equal.
     */
    bool operator!=(const Sphere& s) const { return !operator==(s); }
};


/*!
 * \brief A struct defining a cylinder
 */
struct Cylinder {
    glm::dvec3 center;  //!< \brief The center point of the cylinder
    /*!
     * \brief The direction of the cylinder. E.g. if axis = (0,0,1), top and bottom planes are along xy-plane.
     */
    glm::dvec3 axis = glm::dvec3(0, 0, 1);
    double radius = 0;  //!< \brief The radius of the cylinder
    double length = 0;  //!< \brief The length of the cylinder (the distance between top and bottom)

    /*!
     * \brief Constructs a cylinder with zero length radius at (0,0,0) pointing to z-direction.
     */
    Cylinder() {}

    /*!
     * \brief Constructs a cyinder with given dimensions
     * \param c The center point
     * \param a The direction
     * \param r The radius
     * \param l The length
     */
    Cylinder(const glm::dvec3& c, const glm::dvec3& a, double r, double l) {
        center = c;
        axis = a;
        radius = r;
        length = l;
    }

    /*!
     * \brief The equality operator
     * \param c The cylinder being compared to
     * \return \c true if all attributes are \e exactly equal.
     */
    bool operator==(const Cylinder& c) const {
        return (center == c.center) && (axis == c.axis) && (radius == c.length) && (radius == c.length);
    }

    /*!
     * \brief The inequality operator
     * \param c The cylinder being compared to
     * \return \c true if all attributes are not \e exactly equal.
     */
    bool operator!=(const Cylinder& c) const { return !operator==(c); }
};

#endif  // GEOMETRIES_STRUCT_H
