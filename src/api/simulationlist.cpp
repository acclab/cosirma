/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "simulationlist.h"

#include "api/simulation/irradiationsimulator.h"
#include "api/simulation/relaxationsimulator.h"
#include "api/simulation/singlecascadesimulator.h"

/*!
 * \brief Constructs a state object with given parent.
 *
 * Initializes the simulation list with all COSIRMA simulations. All the simulation states are connected so that their
 * modification signals are passed forward.
 *
 * \param parent Parent object
 */
SimulationList::SimulationList(QObject* parent) : QObject(parent) {
    ParcasRelaxationController* relaxationController = new ParcasRelaxationController();
    relaxationController->setupDefaultState();
    m_controllers.push_back(relaxationController);

    ParcasCascadeController* cascadeController = new ParcasCascadeController();
    cascadeController->setupDefaultState();
    m_controllers.push_back(cascadeController);

    ParcasIrradiationController* irradiationController = new ParcasIrradiationController();
    irradiationController->setupDefaultState();
    m_controllers.push_back(irradiationController);
}


SimulationList::~SimulationList() {
    for (ParcasBaseController* controller : m_controllers) {
        delete controller;
    }
}


ParcasBaseController* SimulationList::currentController() {
    if (m_controllers.size() == 0 || (m_selectedIndex < 0 || m_selectedIndex >= m_controllers.size())) {
        return nullptr;
    }
    return m_controllers[m_selectedIndex];
}


Simulator* SimulationList::createSimulator() {
    if (m_controllers.size() == 0 || (m_selectedIndex < 0 || m_selectedIndex >= m_controllers.size())) {
        return nullptr;
    }

    if (m_controllers[m_selectedIndex]->name() == "Relaxation") {
        return new RelaxationSimulator(static_cast<ParcasRelaxationController*>(m_controllers[m_selectedIndex]));
    } else if (m_controllers[m_selectedIndex]->name() == "Cascade") {
        return new SingleCascadeSimulator(static_cast<ParcasCascadeController*>(m_controllers[m_selectedIndex]));
    } else if (m_controllers[m_selectedIndex]->name() == "Irradiation") {
        return new IrradiationSimulator(static_cast<ParcasIrradiationController*>(m_controllers[m_selectedIndex]));
    }

    return nullptr;
}


ParcasBaseController* SimulationList::controller(const QString& name) {
    for (ParcasBaseController* controller : m_controllers) {
        if (controller->name() == name) {
            return controller;
        }
    }
    return nullptr;
}


/*!
 * \brief Changes the selected simulations state
 * \param name Name of the simulation to be selected (e.g. "Relaxation"). If none of the simulations has the given name,
 * selectedSimulation() will return \c nullptr.
 */
void SimulationList::changeSimulation(const QString& name) {
    qDebug() << "SimulationList::changeSimulation()";
    qDebug() << "    " << name;

    m_selectedIndex = -1;
    for (int i = 0; i < m_controllers.size(); ++i) {
        if (m_controllers[i]->name() == name) {
            m_selectedIndex = i;
            break;
        }
    }

    if (m_selectedIndex != -1) {
        // commitChanges();
    }

    emit simulationChanged();
}

/*!
 * \brief Names of the simulations
 * \return List of name strings
 */
QStringList SimulationList::names() const {
    QStringList ret;
    for (ParcasBaseController* controller : m_controllers) {
        ret.push_back(controller->name());
    }
    return ret;
}

QDataStream& SimulationList::save(QDataStream& out) const {
    for (ParcasBaseController* controller : m_controllers) {
        out << *controller;
    }
    return out;
}

QDataStream& SimulationList::load(QDataStream& in) {
    bool blocked = signalsBlocked();
    blockSignals(true);

    for (ParcasBaseController* controller : m_controllers) {
        in >> *controller;
    }

    blockSignals(blocked);
    return in;
}
