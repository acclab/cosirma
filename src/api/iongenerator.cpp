/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "iongenerator.h"

#include "api/utils/beamdatautil.h"

#include <QFile>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/mat4x4.hpp>

#include <random>

/*!
 * \brief Constructs an object with parameters that are needed for ion generation.
 * \param beam The beam settings
 * \param energy The individual ion energy
 * \param cellLims The dimensionsion of the simulation cell
 * \param pbc The periodic boundary conditions
 * \param n The number of generated ions
 * \param seed The seed for the random number generator
 */
IonGenerator::IonGenerator() {}

/*!
 * \brief Generates the ions based on the parameters givenin the constructor.
 *
 * Mersenne Twister is used for random number generation. The ion positions are wrapped over the periodic directions.
 *
 * \return Vector of generated ions
 */
void IonGenerator::generate(const ExtendedEntryData& entry, const api::BaseParameters& generalParameters,
                            const api::BaseParameters& parameters) {

    unsigned long seed = generalParameters[api::parameters::irradiation::SEED].value().toULongLong();
    int n = generalParameters[api::parameters::irradiation::NUMBER_OF_IONS].value().toInt();

    api::enums::beam::Type beamType =
        static_cast<api::enums::beam::Type>(generalParameters[api::parameters::beam::TYPE].value().toInt());
    api::enums::beam::Face beamFace =
        static_cast<api::enums::beam::Face>(generalParameters[api::parameters::beam::FACE].value().toInt());
    api::enums::beam::Shape beamShape =
        static_cast<api::enums::beam::Shape>(generalParameters[api::parameters::beam::SHAPE].value().toInt());
    double beamWidth = generalParameters[api::parameters::beam::WIDTH].value().toDouble();

    double inclination = generalParameters[api::parameters::beam::INCLINATION].value().toDouble();
    double azimuth = generalParameters[api::parameters::beam::AZIMUTH].value().toDouble();

    double xPosition = generalParameters[api::parameters::ion_transform::X_POSITION].value().toDouble();
    double yPosition = generalParameters[api::parameters::ion_transform::Y_POSITION].value().toDouble();
    double zPosition = generalParameters[api::parameters::ion_transform::Z_POSITION].value().toDouble();
    double beamPhi = generalParameters[api::parameters::ion_transform::PHI].value().toDouble();
    double beamTheta = generalParameters[api::parameters::ion_transform::THETA].value().toDouble();

    double energy = parameters[api::parameters::ion::ENERGY].value().toDouble();

    const Box& cellLims = entry.frame.cellLims;
    const glm::bvec3& pbc = entry.frame.pbc;

    std::mt19937 generator(seed);

    m_ions.clear();

    switch (beamType) {
        case api::enums::beam::Broad: {

            QVector<double> x, y, z;

            if (beamFace == api::enums::beam::X_lower || beamFace == api::enums::beam::X_upper) {
                double fixedX = beamFace == api::enums::beam::X_lower ? cellLims.x1 : cellLims.x2;
                x = QVector<double>(std::max(0, n), fixedX);
            } else {
                std::uniform_real_distribution<double> distX(cellLims.x1, cellLims.x2);
                for (int i = 0; i < n; ++i) x.push_back(distX(generator));
            }

            if (beamFace == api::enums::beam::Y_lower || beamFace == api::enums::beam::Y_upper) {
                double fixedY = beamFace == api::enums::beam::Y_lower ? cellLims.y1 : cellLims.y2;
                y = QVector<double>(std::max(0, n), fixedY);
            } else {
                std::uniform_real_distribution<double> distY(cellLims.y1, cellLims.y2);
                for (int i = 0; i < n; ++i) y.push_back(distY(generator));
            }

            if (beamFace == api::enums::beam::Z_lower || beamFace == api::enums::beam::Z_upper) {
                double fixedZ = beamFace == api::enums::beam::Z_lower ? cellLims.z1 : cellLims.z2;
                z = QVector<double>(std::max(0, n), fixedZ);
            } else {
                std::uniform_real_distribution<double> distZ(cellLims.z1, cellLims.z2);
                for (int i = 0; i < n; ++i) z.push_back(distZ(generator));
            }

            glm::dvec3 direction =
                BroadBeamUtil::direction(inclination, azimuth, static_cast<api::enums::beam::Face>(beamFace));
            double realTheta = glm::degrees(acos(direction.z));
            double realPhi = glm::degrees(atan2(direction.y, direction.x));

            for (int i = 0; i < n; ++i) {
                Ion ion;
                ion.energy = energy;
                ion.transform.position = glm::dvec3(x[i], y[i], z[i]);
                ion.transform.phi = realPhi;
                ion.transform.theta = realTheta;

                m_ions.push_back(ion);
            }

            break;
        }
        case api::enums::beam::Focused:
            // Constructing the transformation matrix for scaling, rotating and translating a point from z=0 plane to
            // the real space coordinate
            glm::dmat4 m(1.0);
            glm::dvec3 beamPosition(xPosition, yPosition, zPosition);
            m = glm::translate(m, beamPosition);
            m = glm::scale(m, glm::dvec3(beamWidth / 2.0));
            m = glm::rotate(m, beamPhi, glm::dvec3(0, 0, 1));
            m = glm::rotate(m, beamTheta, glm::dvec3(0, 1, 0));

            std::uniform_real_distribution<double> dist(-1, 1);
            while (m_ions.size() < n) {
                // Randomizing a point on the z=0 plane
                glm::dvec4 planePos(dist(generator), dist(generator), 0, 0);

                // Rejecting points in order to find the desired shape
                if (beamShape == api::enums::beam::Circular && glm::length2(planePos) > 1) {
                    continue;
                }

                // Transforming the plane point to real space
                glm::dvec4 realPos = m * planePos;

                // Moves the ion over the periodic boundaries until it is inside the cell
                if (pbc.x && cellLims.dx() > 1e-6) {
                    while (realPos.x < cellLims.x1) realPos.x += cellLims.dx();
                    while (realPos.x > cellLims.x2) realPos.x -= cellLims.dx();
                }
                if (pbc.y && cellLims.dy() > 1e-6) {
                    while (realPos.y < cellLims.y1) realPos.y += cellLims.dy();
                    while (realPos.y > cellLims.y2) realPos.y -= cellLims.dy();
                }
                if (pbc.z && cellLims.dz() > 1e-6) {
                    while (realPos.z < cellLims.z1) realPos.z += cellLims.dz();
                    while (realPos.z > cellLims.z2) realPos.z -= cellLims.dz();
                }

                Ion ion;
                ion.energy = energy;
                ion.transform.position = glm::dvec3(realPos);
                ion.transform.phi = beamPhi;
                ion.transform.theta = beamTheta;

                m_ions.push_back(ion);
            }
            break;
    }
}

const QVector<IonGenerator::Ion> IonGenerator::ions() const {
    return m_ions;
}

QVector<IonGenerator::Ion> IonGenerator::mutableIons() const {
    return m_ions;
}

/*!
 * Writes the ion data to file.
 */

/*!
 * \brief Writes the ion data into a file.
 *
 * Uses ions() to generate the ions and then writes them into a file. There will be one line per ion. The column
 * mapping is: x[Å] y[Å] z[Å] theta[deg] phi[deg] energy[eV]
 *
 * \param path Path to the file
 * \return \c true if the writing is successful
 */
bool IonGenerator::write(const QString& path) const {
    QFile file(path);
    if (file.open(QIODevice::ReadWrite | QIODevice::Truncate)) {
        QTextStream stream(&file);

        QString help;
        QVector<Ion> i = ions();
        for (const Ion& ion : i) {
            stream << help.asprintf("%13.6f %13.6f %13.6f %8.3f %8.3f %13.2f\n", ion.transform.position.x,
                                    ion.transform.position.y, ion.transform.position.z, ion.transform.theta,
                                    ion.transform.phi, ion.energy);
        }
    } else {
        return false;
    }
    file.close();
    return true;
}
