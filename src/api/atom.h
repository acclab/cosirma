/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ATOM_H
#define ATOM_H

#include "abstractstreamable.h"
#include "api/glmoperations.h"

#include <QString>

#include <glm/vec3.hpp>

/*! \brief A struct that holds information about an atom.
 */
struct Atom : AbstractStreamable {
    glm::dvec3 position; /*!< \brief Position of the atom [Å]*/
    glm::dvec3 velocity; /*!< \brief Velocity of the atom [Å/fs]*/
    int type;            /*!< \brief Type number of the atom. PARCAS supports from -9 to 9*/
    int identifier;      /*!< \brief Integer used for identifying the atom*/
    QString symbol;      /*!< \brief Element symbol of the atom*/

    QDataStream& save(QDataStream& out) const override {
        return out << position << velocity << type << identifier << symbol;
    }
    QDataStream& load(QDataStream& in) override { return in >> position >> velocity >> type >> identifier >> symbol; }
};

#endif  // ATOM_H
