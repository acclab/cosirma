/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SIMULATIONLIST_H
#define SIMULATIONLIST_H

#include "api/simulation/parcas/parcasbasecontroller.h"
#include "api/simulation/simulator.h"


/*!
 * \brief State class that includes all the available COSIRMA simulations.
 *
 * One of the simulations can be selected.
 */
class SimulationList : public QObject, public AbstractStreamable {
    Q_OBJECT
  public:
    explicit SimulationList(QObject* parent = nullptr);
    ~SimulationList() override;

    ParcasBaseController* currentController();
    Simulator* createSimulator();

    ParcasBaseController* controller(const QString& name);

    void changeSimulation(const QString& name);
    QStringList names() const;

    QDataStream& save(QDataStream& out) const override;
    QDataStream& load(QDataStream& in) override;

  signals:
    /*!
     * \brief Emitted when the simulation state selection changes.
     */
    void simulationChanged();

  private:
    QVector<ParcasBaseController*> m_controllers;

    int m_selectedIndex = -1;
};

#endif  // SIMULATIONLIST_H
