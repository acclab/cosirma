/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "iontransformparameters.h"

#include "api/state/parameters/parameterkeys.h"

api::IonTransformParameters::IonTransformParameters() {
    setup();
}

void api::IonTransformParameters::setupInitialValues() {
    using namespace api::parameters::ion_transform;

    set(X_POSITION, api::value_t(0.0, true));
    set(Y_POSITION, api::value_t(0.0, true));
    set(Z_POSITION, api::value_t(0.0, true));
    set(PHI, api::value_t(0.0, true));
    set(THETA, api::value_t(180.0, true));
}
