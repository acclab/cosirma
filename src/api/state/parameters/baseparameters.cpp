/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "baseparameters.h"

#include <QDebug>


api::BaseParameters::BaseParameters() {}


void api::BaseParameters::setup() {
    setupInitialValues();
    setupDefaultValues();
}


void api::BaseParameters::set(const api::key_t& key, const api::value_t& value) {
    m_parameters[key] = value;
}


void api::BaseParameters::insert(const api::BaseParameters& other) {
    insert(other.m_parameters);
}


void api::BaseParameters::insert(const QMap<api::key_t, api::value_t>& other) {
    for (const api::key_t& key : other.keys()) {
        m_parameters[key] = other[key];
    }
}


void api::BaseParameters::clear() {
    m_parameters.clear();
}


const QMap<api::key_t, api::value_t>& api::BaseParameters::values() const {
    return m_parameters;
}


api::value_t api::BaseParameters::operator[](const api::key_t& key) const {
    return m_parameters[key];
}


QDataStream& api::BaseParameters::save(QDataStream& out) const {
    out << m_parameters;
    return out;
}


QDataStream& api::BaseParameters::load(QDataStream& in) {
    in >> m_parameters;
    return in;
}


void api::BaseParameters::setupInitialValues() {
    qWarning() << "This function should be overloaded!";
}


/**
 * @brief Sets the default values to be the same as the initial values. This way it is possible to revert to that state
 * whenever needed.
 */
void api::BaseParameters::setupDefaultValues() {
    qDebug() << "Setting default values";
    for (const api::key_t& key : m_parameters.keys()) {
        m_defaultParameters[key] = m_parameters[key];
    }
}

QDebug api::operator<<(QDebug d, const api::BaseParameters& p) {
    for (const api::key_t& key : p.values().keys()) {
        d << key << p[key].value() << endl;
    }
    return d;
}
