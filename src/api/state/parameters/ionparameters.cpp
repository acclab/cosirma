/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "ionparameters.h"

#include "api/state/parameters/parameterkeys.h"

api::IonParameters::IonParameters() {
    setup();
}

void api::IonParameters::setupInitialValues() {
    using namespace api::parameters::ion;

    set(BASE_TYPE, api::value_t(1, true));
    set(BASE_TYPE_ATOM_NUMBER, api::value_t(14, true));
    set(ION_TYPE, api::value_t(0, true));
    set(ION_TYPE_ATOM_NUMBER, api::value_t(-1, true));
    set(ENERGY, api::value_t(10.0, true));
    set(BASE_TYPE_MERGE_ON, api::value_t(false, true));
}
