/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ELSTOPFILEDATA_H
#define ELSTOPFILEDATA_H

#include <QTextStream>

struct ElstopFileData {
    double v;
    double F;

    ElstopFileData() {}
    ElstopFileData(const QString& line) {
        QStringList parts = line.split(" ", QString::SkipEmptyParts);
        v = parts[0].toDouble();
        F = parts[1].toDouble();
    }
    ElstopFileData(double _v, double _F) {
        v = _v;
        F = _F;
    }

    friend QTextStream& operator<<(QTextStream& stream, const ElstopFileData& data) {
        stream << QString("%1 %2").arg(data.v).arg(data.F);
        return stream;
    }
};

#endif  // ELSTOPFILEDATA_H
