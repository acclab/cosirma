/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ZBL96_H
#define ZBL96_H

#include <QList>

// defines from zbl96stop.h

#define COLS 54 + 1
#define ROWS 93 + 1
#define HEAD 2
#define ACOLS 16
#define BCOLS 38
#define LINE 250


// defines from zbl96lib.h

#define NA 6.022e23

#define TRUE 1
#define FALSE 0

#define MAI 0
#define NATURAL -1

#define EV_A 0x0001
#define KEV_NM 0x0002
#define KEV_UM 0x0003
#define MEV_MM 0x0004
#define KEV_UG_CM2 0x0005
#define MEV_MG_CM2 0x0006
#define KEV_MG_CM2 0x0007
#define EV_1E15ATOMS_CM2 0x0008

#define SUNIT 0x000f

#define EV 0x0010
#define KEV 0x0020
#define MEV 0x0030

#define V0 0x0100
#define BETA 0x0200
#define M_S 0x0300
#define CM_S 0x0400

#define XUNIT 0x0ff0

#define ENERGY 0x00f0
#define VELOCITY 0x0f00

#define N_ONLY 0x1000
#define N_BOTH 0x2000
#define N_NO 0x3000

#define NUCLEAR 0xf000

//#define DEFAULT (KEV_NM | V0 | N_NO)
#define DEFAULT (EV_A | M_S | N_NO)


class Zbl96 {
  public:
    Zbl96();

    void generate(int z1, int z2, double m1, double m2, double min, double max, double step, unsigned int flag);

    const QList<QPair<double, double>>& values() const;
    double density() const;

  private:
    void readscoef();

    double pstop(int z2, double E);
    double hestop(int z2, double E);
    double histop(int z1, int z2, double E);
    double nuclear(int z1, int z2, double m1, double m2, double E);

    double m_scoef[ROWS][COLS];

    QList<QPair<double, double>> m_values;
    double m_density;
};

#endif  // ZBL96_H
