/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "reppotgenerator.h"

#include <QtMath>


bool ReppotGenerator::generate(const QString& symbol1, const QString& symbol2, double xmin, double xmax, double xstep) {
    ElementReader reader;
    int z1 = reader.zFromSymbol(symbol1);
    int z2 = reader.zFromSymbol(symbol2);

    return generate(z1, z2, xmin, xmax, xstep);
}


/**
 * @brief Using the universal ZBL potential to calculate the repulsive potential between the selected atom pair.
 *
 * V(r) = 1/(4 pi epsilon0) * Z1 * Z2 e^2 / r * phi(r/a)
 *
 * a = au = 0.8854 * a0 / (Z1^0.23 + Z2^0.23)
 * a0 = 0.529177 Å
 *
 * phi(x) = 0.1818 exp(-3.2x) + 0.5099 exp(-0.9423x) + 0.2802 exp(-0.4028x) + 0.02817 exp(-0.2016x)
 *
 * The result is given in eV, so the elemental charge is set to 1 in the V(r) equation.
 *
 * @param t1
 * @param t2
 * @param rmin
 * @param rmax
 * @param rstep
 *
 * @return true if the generation was successful
 */
bool ReppotGenerator::generate(int z1, int z2, double rmin, double rmax, double rstep) {

    // Scaling the bohr radius based on the atomic number
    double au = 0.8854 * 0.529177 / (qPow(z1, 0.23) + qPow(z2, 0.23));
    // Calculating the 1 / (4 pi epsilon0) * Z1 * Z2
    double factor = 14.3996458 * z1 * z2;

    double screeningA[4];
    double screeningB[4];

    // clang-format off
    screeningA[0] = -3.2;       screeningB[0] = 0.1818;
    screeningA[1] = -0.9423;    screeningB[1] = 0.5099;
    screeningA[2] = -0.4028;    screeningB[2] = 0.2802;
    screeningA[3] = -0.2016;    screeningB[3] = 0.02817;
    // clang-format on

    m_data.clear();

    int i;
    double r;
    double x;
    double phi, dphidr;
    double V, dVdr;
    for (i = 0, r = rmin; r <= rmax; ++i, r += rstep) {
        x = r / au;
        // Calculate the screening function, phi(x)
        phi = 0;
        dphidr = 0;
        for (int j = 0; j < 4; ++j) {
            phi += screeningB[j] * qExp(screeningA[j] * x);
            dphidr += screeningA[j] / au * screeningB[j] * qExp(screeningA[j] * x);
        }
        if (r != 0) {
            // Calculate the potential
            V = factor / r * phi;
            // Calculate the spatial derivative of the potential
            dVdr = factor / (r * r) * dphidr;
            m_data.append(ReppotFileData(r, V, dVdr));
        }
    }
    return true;
}


const DataFileContent<ReppotFileData>& ReppotGenerator::data() {
    return m_data;
}
