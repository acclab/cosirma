/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef REPPOTGENERATOR_H
#define REPPOTGENERATOR_H

#include "api/data/datafilecontent.h"
#include "api/data/reppotfiledata.h"
#include "api/entry.h"

#include <QList>


class ReppotGenerator {
  public:
    bool generate(const QString& symbol1, const QString& symbol2, double rmin, double rmax, double rstep);
    bool generate(int z1, int z2, double rmin, double rmax, double rstep);

    const DataFileContent<ReppotFileData>& data();

  private:
    DataFileContent<ReppotFileData> m_data;
};

#endif  // REPPOTGENERATOR_H
