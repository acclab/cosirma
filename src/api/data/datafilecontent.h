/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef DATAFILECONTENT_H
#define DATAFILECONTENT_H

#include <QFile>
#include <QList>
#include <QTextStream>


template <class T> class DataFileContent {
  public:
    void append(const T& value) { m_data.append(value); }
    void clear() { m_data.clear(); }

    bool write(const QString& path) {
        QFile outputFile(path);
        if (outputFile.open(QIODevice::WriteOnly)) {
            QTextStream stream(&outputFile);
            for (const T& data : m_data) {
                stream << data << endl;
            }
            return true;
        }
        return false;
    }

    bool read(const QString& path) {
        // Clear the currently loaded data
        clear();

        // If the input file doesn't exist exit
        QFile inputFile(path);
        if (!inputFile.exists()) {
            return false;
        }

        if (inputFile.open(QIODevice::ReadOnly)) {
            // If the input file exists, read the data!
            QTextStream stream(&inputFile);
            while (!stream.atEnd()) {
                T data(stream.readLine());
                append(data);
            }
        }

        return m_data.size() > 0;
    }


    const QList<T>& values() const { return m_data; };

  private:
    QList<T> m_data;
};

#endif  // DATAFILECONTENT_H
