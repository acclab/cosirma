/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef REPPOTFILEDATA_H
#define REPPOTFILEDATA_H

#include <QTextStream>

struct ReppotFileData {
    double r;
    double V;
    double dV;

    ReppotFileData() {}
    ReppotFileData(const QString& line) {
        QStringList parts = line.split(" ", QString::SkipEmptyParts);
        r = parts[0].toDouble();
        V = parts[1].toDouble();
        dV = parts[2].toDouble();
    }
    ReppotFileData(double _r, double _V, double _dV) {
        r = _r;
        V = _V;
        dV = _dV;
    }

    friend QTextStream& operator<<(QTextStream& stream, const ReppotFileData& data) {
        stream << QString("%1 %2 %3").arg(data.r).arg(data.V).arg(data.dV);
        return stream;
    }
};

#endif  // REPPOTFILEDATA_H
