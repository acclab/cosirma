/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef BEAMDATA_H
#define BEAMDATA_H

#include "api/geometries.h"
#include "api/state/enums.h"

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>


/*!
 * \brief Beam settings struct.
 *
 * In irradiation simulations, beam settings determine how ions are added to the simulation cell.
 *
 * \sa BeamState
 */
struct BeamData {

    api::enums::beam::Type type = api::enums::beam::Broad;  //!< \brief Type of the beam

    // Broad
    /*!
     * \brief \brief Broad beam setting. The simulation cell face that the ions will be introduced at.
     */
    api::enums::beam::Face face = api::enums::beam::Z_upper;

    /*!
     * \brief Broad beam setting. The azimuth angle [deg] of the direction of the beam with respect to the specified
     * face.
     */
    double azimuth = 0;

    /*!
     * \brief Broad beam setting. The azimuth angle [deg] of the direction of the beam with respect to the specified
     * face.
     *
     * 0 deg angle means that the beam points directly at the simulation cell face. 90 deg angle is parallel to the
     * face.
     */
    double inclination = 0;

    // Focused
    /*!
     * \brief Focused beam setting. The shape of the focused beam.
     */
    api::enums::beam::Shape shape = api::enums::beam::Circular;

    /*!
     * \brief Focused beam setting. The width of the beam.
     *
     * If the shape is #Circular, this is the diameter. If the shape is #Square, this is the side length.
     */
    double width = 10;

    /*!
     * \brief Focused beam setting. Position of the beam.
     *
     * The center point of the area on which the ions are added.
     */
    glm::dvec3 position;

    /*!
     * \brief  Focused beam setting. Azimuth angle [deg] of the direction of the beam.
     *
     * The azimuth angle in the global spherical coordinates (unlike #azimuth).
     */
    double phi = 0;

    /*!
     * \brief  Focused beam setting. Inclination angle [deg] of the direction of the beam.
     *
     * The inclination angle in the global spherical coordinates (unlike #inclination).
     */
    double theta = 0;

    /*!
     * \brief Equality operator
     * \param d The data struct being compared to
     * \return \c true if all the attributes are \em exactly the same.
     */
    bool operator==(const BeamData& d) const {
        return (type == d.type) && (face == d.face) && (azimuth == d.azimuth) && (inclination == d.inclination) &&
               (shape == d.shape) && (width == d.width) && (position == d.position) && (phi == d.phi) &&
               (theta == d.theta);
    }

    /*!
     * \brief Inequality operator
     * \param d The data struct being compared to
     * \return \c true if one or more attributes are not \em exactly the same.
     */
    bool operator!=(const BeamData& d) const { return !operator==(d); }
};
#endif  // BEAMDATA_H
