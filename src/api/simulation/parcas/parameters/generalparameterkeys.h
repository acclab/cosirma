/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef GENERALPARAMETERKEYS_H
#define GENERALPARAMETERKEYS_H

#include "api/simulation/mdparameterkeys.h"

const parameter SEED{"seed", true, 12345};
const parameter NSTEPS{"nsteps", true, 1};
const parameter TMAX{"tmax", true, 1.0};
const parameter DELTA{"delta", true, 0.1};
const parameter NEISKINR{"neiskinr", true, QVariant()};
const parameter NPRTBL{"nprtbl", true, QVariant()};
const parameter NBALANCE{"nbalance", true, QVariant()};

const QList<parameter> generalKeys{SEED, NSTEPS, TMAX, DELTA, NEISKINR, NPRTBL, NBALANCE};

#endif  // GENERALPARAMETERKEYS_H
