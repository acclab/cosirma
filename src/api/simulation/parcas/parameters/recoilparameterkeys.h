/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef RECOILPARAMETERKEYS_H
#define RECOILPARAMETERKEYS_H

#include "api/simulation/mdparameterkeys.h"

const parameter RSMODE{"rsmode", false, QVariant()};
const parameter IREC{"irec", false, QVariant()};
const parameter RECATYPE{"recatype", false, QVariant()};
const parameter XREC{"xrec", false, QVariant()};
const parameter YREC{"yrec", false, QVariant()};
const parameter ZREC{"zrec", false, QVariant()};
const parameter RECEN{"recen", false, QVariant()};
const parameter RECTHETA{"rectheta", false, QVariant()};
const parameter RECPHI{"recphi", false, QVariant()};

const QList<parameter> recoilKeys{RSMODE, IREC, RECATYPE, XREC, YREC, ZREC, RECEN, RECTHETA, RECPHI};

#endif  // RECOILPARAMETERKEYS_H
