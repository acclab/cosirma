/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PRESSURECONTROLPARAMETERKEYS_H
#define PRESSURECONTROLPARAMETERKEYS_H

#include "api/simulation/mdparameterkeys.h"

const parameter BPCMODE{"bpcmode", false, QVariant()};
const parameter BPCP0X{"bpcP0x", false, QVariant()};
const parameter BPCP0Y{"bpcP0y", false, QVariant()};
const parameter BPCP0Z{"bpcP0z", false, QVariant()};
const parameter BPCTAU{"bpctau", false, QVariant()};
const parameter BPCBETA{"bpcbeta", false, QVariant()};

const QList<parameter> pressureControlKeys{BPCMODE, BPCP0X, BPCP0Y, BPCP0Z, BPCTAU, BPCBETA};

#endif  // PRESSURECONTROLPARAMETERKEYS_H
