/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "parcasbasecontroller.h"

#include "api/programsettings.h"
#include "api/typepairs.h"

#include <QDir>


ParcasBaseController::ParcasBaseController() {}


bool ParcasBaseController::createLocalSimulation(const QString& path, const ExtendedEntryData& entry) {
    if (!createSimulationEnvironment(path, entry)) return false;
    if (!writeLocalScript(path, entry)) return false;

    return true;
}


bool ParcasBaseController::createStandaloneSimulation(const QString& path, const ExtendedEntryData& entry,
                                                      const ExportData& data) {
    if (!createSimulationEnvironment(path, entry)) return false;
    if (!writeStandaloneScript(path, entry, data)) return false;

    if (!data.description.isEmpty()) {
        qDebug() << "Write the description to a file!";
    }

    for (const QString& key : data.files.keys()) {
        qDebug() << "file" << key << "content" << data.files[key];
    }

    return true;
}


bool ParcasBaseController::createSimulationEnvironment(const QString& path, const ExtendedEntryData& entry) {
    QDir dir(path);
    if (!dir.isEmpty()) {
        qDebug() << "Directory not empty!" << path;
        return false;
    }

    dir.mkpath(dir.path());
    dir.mkpath(dir.path() + "/in");
    dir.mkpath(dir.path() + "/frames/frame_0");

    // Write the inital lattice file
    if (!entry.frame.write(path + "/frames/frame_0/mdlat.xyz")) return false;

    // Write potentials
    for (DataFile<ReppotFileData> file : entry.data.potentialFiles.values()) {
        qDebug() << file.path();
        qDebug() << file.filename();
        qDebug() << file.filepath();
        qDebug() << "------";
        file.write(file.filepath());
        file.write(dir.path() + "/in/" + file.filename());
    }

    // Write elstop files
    for (DataFile<ElstopFileData> file : entry.data.elstopFiles.values()) {
        qDebug() << file.path();
        qDebug() << file.filename();
        qDebug() << file.filepath();
        qDebug() << "------";
        QString path = dir.path() + "/in/" + file.filename();
        qDebug() << path;
        file.write(file.filepath());
        file.write(dir.path() + "/in/" + file.filename());
    }

    return true;
}


void ParcasBaseController::insertParameters(api::BaseParameters parameters) {
    m_parameters.insert(parameters);
}


api::BaseParameters ParcasBaseController::parameters() {
    return m_parameters;
}


void ParcasBaseController::setEntry(const ExtendedEntryData& entry) {
    m_entry = entry;
}


const ExtendedEntryData& ParcasBaseController::entry() {
    return m_entry;
}


QDataStream& ParcasBaseController::save(QDataStream& out) const {
    out << m_parameters;
    return out;
}


QDataStream& ParcasBaseController::load(QDataStream& in) {
    in >> m_parameters;
    return in;
}


bool ParcasBaseController::writeTemplateFile(const QString& templatePath, const QString& path,
                                             const QMap<QString, QString>& parameterMap) {
    QFile assetFile(templatePath);
    if (assetFile.open(QIODevice::ReadOnly)) {
        QByteArray content = assetFile.readAll();

        for (const QString& key : parameterMap.keys()) {
            content.replace(key.toLatin1(), parameterMap[key].toLatin1());
        }

        QFile outFile(path);
        if (outFile.open(QIODevice::WriteOnly)) {
            QTextStream out(&outFile);
            out << content;
        }
    } else {
        return false;
    }

    return true;
}


bool ParcasBaseController::writeFiles(const QString& path, const QMap<QString, QString>& files) {
    for (const QString& filename : files.keys()) {
        QFile file(path + "/" + filename);
        if (file.open(QIODevice::WriteOnly)) {
            QTextStream stream(&file);
            stream << files[filename];
            file.close();
        } else {
            return false;
        }
    }

    return true;
}
