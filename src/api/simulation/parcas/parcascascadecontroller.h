/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PARCASCASCADECONTROLLER_H
#define PARCASCASCADECONTROLLER_H

#include "api/simulation/parcas/parcasbasecontroller.h"


class ParcasCascadeController : public ParcasBaseController {
  public:
    ParcasCascadeController();

    bool writeLocalScript(const QString& path, const ExtendedEntryData& entry) override;
    bool writeStandaloneScript(const QString& path, const ExtendedEntryData& entry, const ExportData& data) override;
    bool writeInputFile(const QString& path, const ExtendedEntryData& entry) override;

    void setupDefaultState() override;

    QString name() override;
};

#endif  // PARCASCASCADECONTROLLER_H
