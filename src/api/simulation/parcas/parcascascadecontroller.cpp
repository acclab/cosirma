/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "parcascascadecontroller.h"

#include "api/programsettings.h"
#include "api/simulation/parcas/parcascontrollerutils.h"

#include "api/state/parameters/ionparameters.h"
#include "api/state/parameters/iontransformparameters.h"
#include "api/state/parameters/outputparameters.h"
#include "api/state/parameters/pressurecontrolparameters.h"
#include "api/state/parameters/temperaturecontrolparameters.h"
#include "api/state/parameters/timeparameters.h"


ParcasCascadeController::ParcasCascadeController() {}


bool ParcasCascadeController::writeLocalScript(const QString& path, const ExtendedEntryData& entry) {
    QMap<QString, QString> parameterMap;
    parameterMap.insert("{{parcas}}", ProgramSettings::getInstance().backendSettings().parcasPath);

    //! \todo Wrap this with the useMpi boolean found in the backendSettings
    parameterMap.insert("{{parallel_exe}}", QString("mpirun -np %1").arg(ProgramSettings::getInstance().backendSettings().mpiCores));

    return writeTemplateFile(":/scripts/local-parcas-exe-template.sh", path + "/run-parcas.sh", parameterMap);
}


bool ParcasCascadeController::writeStandaloneScript(const QString& path, const ExtendedEntryData& entry,
                                                    const ExportData& data) {
    qDebug() << this << "writeStandaloneScript()";

    QMap<QString, QString> parameterMap;
    parameterMap.insert("{{parcas}}", data.parcasPath);
    int saveMovie = parameters()[api::parameters::output::MOVIE_ON].value().toBool() ? 1 : 0;
    parameterMap.insert("{{save_movie}}", QString::number(saveMovie));
    parameterMap.insert("{{parallel_exe}}", data.parameters["parallel_exe"].toString());
    parameterMap.insert("{{label_simulation_type}}", "Cascade");
    parameterMap.insert("{{label_frame_complete}}", "Wrote frame");

    if (!writeTemplateFile(":/scripts/standalone-parcas-simulation-template.sh", path + "/run-simulation.sh",
                           parameterMap))
        return false;

    if (!writeInputFile(path + "/in/md.in", entry)) return false;

    if (!writeFiles(path, data.files)) return false;

    return true;
}


bool ParcasCascadeController::writeInputFile(const QString& path, const ExtendedEntryData& entry) {
    ParcasControllerUtils utils;
    return utils.writeCascadeFile(entry, path, parameters());
}


void ParcasCascadeController::setupDefaultState() {
    m_parameters.insert(api::IonParameters());
    m_parameters.insert(api::IonTransformParameters());
    m_parameters.insert(api::TimeParameters());
    m_parameters.insert(api::TemperatureControlParameters());
    m_parameters.insert(api::PressureControlParameters());
    m_parameters.insert(api::OutputParameters());
}


QString ParcasCascadeController::name() {
    return "Cascade";
}
