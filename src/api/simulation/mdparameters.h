/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef MDPARAMETERS_H
#define MDPARAMETERS_H

#include "api/simulation/mdparameterkeys.h"

#include <QDataStream>
#include <QDebug>
#include <QMap>
#include <QObject>


struct ParameterValue {
    const static int Scalar = 0;
    const static int Array1D = 1;
    const static int Array2D = 2;

    int dim = Scalar;

    QVariant value;
    QList<QVariant> value1d;
    QList<QList<QVariant>> value2d;
    bool arrayZeroIndex;

    ParameterValue() {
        value = QVariant();
        dim = Scalar;
    }
    ParameterValue(QVariant _value) {
        value = _value;
        dim = Scalar;
    }
    ParameterValue(QList<QVariant> _value, bool zeroIndex = false) {
        value1d = _value;
        dim = Array1D;
        arrayZeroIndex = zeroIndex;
    }
    ParameterValue(QList<QList<QVariant>> _value, bool zeroIndex = false) {
        value2d = _value;
        dim = Array2D;
        arrayZeroIndex = zeroIndex;
    }

    bool operator==(const ParameterValue& other) const {
        if (dim != other.dim) {
            return false;
        }

        switch (dim) {
            case Scalar:
                return value == value;
            case Array1D:
                return value1d == value1d;
            case Array2D:
                return value2d == value2d;
        }

        return false;
    }

    bool operator!=(const ParameterValue& other) const {
        if (dim != other.dim) {
            return true;
        }

        switch (dim) {
            case ParameterValue::Scalar:
                return value != other.value;
            case ParameterValue::Array1D:
                return value1d != other.value1d;
            case ParameterValue::Array2D:
                return value2d != other.value2d;
        }

        return false;
    }

    friend QDebug operator<<(QDebug d, const ParameterValue& p) {
        switch (p.dim) {
            case ParameterValue::Scalar:
                d << p.value;
                break;
            case ParameterValue::Array1D:
                d << p.value1d;
                break;
            case ParameterValue::Array2D:
                d << p.value2d;
                break;
        }
        return d;
    }

    friend QDataStream& operator<<(QDataStream& stream, const ParameterValue& value) {
        stream << value.dim;
        switch (value.dim) {
            case Scalar:
                stream << value.value;
            case Array1D:
                stream << value.value1d;
            case Array2D:
                stream << value.value2d;
        }
        return stream;
    }

    friend QDataStream& operator>>(QDataStream& stream, ParameterValue& value) {
        stream >> value.dim;
        switch (value.dim) {
            case Scalar:
                stream >> value.value;
            case Array1D:
                stream >> value.value1d;
            case Array2D:
                stream >> value.value2d;
        }
        return stream;
    }
};


class MDParameters {
  public:
    MDParameters();

    void removeParameter(const parameter& key);
    void setParameter(const parameter& key, const ParameterValue& value);
    const ParameterValue& value(const parameter& key);
    const QMap<parameter, ParameterValue>& parameters() const;

    void updateParameters(const MDParameters& other);
    void updateParameters(const QMap<parameter, ParameterValue>& parameters);

    ParameterValue operator[](const parameter& key) const;

    bool save(const QString& path);
    bool load(const QString& path);

    bool write(const QString& path);

  protected:
    QMap<parameter, ParameterValue> m_parameters; /*!< \brief Map from md.in variable name to value */

  signals:
};


QDebug operator<<(QDebug d, const MDParameters& p);

#endif  // MDPARAMETERS_H
