/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef IRRADIATIONSIMULATOR_H
#define IRRADIATIONSIMULATOR_H

#include "api/simulation/parcas/parcasirradiationcontroller.h"
#include "api/simulation/simulator.h"

#include "api/entryeditor.h"


class IrradiationSimulator : public Simulator {
  public:
    IrradiationSimulator(ParcasIrradiationController* controller, QObject* parent = nullptr);

    ParcasIrradiationController* controller();

  protected:
    void preSimulationStep() override;
    void simulationStep() override;
    void postSimulationStep() override;

  private:
    int m_cascadeCounter = 0;
    int m_iterationCounter = 0;
    int m_relaxationCounter = 0;

    bool m_preProcessRelaxation = false;
    bool m_preProcessCascade = false;

    bool m_mergeIons = false;

    glm::dvec3 m_cascadeShift;

    EntryEditor m_editor;
};

#endif  // IRRADIATIONSIMULATOR_H
