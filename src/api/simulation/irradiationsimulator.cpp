/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "irradiationsimulator.h"

#include "api/project.h"

#include <QDir>


IrradiationSimulator::IrradiationSimulator(ParcasIrradiationController* controller, QObject* parent)
        : Simulator(controller, parent) {
    m_iterationCounter = 0;
    m_cascadeCounter = 0;
}


ParcasIrradiationController* IrradiationSimulator::controller() {
    return static_cast<ParcasIrradiationController*>(Simulator::controller());
}


void IrradiationSimulator::preSimulationStep() {

    QString path = QString("%1/frames/frame_%2/").arg(runtimePath()).arg(m_iterationCounter);
    qDebug() << "Input frame: " << path;
    qDebug() << "    iterations: " << m_iterationCounter;
    qDebug() << "    relaxations:" << m_relaxationCounter;
    qDebug() << "    cascades:   " << m_cascadeCounter;

    if (m_iterationCounter == 0) {
        // If the simulation should start with a relaxation, set the value to 0, else 1. The reset later is always set
        // to 1 to not get two relaxations after each other.
        m_relaxationCounter =
            controller()->parameters()[api::parameters::irradiation::START_WITH_RELAX_ON].value().toBool() ? 0 : 1;
    }

    const ExtendedEntryData& entry = currentEntry();
    m_editor.setEntry(entry);

    qDebug() << m_relaxationCounter;
    int relaxInterval = controller()->cascadeParameters()[api::parameters::irradiation::RELAX_INTERVAL].value().toInt();
    if (m_relaxationCounter % relaxInterval == 0) {
        controller()->writeRelaxInputFile(runtimePath() + "/in/md.in", entry);
    } else {
        EntryEditor::CenterCascadeAction action(controller()->currentIonTransform(), 3.0, 10);
        m_editor.edit(action);
        m_cascadeShift = action.shiftOffset();

        controller()->writeCascadeInputFile(runtimePath() + "/in/md.in", entry);
    }

    m_editor.saveEntryData(runtimePath() + "/in/mdlat.in.xyz");

    Simulator::preSimulationStep();
}

void IrradiationSimulator::simulationStep() {
    Simulator::simulationStep();
    //    m_stepDone = true;
}

void IrradiationSimulator::postSimulationStep() {
    QString path = QString("%1/frames/frame_%2/").arg(runtimePath()).arg(m_iterationCounter + 1);
    qDebug() << "Output frame: " << path;
    QDir dir(path);
    if (!dir.isEmpty()) {
        qDebug() << "The target directory is already in use!";
        m_running = false;
        return;
    }
    if (dir.mkpath(dir.path())) {
        qDebug() << "    "
                 << "Created path: " << dir.path();
    }

    ExtendedEntryData entry;
    entry.frame.read(runtimePath() + "/out/end.xyz");

    m_editor.setEntry(entry);

    int relaxInterval = controller()->cascadeParameters()[api::parameters::irradiation::RELAX_INTERVAL].value().toInt();
    if (m_relaxationCounter % relaxInterval == 0) {
        // Finalize relaxation
        m_relaxationCounter = 0;  // Reset the relaxation counter. It will be reset to 1 as the incrementor later in
                                  // this function handles the +1.
    } else {
        // Finalize cascade

        // Shift the cell back to how it was before the cascade.
        EntryEditor::ShiftCellAction action(-1.0 * m_cascadeShift);
        m_editor.edit(action);

        EntryEditor::RemoveSputteredAction removeSputteredAction(3.0);
        m_editor.edit(removeSputteredAction);

        m_cascadeCounter++;
    }

    // Merge the ion wth the correct atom type if the setting is enabled.
    if (controller()->parameters()[api::parameters::ion::BASE_TYPE_MERGE_ON].value().toBool()) {
        qDebug() << "Merge the ion type with the selected atom type";
        ElementReader reader;
        int zTarget = controller()->parameters()[api::parameters::ion::BASE_TYPE_ATOM_NUMBER].value().toInt();
        AtomType targetType;
        targetType.mass = reader.getMass(zTarget);
        targetType.symbol = reader.getSymbol(zTarget);
        targetType.atomNumber = zTarget;
        targetType.typeNumber = controller()->parameters()[api::parameters::ion::BASE_TYPE].value().toInt();
        TypeSelection selection(0);
        EntryEditor::MergeToTypeAction action(targetType, true, &selection);
        m_editor.edit(action);
    }

    m_editor.saveEntryData(path + "/mdlat.xyz");

    QFile::copy(runtimePath() + "/in/md.in", path + "/md.in");
    QFile::copy(runtimePath() + "/parcas.log", path + "/parcas.log");

    //! \todo Check this separately for both cascades and relaxations!
    //    if (controller()->saveMovieFile()) {
    //        QFile::rename(runtimePath() + "/out/md.movie", path + "/md.movie");
    //    }


    // Increment the counters
    m_iterationCounter++;
    m_relaxationCounter++;

    int nIons = controller()->cascadeParameters()[api::parameters::irradiation::NUMBER_OF_IONS].value().toInt();
    if (m_cascadeCounter >= nIons) {
        m_running = false;
    }

    setCurrentEntry(entry);

    Simulator::postSimulationStep();
}
