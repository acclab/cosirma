/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ELEMENTREADER_H
#define ELEMENTREADER_H

#include <QString>

/*! \brief Provides names, symbols, masses and atomic numbers for elements.
 *
 * Includes the required getters, setters and conversion operations
 * for the periodic table included in the header file "MDpt.h".
 */
class ElementReader {
  public:
    QString getName(int z);
    QString getSymbol(int z);
    double getMass(int z);
    int zFromSymbol(const QString& symbol);

    QString latticeType(int z);
    double latticeConstant(int z);
};

#endif  // ELEMENTREADER_H
