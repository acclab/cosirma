/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "elementreader.h"

#include "MDpt.h"


/*!
 * \brief Searches for the name of the element with the atomic number \a z.
 * \param z Atomic number
 * \return Element name. Empty Qstring if not found
 */
QString ElementReader::getName(int z) {
    int index = z - 1;
    if ((index < 0) | (index >= p_table_length)) return "";
    return (QString::fromUtf8(p_table[index].name)).trimmed();
}

/*!
 * \brief Searches for the symbol of the element with the atomic number \a z.
 * \param z Atomic number
 * \return Element symbol. Empty QString if not found
 */
QString ElementReader::getSymbol(int z) {
    int index = z - 1;
    if ((index < 0) | (index >= p_table_length)) return "";
    return (QString::fromUtf8(p_table[index].symbol)).trimmed();
}

/*!
 * \brief Searches for the atomic number of the element with the element symbol \a symbol.
 * \param symbol Element symbol
 * \return Atomic number Z. -1 if not found
 */
int ElementReader::zFromSymbol(const QString& symbol) {
    for (int i = 0; i < p_table_length; i++) {
        if (symbol.toLower() == QString::fromUtf8(p_table[i].symbol).trimmed().toLower()) {
            return i + 1;
        }
    }
    return -1;
}

/*!
 * \brief Searches for the atomic mass of the element with the atomic number \a z.
 * \param z Atomic number
 * \return Element atomic mass. 0 if not found
 */
double ElementReader::getMass(int z) {
    int index = z - 1;
    if ((index < 0) | (index >= p_table_length)) return 0;
    return p_table[index].atomic_mass;
}


QString ElementReader::latticeType(int z) {
    int index = z - 1;
    if ((index < 0) | (index >= p_table_length)) return "";
    return p_table[index].crystal;
}


double ElementReader::latticeConstant(int z) {
    int index = z - 1;
    if ((index < 0) | (index >= p_table_length)) return 0;
    return p_table[index].a;
}
